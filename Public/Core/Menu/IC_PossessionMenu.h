// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "IC_PossessionMenu.generated.h"

UCLASS()
class THEINDIANCAVE_API AIC_PossessionMenu : public AActor
{
	GENERATED_BODY()
	
public:	

  DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMenuMove, int, idx);

  UPROPERTY(BlueprintAssignable, Category = "Config | Events")
    FMenuMove OnMenuMove;

	// Sets default values for this actor's properties
	AIC_PossessionMenu();

  // Called every frame
  virtual void Tick(float DeltaTime) override;

  void MenuLeft();
  void MenuRight();
  void MenuPossess();
  void MenuRecover();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
    int idx_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
    int possessed_idx_;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	
};
