// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Core/Objects/IC_Object.h"
#include "IC_Character.h"
#include "Characters/IC_Character_Indian.h"
#include "IC_BlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_BlueprintFunctionLibrary : public UBlueprintFunctionLibrary {
  GENERATED_BODY()

public:

  /**
  * Compares the object that is being holded by the Indian by the given name.
  * @param gamemode GameMode of the level (doesn't have to be IC_GameMode).
  * @param name name we want to check against the name of the grabbed object by the indian.
  */
  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static bool CompareIndianHoldingObject(FString name, UObject* WorldContextObject);

  /**
  * Teleport the parameter animal to the target location assigned.
  * @param type Character type.
  * @param TargetPosition Position where the animal will be located.
  */
  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void TeleportAnimalToPosition(ECharacterType type, FVector TargetPosition, UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static AIC_Character_Indian* GetIndianReference(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static AIC_Character* GetCurrentCharacterReference(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void ControllerShake(float intensity, float duration, bool affectLeftLarge, bool affectLeftSmall, bool affectRightLarge, bool affectRightSmall, UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static void ShowSubtitle(UObject* WorldContextObject, FString Cadena, FColor color = FColor::White, float TimeToShowSubtitle_ = 3.0f, float Height = -20.0f);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void EnableAllCharactersMovement(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void DisableAllCharactersMovement(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void PutObjectInToolbelt(AIC_Object* object);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void PickObjectOfToolbelt(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void PickAnyObject(AIC_Object* object);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static bool GameInstanceDevelopment(UObject * WorldContextObject);

	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static void SaveGame(UObject * WorldContextObject);
		 
	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static int LoadGame(UObject * WorldContextObject);

	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static void SetActualMapSection(UObject * WorldContextObject, int IndexSection);
    
	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
		static void SetActualMapsName(UObject * WorldContextObject, TArray<FName> LoadMaps, TArray<FName> UnloadMaps);

	UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void ShowSubtitles(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void HideSubtitles(UObject* WorldContextObject);

  //////////////////////////////////////////////////////////////////////////////
  // Tutorial Functions
  //////////////////////////////////////////////////////////////////////////////

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void NextTutorial(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void PreviousTutorial(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void ShowTutorial(UObject* WorldContextObject);

  UFUNCTION(BlueprintCallable, Category = "IC_BlueprintLibrary", meta = (WorldContext = WorldContextObject))
    static void HideTutorial(UObject* WorldContextObject);

};		

