// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "IC_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_UserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Menu")
    UButton* FocusRedirect;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Menu")
    TArray<UIC_UserWidget*> Buttons;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Menu")
    bool bComponentsWidget;

  virtual void Tick(float DeltaSeconds);
	
  UFUNCTION(BlueprintCallable, Category = "Config | UI")
    virtual void SetFocusHere();

  //virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

  //virtual void NativeOnFocusLost(const FFocusEvent& InFocusEvent) override;

  virtual FReply NativeOnFocusReceived(const FGeometry & InGeometry, const FFocusEvent & InFocusEvent) override;

  UFUNCTION(BlueprintCallable, Category = "Config | Key", meta = (DeprecatedFunction, DeprecationMessage = "Created for developing purposes, it maybe will be destroy after this process."))
    void SendKeyInputFromChar(uint8 inChar);
};
