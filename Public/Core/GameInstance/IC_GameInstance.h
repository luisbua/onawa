// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "IC_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
  public:
    UIC_GameInstance();

		

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
      bool bSquirrelTutorialCompleted;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
      bool bEmptyBool1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
      bool bEmptyBool2;
												
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
      bool bDevelopment;
		
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
			int MapIndex;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_GameInstance")
			TArray<FName> CurrentMaps;

		
};
