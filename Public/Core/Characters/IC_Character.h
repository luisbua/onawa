// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Objects/IC_Object.h"

#include "Interfaces/IC_Interface.h"
#include "Components/IC_CarryObjectComponent.h"
#include "Components/WidgetComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMaterialLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "IC_HudWidget.h"
#include "Trigger/IC_Trigger.h"
#include "IC_Character.generated.h"

class AIC_PlayerController;
class AIC_Character_Squirrel;
class AIC_GameMode;

UCLASS()
class THEINDIANCAVE_API AIC_Character : public ACharacter, public IIC_Interface {
  
  GENERATED_BODY()

public:

  ////////////////////////////////////////////////////////////////////////
  // Character Components
  ///////////////////////////////////////////////////////////////////////

  /** First person camera. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Camera")
    UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
		UMaterialBillboardComponent* billboardComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
		UMaterialInstanceDynamic* DynamicCrosshair;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
    USceneComponent* PossessionMenuLocation;

  /** Type of the Character. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Character")
    ECharacterType CharacterType;

  UPROPERTY()
    AIC_GameMode* GameMode;

  UPROPERTY()
    AIC_PlayerController* PlayerController;
	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		UTextRenderComponent* Subtitle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		UStaticMeshComponent* SubtitleBack;
	*/
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		UFont* Font;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		UMaterialInterface* TextMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		UWidgetComponent* HudWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
		TSubclassOf<UIC_HudWidget> HudReference;
	
	
  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this character's properties.
  AIC_Character();

  // Called when the game starts or when spawned.
  virtual void BeginPlay() override;

  // Called every frame.
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Raycast Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Handles Raycast */
  void Raycast();

  /** Behaviour for accumulating time when raycast hits an IC_Trigger with bVision enabled. */
  void TriggerVisionRaycast();

	/** Handles Billboard Raycast */
	void BillboardRaycast();

  /** Determines behaviour of the object hitted by the Raycast. */
  virtual void RaycastOnHoverHitObject();

  /* ----------------------------------------------------------------------- */

  /** Maximum distance of the Raycast. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Raycast")
    float RaycastDistance;

  /** Stores data relative to the object hitted by the Raycast. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Raycast")
    FHitResult HitData;

  /** Wether the Raycast hitted a object or not. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Raycast")
    bool bRaycastHitObject;

  /** Raycast hitted object reference. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Raycast")
    AIC_Object* ObjectHitByRaycast;

  UPROPERTY(BlueprintReadOnly)
    AIC_Trigger* TriggerHitByRaycast;

  UPROPERTY(BlueprintReadOnly)
    AIC_Character_Squirrel* SquirrelHitByRaycast;

  /** Color of the object when being hit by the raycast. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Raycast")
    FLinearColor HoverColor;

  ////////////////////////////////////////////////////////////////////////
  // Possess Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Changes possession between characters */
  UFUNCTION(BlueprintCallable, Category = "IC_Character")
  virtual void PossessAnimal(ECharacterType Creature, bool bForce = false);

  /** Allow player to select an animal to possess or recover */
  virtual void InteractMenu();

  /** Change bAcceptMenu to true */
  virtual void AcceptMenu();

  /** Change bRecoverMenu to true */
  virtual void RecoverMenu();

  /** Change selected animal location to another near the player */
  virtual void Recover(AIC_Character *animal);

  /** Change menu visibility between true and false */
  virtual void ToggleMenu();

  /** Move possession menu one position left */
  virtual void MenuLeft();

  /** Move possession menu one position right */
  virtual void MenuRight();

  virtual void PossessedBy(AController* NewController) override;

  virtual void UnPossessed() override;

  /* ----------------------------------------------------------------------- */

  /** Fade duration between possessions */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
    float FadeTime;

  /** Fade color between possessions */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
    FLinearColor FadeColor;

  /** Whether accept button is pressed */
  UPROPERTY(BlueprintReadWrite, Category = "Config | Possession")
    bool bAcceptMenu;

  /** Whether recover button is pressed */
  UPROPERTY(BlueprintReadWrite, Category = "Config | Possession")
    bool bRecoverMenu;

  /** Whether possession menu is opened */
  UPROPERTY(BlueprintReadWrite, Category = "Config | Possession")
    bool bToggleMenu;

  ////////////////////////////////////////////////////////////////////////
  // Movement Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Handles moving forward/backward. */
  virtual void MoveForward(float Val);

  /** Handles staffing movement, left and right. */
  virtual void MoveRight(float Val);

  /**
  * Called via input to turn at a given rate.
  * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate.
  */
  virtual void TurnAtRate(float Rate);

  /**
  * Called via input to turn look up/down at a given rate.
  * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate.
  */
  virtual void LookUpAtRate(float Rate);

  /** Replaces character walking speed by running speed. */
  virtual void StartSprint();

  /** Replaces character walking speed by default walking speed. */
  virtual void EndSprint();

  /** Unlocks the movement of the player. */
  UFUNCTION(BlueprintCallable, Category = "IC_Character")
    void EnableMovement(bool enable_mouse_rotation);

  /** Locks the movement of the player. */
  UFUNCTION(BlueprintCallable, Category = "IC_Character")
    void DisableMovement(bool disable_mouse_rotation);

  /* ----------------------------------------------------------------------- */

  /** Default walk speed of this character. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Movement")
    float WalkSpeed;

  /** Speed the character will take when running. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Movement")
    float RunningSpeed;

  /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Camera")
    float BaseTurnRate;

  /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Camera")
    float BaseLookUpRate;

  /** Whether the movement input is disabled or not. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Input")
    bool bDisableMovement;

  /** Whether the rotation input is disabled or not. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Input")
    bool bDisableMouseRotation;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Manages interaction between the Character and the Objects. */
  virtual void Interact();

  /** Base value of brightness of the object hovered by the raycast. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object Interaction")
    float HoverBaseBrightness;

  ////////////////////////////////////////////////////////////////////////
  // Wolf Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  virtual void EnableWolfVision();
  virtual void DisableWolfVision();

  /* ----------------------------------------------------------------------- */

  UPROPERTY(EditAnywhere, Category = "COLLECTION")
    UMaterialParameterCollection * Collection;

  ////////////////////////////////////////////////////////////////////////
  // Interface Functions and Variables
  ///////////////////////////////////////////////////////////////////////

	/** Return Character Weight*/
  UFUNCTION(BlueprintCallable, Category = "Config | Interface")
    virtual float GetWeight() override;
	/** Check bWeightLocked an return Character Weight or 0*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual float GetWeightIn() override;
	/** Check bWeightLocked an return Character Weight or 0*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual float GetWeightOut() override;
	
	/** Return bWeightLocked value*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual bool GetLockWeight() override;
	/** Set true bWeightLocked*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual void LockWeight() override;
	/** Set false bWeightLocked*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual void UnlockWeight() override;
	
	/** Wether weight is locked or not*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | Interface")
		bool bWeightLocked;
	/** Character weight*/
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Interface")
    float Weight;


  ////////////////////////////////////////////////////////////////////////
  // Collision detection Variables
  ///////////////////////////////////////////////////////////////////////

  UPROPERTY()
    UCapsuleComponent* Capsule;

  ////////////////////////////////////////////////////////////////////////
  // Respawn fucntions
  ///////////////////////////////////////////////////////////////////////

  /** Manages the respawn of a character, each one implements this. */
  virtual void Respawn();


	////////////////////////////////////////////////////////////////////////
	// Subtitles fucntions and Variables
	///////////////////////////////////////////////////////////////////////

	UFUNCTION(BlueprintCallable, Category = "Config | Subtitle")
	 void UpdateSubtitleParams(AIC_Character* Character);
	
	bool ActiveSubtitle;
	FVector StartPos;
	FVector EndPos;
	float StartTime;
	float TimeToShowSubtitle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Subtitle")
	float ScrollSpeed;
	
	};
