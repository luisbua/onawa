// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "IC_PlayerController.generated.h"

/**
*
*/
UCLASS()
class THEINDIANCAVE_API AIC_PlayerController : public APlayerController
{
  GENERATED_BODY()

public:
  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  AIC_PlayerController(const FObjectInitializer& ObjectInitializer);

  virtual void SetupInputComponent() override;

  ////////////////////////////////////////////////////////////////////////
  // Input Functions
  ///////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////
  // Movement Input Functions
  ///////////////////////////////////////////////////////////////////////

  /**
  * Input to move the Character Forward/Backwards.
  * @param Val Input Value,
  */
  void MoveForward(float Val);

  /**
  * Input to move the Character Left/Right.
  * @param Val Input Value.
  */
  void MoveRight(float Val);

  /**
  * Input to rotate the camera on the Yaw Axis.
  * @param Rate rate of turn.
  */
  void TurnAtRate(float Rate);

  /**
  * Input to rotate the camera on the Pitch Axis.
  * @param Rate rate of turn.
  */
  void LookUpAtRate(float Rate);

  // TEMPORAL PARA BUILD
  bool bSprint;

  /** Input to make the Character sprint. */
  void StartSprint();

  /** Input to make the Character stop sprinting. */
  void EndSprint();

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Input Functions
  ///////////////////////////////////////////////////////////////////////

  /** Input to interact and grab objects. */
  void InteractObject();

  /** Input to start charging the force to throw objects. */
  void EnableChargingForce();

  /** Input to throw the current grabbed objects */
  void Throw();

  ////////////////////////////////////////////////////////////////////////
  // Squirrel Input Functions
  ///////////////////////////////////////////////////////////////////////

  void StartGlide();
  void EndGlide();

  ////////////////////////////////////////////////////////////////////////
  // Wolf Input Functions
  ///////////////////////////////////////////////////////////////////////

  /**
  * Input to change the wolf status between growl or not.
  */
  void ToggleGrowl();

  ////////////////////////////////////////////////////////////////////////
  // Posession Input Functions
  ///////////////////////////////////////////////////////////////////////

  void AcceptMenu();
  void RecoverMenu();
  void ToggleMenu();

  void MenuLeft();
  void MenuRight();

  void PossessBear();
  void PossessIndian();
  void PossessWolf();
  void PossessSquirrel();

  ////////////////////////////////////////////////////////////////////////
  // Tutorial Input Functions
  ///////////////////////////////////////////////////////////////////////
  
  void NextTutorial(float Val);
  void PreviousTutorial(float Val);
  void CloseTutorial();
};
