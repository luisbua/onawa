// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/IC_Interface.h"
#include "IC_Object.generated.h"

UCLASS()
class THEINDIANCAVE_API AIC_Object : public AActor, public IIC_Interface {

	GENERATED_BODY()

private: 
  /** Initial Scale of this Object (World3D Scale) */
  FVector InitialScale;
  /** When true it will calculate the difference between actual and initial scale. */
  bool bCalculateScaleDifference;
  /** Stores the (InitialScale - CurrentScale) calculation. */
  FVector ScaleDifference;
  /** Wether this object was pickable at creation stage or not. */
  bool bWasPickable;

protected:

  ////////////////////////////////////////////////////////////////////////
  // Protected Functions
  ///////////////////////////////////////////////////////////////////////

  void SetupStaticMeshReference();

public:	

  ////////////////////////////////////////////////////////////////////////
  // Object Components
  ///////////////////////////////////////////////////////////////////////

  // This material should be the material with the fresnel function
  UPROPERTY(BlueprintReadOnly, Category = "Material with highlight")
    UMaterialInstanceDynamic* DynamicMaterial;

  UPROPERTY(VisibleAnywhere, Category = "Object", meta = (DisplayName = "Mesh"))
    UStaticMeshComponent* StaticMesh;

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this actor's properties
  AIC_Object();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Wether the Object can be picked up or not. */
  virtual bool CanPickup();

  /** Wether the Object can be interacted or not. */
  virtual bool CanInteract();

  /** Defines the behaviour of this object when interacted. */
  virtual void Interact();

  /* ----------------------------------------------------------------------- */

  /** Wether this object can be picked up or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object")
    bool bPickable;

  /** Wether this object can be interacted or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object")
    bool bInteractable;

  /** Wether this object should be placed on the indian's right hand or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object")
    bool bBig;

  /** Wether this object should be placed on the indian's right hand or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object")
    FVector TargetScale;

  /** Type of this object. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object", meta = (DisplayName = "Object type"))
    EObjectType Type;

  /** Name of this object. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object", meta = (DisplayName = "Object name"))
    FString Name;

  /** Wether this object should restore it's scale to it's initial value */
  bool bRestoreScale;

  ////////////////////////////////////////////////////////////////////////
  // Object Highlighting Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** 
  * Function to highlight objects when hovering them with a Raycast.
  * @param value emissive power.
  * @param color color of the highlighting.
  */
  virtual void BeginHover(float value, FLinearColor color);

  /** Function to deactivate the highlight effect */
  virtual void EndHover();

  /** Check if the material can handle the highlight effect */
  virtual bool CheckIfMaterialIsValidForHover();

  virtual void ReadMeshesAndLoadMaterials();

  UFUNCTION(BlueprintCallable, Category = "Functions | Highlight")
    virtual void BeginWolfHighlight(float value);

  UFUNCTION(BlueprintCallable, Category = "Functions | Highlight")
    virtual void EndWolfHighlight();

  void SetWolfHighlight();

  /* ----------------------------------------------------------------------- */

  UPROPERTY()
    bool bValidMaterial;

  /** The value that the will be set to the material parameter EmissivePower. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Hover")
    float EmissiveValue;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Highlight")
  TArray<UMaterialInstanceDynamic*> DynamicMaterials;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Highlight")
    float InterpoleVelocity;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Highlight", meta = (DisplayName = "Disolve Radius"))
    float GoalInterpolationValue;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Highlight", meta = (DisplayName = "DEBUG radius"))
    float CurrentInterpolationValue;

  bool bHighlightInterpoleBegin;
  bool bHighlightDissolveFinished;

  ////////////////////////////////////////////////////////////////////////
  // Squirrel navigation through objects Variables
  ///////////////////////////////////////////////////////////////////////

  /** Whether this object can be climbable or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object")
    bool bClimbable;

  ////////////////////////////////////////////////////////////////////////
  // Interface Functions and Variables
  ///////////////////////////////////////////////////////////////////////

	/** Return Object Weight*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual float GetWeight() override;
	/** Check bWeightLocked an return Object Weight or 0*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
    virtual float GetWeightIn() override;
	/** Check bWeightLocked an return Object Weight or 0*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual float GetWeightOut() override;
	
	/** Return bWeightLocked value*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual bool GetLockWeight() override;
	/** Set true bWeightLocked*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual void LockWeight() override;
	/** Set false bWeightLocked*/
	UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		virtual void UnlockWeight() override;
	
	/** Wether weight is locked or not*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | Interface")
		bool bWeightLocked;
	/** Object weight*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | Interface")
		bool bInto;

  /* ----------------------------------------------------------------------- */

  /** Weight of this Object. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Interface")
    float Weight;

};
