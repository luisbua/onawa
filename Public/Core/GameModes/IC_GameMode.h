// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "Characters/IC_Character_Bear.h"
#include "Characters/IC_Character_Indian.h"
#include "Characters/IC_Character_Squirrel.h"
#include "Characters/IC_Character_Wolf.h"
#include "Objects/IC_Object_Static.h"
#include "Objects/IC_Object_Static_AnimalSpawner.h"
#include "Menu/IC_PossessionMenu.h"
#include "TheIndianCave/TheIndianCave.h"
#include "IC_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_GameMode : public AGameMode {
	GENERATED_BODY()
public:
  
  DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPossessionAction, ECharacterType, CharacterType);

  UPROPERTY(BlueprintAssignable, Category = "Config | Events")
    FPossessionAction OnPossess;
  
  DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPossessionMenu, bool, status);

   UPROPERTY(BlueprintAssignable, Category = "Config | Events")
     FPossessionMenu OnPossessionMenu;

  DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSummonAction, ECharacterType, CharacterType);

  UPROPERTY(BlueprintAssignable, Category = "Config | Events")
    FSummonAction OnSummon;

   AIC_GameMode();
   virtual void BeginPlay();

   virtual APawn * SpawnDefaultPawnFor_Implementation(AController *NewPlayer, class AActor *StartSpot) override;
	 virtual class AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName = TEXT("")) override;

	 UFUNCTION()
	 void ObtainAnimalSpawnerReferences();

   virtual void TogglePossessionMenu(bool toggle, const FVector &newPosition, const FRotator &newRotation);

   //TODO: Try to move the functionality from SpawnDefaultPawnFor_Implementation to RestartPlayer
   //void RestartPlayer(AController *New Player) override;

   UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Config | Characters")
     TSubclassOf<AIC_Character_Bear> ABearCharacterClass;

   UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Config | Characters")
     TSubclassOf<AIC_Character_Squirrel> ASquirrelCharacterClass;

   UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Config | Characters")
     TSubclassOf<AIC_Character_Wolf> AWolfCharacterClass;

   UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Config | Characters")
     TSubclassOf<AActor> APossessionMenu;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Characters")
     ECharacterType CharacterTypePossessed;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Animals")
   AIC_Character_Bear *Bear;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Animals")
   AIC_Character_Indian *Indian;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Animals")
   AIC_Character_Wolf *Wolf;
   
   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Animals")
   AIC_Character_Squirrel *Squirrel;

   //Animals that must spawn at start
   UPROPERTY(EditAnywhere, BlueprintReadwrite, Category = "Config | Animals")
    bool bBear;

   UPROPERTY(EditAnywhere, BlueprintReadwrite, Category = "Config | Animals")
    bool bSquirrel;

   UPROPERTY(EditAnywhere, BlueprintReadwrite, Category = "Config | Animals")
    bool bWolf;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Possession")
     AActor* PossessionMenu;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Possession")
     AIC_PossessionMenu* PossessionMenuNew;

   /** Change between DreamCatcher and new Possession Menu */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bChangeMenu;

   /** Whether the player can possess or not */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bCanPossess;

   /** Whether the player can possess the bear or not */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bCanPossessBear;

   /** Whether the player can possess the indian or not */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bCanPossessIndian;

   /** Whether the player can possess the squirrel or not */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bCanPossessSquirrel;

   /** Whether the player can possess the wolf or not */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Possession")
     bool bCanPossessWolf;

   ////////////////////////////////////////////////////////////////////////
   // Respawn and checkpoints
   ///////////////////////////////////////////////////////////////////////
	 UFUNCTION()
		 void CalculateAnimalsRecoverPosition(const FVector& IndianForward, const FVector& IndianLocation);

	 UFUNCTION(BlueprintCallable, Category = "Config | Respawn")
		 void RespawnAnimalsIntoPlatform();

   UFUNCTION(BlueprintCallable, Category = "Config | Respawn")
     void SetIndianCheckpointLocation(FVector location);

   UFUNCTION(BlueprintCallable, Category = "Config | Respawn")
     FVector GetRespawnLocation(ECharacterType type);

   UFUNCTION(BlueprintCallable, Category = "Config | Respawn", meta = (DeprecatedFunction, DeprecationMessage = "The function has been moved to IC Blueprint Library, you must use this one instead. Ask Programmers if you have any doubt."))
     void TeleportAnimalToPosition(ECharacterType type, FVector TargetPosition);

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 FVector BearDistanceToIndian;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 FVector SquirrelDistanceToIndian;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
	   FVector WolfDistanceToIndian;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 AIC_Object_Static_AnimalSpawner* PlatformSpawner;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 FVector SpawnerRecoverBear;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 FVector SpawnerRecoverSquirrel;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
		 FVector SpawnerRecoverWolf;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Respawn")
     bool bTutorialOpened;

   ////////////////////////////////////////////////////////////////////////
   // Exec functions
   ///////////////////////////////////////////////////////////////////////

   UFUNCTION(Exec)
     void EnableAllCharactersMovement();

   UFUNCTION(Exec)
     void DisableAllCharactersMovement();

 private:
   UPROPERTY()
     FVector IndianLastCheckpoint;

   UPROPERTY()
     FVector BearRecoverLocation;

   UPROPERTY()
     FVector SquirrelRecoverLocation;

   UPROPERTY()
     FVector WolfRecoverLocation;
};
