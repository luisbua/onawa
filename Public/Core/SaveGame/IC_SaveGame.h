// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "IC_SaveGame.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_SaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UPROPERTY(VisibleAnywhere, Category = "IC_SaveGame")
		uint32 MapIndex;

	UPROPERTY(VisibleAnywhere, Category = "IC_SaveGame")
		FVector Position;

	UPROPERTY(VisibleAnywhere, Category = "IC_SaveGame")
		TArray<FName> CurrentMaps;
	
	UPROPERTY(VisibleAnywhere, Category = "IC_SaveGame")
		FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = "IC_SaveGame")
		uint32 UserIndex;

	UIC_SaveGame();
};
