// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "IC_Object_Environment.h"
#include "IC_Object_Static_Spawner.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_Spawner : public AIC_Object_Static
{
	GENERATED_BODY()
	
public:

	AIC_Object_Static_Spawner();
	virtual void BeginPlay() override;
	
	/** Function to manage BeginoverLap*/
	UFUNCTION(BlueprintCallable, Category = "Misc")
		void Change(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	/**Staic Mesh*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* BoxSpawner;

	/** SubClassOf to chose object class*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object Spawn")
		TSubclassOf<AIC_Object_Environment> ObjectClass;


	/** Scale the spawned object will take. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object Spawn")
		FVector SpawnedObjectScale;

	/** Rotation the spawned object will take. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Object Spawn")
		FQuat SpawnedObjectRotation;
	
private:
	/** Time Handler*/
	FTimerHandle TimerHandler;
	/** Spawn an Object delay X segs*/
	void DelaySpawnObject();
};
