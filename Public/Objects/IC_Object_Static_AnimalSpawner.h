// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "IC_Object_Static_AnimalSpawner.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_AnimalSpawner : public AIC_Object_Static
{
	GENERATED_BODY()

public:

	////////////////////////////////////////////////////////////////////////
	// Object Static Components
	///////////////////////////////////////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		USceneComponent* BearSpawnLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		USceneComponent* SquirrelSpawnLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		USceneComponent* WolfSpawnLocation;

	////////////////////////////////////////////////////////////////////////
	// Default UE4 Functions / Constructor / BeginPlay / Tick
	///////////////////////////////////////////////////////////////////////

	AIC_Object_Static_AnimalSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;	
};
