// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Objects/IC_Object.h"
#include "IC_Object_Environment.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Environment : public AIC_Object
{
	GENERATED_BODY()
	
public:
  AIC_Object_Environment();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  virtual bool CanPickup();

  /** Wether the Object can be interacted or not. */
  virtual bool CanInteract();

  virtual void Interact();

  virtual void BeginHover(float value, FLinearColor color);

  virtual void EndHover();
};
