// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Objects/IC_Object.h"
#include "Components/WidgetComponent.h"
#include "IC_Object_Menu.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Menu : public AIC_Object
{
	GENERATED_BODY()
public:
  AIC_Object_Menu();

  void Tick(float DeltaSeconds) override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Widget")
    UBoxComponent* BoxCollision;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Widget")
    USpotLightComponent* SpotLight;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Widget")
    bool bHovered;

  UFUNCTION(BlueprintCallable, Category = "Config | Menu")
    bool CanInteract();

  UFUNCTION(BlueprintCallable, Category = "Config | Menu")
    void Interact();
	
  UFUNCTION(BlueprintNativeEvent, Category = "Config | Widget")
    void Execute();

  UFUNCTION(BlueprintNativeEvent, Category = "Config | Static Menu Object")
    void AfterBeginPlay();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Widget")
    bool bAfterBeginPlay;
};
