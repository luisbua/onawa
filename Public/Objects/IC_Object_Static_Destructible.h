// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "Core/Characters/IC_Character.h"
#include "IC_Object_Static_Destructible.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_Destructible : public AIC_Object_Static {
	GENERATED_BODY()
	
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDestructibleInteractor);

public:

  ////////////////////////////////////////////////////////////////////////
  // Object Static Components
  ///////////////////////////////////////////////////////////////////////

  UPROPERTY(VisibleAnywhere)
    UDestructibleComponent* DestructibleMesh;

  /** Spawn point from where the Objects will be spawned. */
  UPROPERTY(VisibleAnywhere)
    UBoxComponent* SpawnPoint;

  ////////////////////////////////////////////////////////////////////////
  // Object Spawner Properties
  ///////////////////////////////////////////////////////////////////////

  /* Wether this Breakable will spawn an object after fracturing. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Spawn")
    bool bSpawnObject;

  /** Blueprint of the Object to spawn. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Spawn")
    bool bSpawnAtBeginPlay;

  /** Scale the spawned object will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Spawn")
    FVector SpawnedObjectScale;

  /** Rotation the spawned object will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Spawn")
    FQuat SpawnedObjectRotation;

  /** Blueprint of the Object to spawn. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Spawn")
    TSubclassOf<AIC_Object> ObjectToSpawn;

  // Spawned Object Reference
  AIC_Object* SpawnedObject;

  ////////////////////////////////////////////////////////////////////////
  // Object Spawner Functions
  ///////////////////////////////////////////////////////////////////////

  /** Gets a reference of the SpawnedObject of this Instance. */
  UFUNCTION(BlueprintCallable, Category = "IC_Object_Static_Destructible")
    AIC_Object* GetSpawnedObject(bool &IsValid);

  // Spawns the object at Spawner location.
  void SpawnObject();

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  AIC_Object_Static_Destructible();
	
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  virtual bool CanInteract();

  /** Defines the behaviour of this object when interacted. */
  virtual void Interact();

  /** Spawns an object if bSpawn is true. If bSpawnAtBeginPlay is true
      it will set the object physics back to true. */
  UFUNCTION(BlueprintCallable, Category = "Functions | Destructible")
    void OnFracture();

  /** Breaks the object according BaseDamage, DamageRadius and ImpulseStrenght. */
  UFUNCTION(BlueprintCallable, Category = "Functions | Destructible")
  /*virtual*/ void Break();

  /** Whether this object has to break when interacted or throw an event. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Interaction")
    bool bBreakOnInteract;

  /** Whether this object can be interacted by the indian or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Interaction")
    bool bInteractableByIndian;

  /** Whether this object can be interacted by the bear or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Interaction")
    bool bInteractableByBear;

  ////////////////////////////////////////////////////////////////////////
  // Radius Damage Parameters for Destructible Mesh
  ///////////////////////////////////////////////////////////////////////

  /** Base damage which will be applied when the Bear interacts with it. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Damage")
    float BaseDamage;

  /** Radius of the damage applied to the Destructible. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Damage")
    float DamageRadius;

  /** Strenght of the impulse applied to the Destructible. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Destructible | Damage")
    float ImpulseStrength;

  ////////////////////////////////////////////////////////////////////////
  // Events
  ///////////////////////////////////////////////////////////////////////

  /** Event dispatched when the Destructible fractures */
  UPROPERTY(BlueprintAssignable, Category = "IC_Static_Destructible")
    FDestructibleInteractor OnBreak;


private:
  // True if this object has been fractured.
  bool bFractured;
};
