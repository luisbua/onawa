// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Objects/IC_Object.h"
#include "IC_Object_Static.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInteractor);
/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static : public AIC_Object {
  GENERATED_BODY()
    
public:
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FReachAltarDelegate);
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLeaveAltarDelegate);

  ////////////////////////////////////////////////////////////////////////
  // Object Static Components
  ///////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  AIC_Object_Static();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  virtual void OnConstruction(const FTransform& Transform) override;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Wether the Object can be picked up or not. */
  virtual bool CanPickup();

  virtual bool CanInteract();

  /** Defines the behaviour of this object when interacted. */
  virtual void Interact();

  ////////////////////////////////////////////////////////////////////////
  // Object Highlighting Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /**
  * Function to highlight objects when hovering them with a Raycast.
  * @param value emissive power.
  * @param color color of the highlighting.
  */
  void BeginHover(float value, FLinearColor color);

  /** Function to deactivate the highlight effect */
  void EndHover();

  ////////////////////////////////////////////////////////////////////////
  // Event Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  UFUNCTION(BlueprintCallable, Category = "Config | Static Object")
    void MovementBear(float MovementDistance);

  UFUNCTION(BlueprintCallable, Category = "Config | Static Object")
    void ChangeColorStatus(AIC_Object_Static *object, FLinearColor correctColor, FLinearColor incorrectColor);
  
  UFUNCTION(BlueprintCallable, Category = "Config | Static Object")
    void ChangeColor(AIC_Object_Static *object, FLinearColor color, bool activate);

  UFUNCTION(BlueprintCallable, Category = "Config | Static Object")
    bool CheckColorForPuzzle(TArray<AIC_Object_Static*> object);

  /* ----------------------------------------------------------------------- */

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Static Object")
    bool bColor;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Static Object")
    bool bMovementObject;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Static Object")
    FVector NextPosition; // NextPositionAfterBearPush // bChangeObjectColor

  UPROPERTY(BlueprintAssignable, Category = "Config | Static Object")
    FInteractor ActivateEvent;

  UPROPERTY(BlueprintAssignable, Category = "Config | Static Object")
    FInteractor ActivateEventBear;

  UPROPERTY(BlueprintAssignable, Category = "Config | Static Object")
    FInteractor ActivateEventSquirrel;
    

  //////////////////////////////////////////////////////////////////////////
  // UNDER DEVELOPMENT
  ////////////////////////////////////////////////////////////////////////

  UFUNCTION()
    void ObjectReachHolder();

  UFUNCTION()
    void ObjectLeavesHolder();

  /** This event will be used only when the object has attached a IC_Object_Holder */
  UPROPERTY(BlueprintAssignable, Category = "Events")
    FReachAltarDelegate OnObjectReachHolder;

  /** This event will be used only when the object has attached a IC_Object_Holder */
  UPROPERTY(BlueprintAssignable, Category = "Events")
    FLeaveAltarDelegate OnObjectLeavesHolder;
};
