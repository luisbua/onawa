// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "IC_Object_Static_Rocker.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_Rocker : public AIC_Object_Static
{
	GENERATED_BODY()
public:
	////////////////////////////////////////////////////////////////////////
	// Object Static Components
	///////////////////////////////////////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		UStaticMeshComponent* LeftPaddle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		UStaticMeshComponent* RightPaddle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		UBoxComponent* LeftCollider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Components")
		UBoxComponent* RightCollider;

	////////////////////////////////////////////////////////////////////////
	// Default UE4 Functions / Constructor / BeginPlay / Tick
	///////////////////////////////////////////////////////////////////////

	AIC_Object_Static_Rocker();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	////////////////////////////////////////////////////////////////////////
	// Configuration variables
	///////////////////////////////////////////////////////////////////////
	/// Maximum height that all paddle can reach
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fMaxHeight;

	/// Minimum height that all paddle can reach
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fMinHeight;

	/// Mid height that all paddle reach at same weight
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fMidHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fLeftWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fRightWeight;

	/// Interpolation speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		float fInterpolationSpeed;

	/// Left paddle initial location in world coordinates
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		FVector LeftPaddleInitialLocation;

	/// Right paddle initial location in world coordinates
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Configuration")
		FVector RightPaddleInitialLocation;

	////////////////////////////////////////////////////////////////////////
	// Configuration methods
	///////////////////////////////////////////////////////////////////////

	/// Reset two paddle position to the initial
	UFUNCTION()
		void ReadjustInitialPosition();

	/** 
		Calculate the weight over determinate paddle
		@param paddle UStaticMeshComponent that will calculate the weight over it
		@param weight that will be modified
	*/
	UFUNCTION()
		float CalculateWeightOverPaddle(UStaticMeshComponent* paddle);

	/**
		Distribute the total height based on weight
		@param DeltaSeconds 
	*/
	UFUNCTION()
		void DistributeHeightUsingWeight(float DeltaSeconds);

	/**
		Interpolate the height between two paddles and move them
		@param PaddleUp StaticMeshComponent reference to the paddle that will be moved up
		@param PaddleUp StaticMeshComponent reference to the paddle that will be moved down
		@param DeltaSeconds
	*/
	UFUNCTION()
		void InterpAndMove(UStaticMeshComponent* PaddleUp, UStaticMeshComponent* PaddleDown, float DeltaSeconds, float MinHeight, float MaxHeight);

};
