// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Objects/IC_Object.h"
#include "Objects/IC_Object_Static.h"
#include "../Public/Components/IC_ObjectHolderComponent.h"
#include "IC_Object_Mission.generated.h"

/**
*
*/
UCLASS()
class THEINDIANCAVE_API AIC_Object_Mission : public AIC_Object {

  GENERATED_BODY()

public:
  DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTargetDetection, AIC_Object_Mission*, ThisObject, AIC_Object_Static*, OtherObject);
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInteractor);
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPickUp);

private:

  ////////////////////////////////////////////////////////////////////////
  // Private Position Tracking Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Adds current object position to the positions vector. */
  void TrackPositions();

  /** Checks if the object has reached desired Z position to start returning back. */
  void ObjectReachedZDistanceToReturnBack();

  /**
  * Takes the positions vector and starts setting this object position from the
  * latest position index to the first one. This makes the object 'go back in time'.
  */
  void InterpolateToInitialPosition();

  /**
  * Shortcut for using TrackPositions, ObjectReachedZDistanceToReturnBack and
  * InterpolateToInitialPosition functions.
  */
  void ReturnObjectAfterFalling();

  /**
  * Starts tracking the object positions if the object has any velocity, meaning that
  * the player or the environment is moving it. If the object goes back to 0 velocity
  * the positions vector gets cleared.
  */
  void AutoTracking();

  /* ----------------------------------------------------------------------- */

  /** References of meshes for easy turn on turn off properties. */
  UPROPERTY()
    TArray<UStaticMeshComponent*> DisabledMeshes;

  /** Used to reverb the positions array to start moving from back to start. */
  int PositionsPaddingIndex;

  /** Wether the object will start interpolating back or not. */
  bool bStartInterpolatingObject;

  /** To prevent object continuous falling loop we track the player head position. */
  bool bPlayerHeadHasBeenTracked;

  /** Z distance above the player when tracking his head position (like a offset) */
  float ZDistanceAbovePlayer;

  /** Current velocity of this object. */
  FVector Velocity;

public:

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  AIC_Object_Mission();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Public Position Tracking Variables
  ///////////////////////////////////////////////////////////////////////

  /** Distance on Z axis where this object will start to return back. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Objects")
    bool bToolbeltItem;

  /** Distance on Z axis where this object will start to return back. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Physics")
    float ZDistanceToReturnBack;

  /** Vector of positions used to track the latest positions of this object. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Physics")
    TArray<FVector> TrackedPositions;

  /** Wether this object has been thrown or not by the player. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Physics")
    bool bThrowed;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Wether the Object can be picked up or not. */
  virtual bool CanPickup();

  /** Wether the Object can be interacted or not. */
  virtual bool CanInteract();

  /** Defines the behaviour of this object when interacted. */
  virtual void Interact();

  void LockInteractionAndPickup();

  void UnlockInteractionAndPickup();

  /* ----------------------------------------------------------------------- */

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FInteractor OnActivate;

  bool bLockInteractionAndPickup;

  ////////////////////////////////////////////////////////////////////////
  // Collision interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  UFUNCTION()
    void CheckForOverlapedTargets();

  UFUNCTION()
    void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndOverlap(class UPrimitiveComponent* HitComp,
      class AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex);

  /* ----------------------------------------------------------------------- */

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    UCapsuleComponent* CollisionDetector;

  UPrimitiveComponent* MyPrimitiveComponent;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Interaction", meta = (DisplayName = "Object to interact"))
    TArray<class AIC_Object_Static*> InteractionTargets;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Interaction", meta = (DisplayName = "Start with detection enabled"))
    bool bDetectionEnabled;

  //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Interaction")
  bool bTargetDetected;

  int32 TargetIndex;

  int countdown;

  ////////////////////////////////////////////////////////////////////////
  // Object Highlighting Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /**
  * Function to highlight objects when hovering them with a Raycast.
  * @param value emissive power.
  * @param color color of the highlighting.
  */
  virtual void BeginHover(float value, FLinearColor color);

  virtual void EndHover();

  ////////////////////////////////////////////////////////////////////////
  // Placing in holder Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  void SetupHolder();

  void StartHolder();

  UFUNCTION(BlueprintCallable, Category = "Config | Holder")
    void RemoveHolder(int32 holder_index);

  void LockInLocation();

  void UnlockInLocation();

  void BroadcastPickUpEvent();

  /* ----------------------------------------------------------------------- */

  //UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | When in holder")
  bool bLockedInLocation;

  ////////////////////////////////////////////////////////////////////////
  // Target Detection Event Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  void BroadcastTargetDetectionEvent();

  /* ----------------------------------------------------------------------- */

  UPROPERTY(BlueprintAssignable, Category = "Config | Holder")
    FTargetDetection TargetDetectionEvent;

  UPROPERTY(BlueprintAssignable, Category = "Config | Holder")
    FPickUp PickUpEvent;

  bool bBroadcastEventOnce;
};
