// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Objects/IC_Object.h"
#include "Core/Characters/IC_Character.h"
#include "IC_Object_Breakable.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Breakable : public AIC_Object {
  GENERATED_BODY()

  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBreakableInteractor);

public:
  ////////////////////////////////////////////////////////////////////////
  // Object Components
  ///////////////////////////////////////////////////////////////////////

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
  UDestructibleComponent* DestructibleMesh;

  /** Spawn point from where the Objects will be spawned. */
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    UBoxComponent* SpawnPoint;

  ////////////////////////////////////////////////////////////////////////
  // Object Spawner Properties
  ///////////////////////////////////////////////////////////////////////

  /* Wether this Breakable will spawn an object after fracturing. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Breakable")
    bool bSpawnObject;

  /** Scale the spawned object will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Breakable")
    FVector SpawnedObjectScale;

  /** Rotation the spawned object will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Breakable")
    FQuat SpawnedObjectRotation;

  /** Blueprint of the Object to spawn. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Breakable")
    TSubclassOf<AIC_Object> ObjectToSpawn;

  // Spawned Object Reference
  AIC_Object* SpawnedObject;

  ////////////////////////////////////////////////////////////////////////
  // Object Spawner Functions
  ///////////////////////////////////////////////////////////////////////

  /** Gets a reference of the SpawnedObject of this Instance. */
  UFUNCTION(BlueprintCallable, Category = "IC_Object_Breakable")
    AIC_Object* GetSpawnedObject(bool &IsValid);

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this actor's properties
  AIC_Object_Breakable();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Wether the Object can be picked up or not. */
  virtual bool CanPickup() override;

  ////////////////////////////////////////////////////////////////////////
  // Destructible Behaviour
  ///////////////////////////////////////////////////////////////////////

  /** Changes linear damping and bDestroyed on breaking object event. */
  UFUNCTION(BlueprintCallable, Category = "Breakable")
  void OnFracture();

  // Wether this object have been destroyed or not. (One hit destruction).
  bool bDestroyed;
  
  ////////////////////////////////////////////////////////////////////////
  // Events
  ///////////////////////////////////////////////////////////////////////

  /** Event dispatched when the Breakable fractures. */
  UPROPERTY(BlueprintReadOnly, BlueprintAssignable, Category = "IC_Static_Destructible")
    FBreakableInteractor OnBreak;
};
