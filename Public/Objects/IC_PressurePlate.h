// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "IC_PressurePlate.generated.h"

/**
 *
 */
UCLASS()
class THEINDIANCAVE_API AIC_PressurePlate : public AIC_Object_Static
{
	GENERATED_BODY()
public:

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPressureEvent);

	////////////////////////////////////////////////////////////////////////
	// Pressure Plate Components
	///////////////////////////////////////////////////////////////////////

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	/** Used to detect the objects placed above. */
	UPROPERTY(VisibleAnywhere)
		UBoxComponent* WeightDetector;

	////////////////////////////////////////////////////////////////////////
	// Default UE4 Functions / Constructor / BeginPlay / Tick
	///////////////////////////////////////////////////////////////////////

  // Sets default values for this character's properties
	AIC_PressurePlate();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	////////////////////////////////////////////////////////////////////////
  // Weight Detection Functions and Variables
  ///////////////////////////////////////////////////////////////////////

	/** Gets the weight of the passed OtherActor and adds it to the
		* AccumulatedWeight property.
		*/
	UFUNCTION(BlueprintCallable, Category = "Config | PressurePlate")
		void UpperCollisionBeginOverlap(class UPrimitiveComponent* HitComp,
																	  class AActor* OtherActor,
																	  class UPrimitiveComponent* OtherComp,
																		int32 OtherBodyIndex,
																		bool bFromSweep,
																		const FHitResult & SweepResult);

	/** Gets the weight of the passed OtherActor and removes it from
		* AccumulatedWeight property.
		*/
	UFUNCTION(BlueprintCallable, Category = "Config | PressurePlate")
		void UpperCollisionEndOverlap(class UPrimitiveComponent* HitComp,
																 	class AActor* OtherActor,
																	class UPrimitiveComponent* OtherComp,
																	int32 OtherBodyIndex);

	/* ----------------------------------------------------------------------- */

	/** Weight needed to trigger. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | PressurePlate")
		float ActivationWeight;

	/** Accumulated Weight on the Pressure Plate. */
	UPROPERTY(BlueprintReadOnly, Category = "Config | PressurePlate")
		float AccumulatedWeight;

	/** Displacement the Plate will take when triggered. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | PressurePlate")
    FVector PlateActivationDisplacement;

	/** Velocity to Up/Down the Plate*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | PressurePlate")
		float VelocityPlate;
	
	/** Events from Deactivated PressurePlate */
	UPROPERTY(BlueprintAssignable, Category = "Config | PressurePlate")
		FPressureEvent UnPressed;
	/** Events from Activated PressurePlate */
	UPROPERTY(BlueprintAssignable, Category = "Config | PressurePlate")
		FPressureEvent Pressed;

	/*-----------------------------------*/

private:
  bool bPressed;
	/// Initial position of the PressurePlate
	FVector PlateStartLocation;
	/// Destination position when the PressurePlate triggers to make the transition.
	FVector PlateEndLocation;

};
