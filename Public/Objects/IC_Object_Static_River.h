// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "Core/Objects/IC_Object.h"
#include "IC_Object_Static_River.generated.h"

/**
*
*/
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_River : public AIC_Object_Static
{
	GENERATED_BODY()

public:

	////////////////////////////////////////////////////////////////////////
	// River Components
	///////////////////////////////////////////////////////////////////////

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* DetectorBox;


	////////////////////////////////////////////////////////////////////////
	// Default UE4 Functions / Constructor / BeginPlay / Tick
	///////////////////////////////////////////////////////////////////////

	// Sets default values for this character's properties
	AIC_Object_Static_River();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	////////////////////////////////////////////////////////////////////////
	// River Functions and Variables
	///////////////////////////////////////////////////////////////////////

	/** Set var RockIn to True and var RiverOffset to Rock position*/
	UFUNCTION(BlueprintCallable, Category = "Config | River")
		void DetectorBoxBeginOverlap(class UPrimitiveComponent* HitComp,
			class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult & SweepResult);

	/** Set var RockIn to False*/
	UFUNCTION(BlueprintCallable, Category = "Config | River")
		void DetectorBoxEndOverlap(class UPrimitiveComponent* HitComp,
			class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

	/* ----------------------------------------------------------------------- */

	/** To modify de Heigth of the River*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | River")
		float RiverHeigth;
	/** To set the point where the river is cut*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | River")
		float RiverOffset;

	/** To check if any rock blocks the River*/
	UPROPERTY(BlueprintReadWrite, Category = "Config | River")
		bool RockIn;

	/** Control class type of Rock*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | River")
		TSubclassOf<AIC_Object> BlockerObject;

	/** River Mesh Length obtained from StaticMesh(river)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | River")
		float RiverLength;
	
	/** Max river Depht */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | River")
		float MaxDepht;
	/** Water level velocity descend*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | River")
		float DownVelocity;

private:
	/** To add Dynamic Material*/
	UMaterialInstanceDynamic* DynamicMaterialRiver;
};
