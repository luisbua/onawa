// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Objects/IC_Object_Static.h"
#include "IC_Object_Static_Invisible.generated.h"

class AIC_GameMode;


/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Object_Static_Invisible : public AIC_Object_Static
{
	GENERATED_BODY()
 public:
   DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FObjectReveal, AIC_Object*, Object);

   ////////////////////////////////////////////////////////////////////////
   // Default UE4 Functions / Constructor / BeginPlay / Tick
   ///////////////////////////////////////////////////////////////////////

   AIC_Object_Static_Invisible();

   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

   // Called every frame
   virtual void Tick(float DeltaSeconds) override;
	
   ////////////////////////////////////////////////////////////////////////
   // Visualization functions
   ///////////////////////////////////////////////////////////////////////

   UFUNCTION(BlueprintCallable, Category = "Functions | Visibility")
    void Hide(bool force);

   UFUNCTION(BlueprintCallable, Category = "Functions | Visibility")
    void Show();

   void ForceShow();

   UFUNCTION()
    bool DetectOnScreen();

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Invisibility")
    bool bRevealOnReachScreenSpace;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Invisibility")
    bool bKeepVisibleAfterReveal;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Invisibility")
     bool bSetupAsClue;

   UPROPERTY(BlueprintAssignable, Category = "Object Events")
    FObjectReveal RevealEvent;

 private:
   bool bRevealed;
   AIC_GameMode* GameMode_;
};
