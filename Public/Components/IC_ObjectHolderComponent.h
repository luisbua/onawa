// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "IC_ObjectHolderComponent.generated.h"


class AIC_Object_Mission;
class AIC_Object_Static;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THEINDIANCAVE_API UIC_ObjectHolderComponent : public USceneComponent
{
  GENERATED_BODY()

public:
  //DECLARE_DYNAMIC_MULTICAST_DELEGATE(FReachHolderDelegate);
  //DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLeaveHolderDelegate);

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this component's properties
  UIC_ObjectHolderComponent();

  // Called when the game starts
  virtual void BeginPlay() override;

  // Called every frame
  virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

  ////////////////////////////////////////////////////////////////////////
  // Place / Rotate Item Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Speed used to interpol the location and rotation of the object detected. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Item movement")
    float InterpolationSpeed;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Item movement")
    float ReachTolerance;

  /** Whether the object detected will block its location and rotation. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Holder settings")
    bool bBlockMovement;

  /** Whether the object detected will be unable to receive any interaction from player. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Holder settings")
    bool bBlockItemInteraction;

  /** Whether the object detected will be stopped when reaches the holder. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Holder settings")
    bool bStopInertia;

  /** Add a new object to ObjectsToPlace array and assign the object detection event. */
  UFUNCTION(BlueprintCallable, Category = "Config | Object Holder")
    void SetObjectToPlace(AIC_Object_Mission* ObjectReference);


  /** Function assigned to objects in ObjectsToPlace array. 
  *   It setup the configuration to start the interpolation of the object.
  *   @param HolderId Unique id to ensure whether the object detected this holder.
  *   @param ObjectId Unique id to check if the object that had broadcast the event is in the array.
  */
  UFUNCTION(BlueprintCallable, Category = "Config | Object Holder")
    void PlaceItem(int32 ObjectId);


  /** Whether the object detected is being placed. */
  bool bPlacingItem;

  /** Whether the object detected is in the desired location. */
  bool bItemInLocation;

  /** Array of items related to this holder. */
  TArray<AIC_Object_Mission*> ObjectsToPlace;

  /** Reference of the object that is being interpolated. */
  AIC_Object_Mission* CurrentObjectReference;

  /** Goes through ObjectsToPlace array looking for an equal id.
  *   It checks ue4 unique ids comparing the passed id with the references
  *   in the array.
  *   @param ObjectID ue4 unique id to compare with elements in array.
  */
  void CheckItemHaveToBePlaced(unsigned int ObjectID);


  /** Moves the object detected to this component location. */
  void InterpoleItemToLocationAndRotation();

  /** Checks if the object reached the location and configure the holder. */
  void ItemReachedLocation();

  ////////////////////////////////////////////////////////////////////////
  // Events Functions and Variables
  ///////////////////////////////////////////////////////////////////////
  
  AIC_Object_Static* ParentObject;
};