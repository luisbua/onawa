// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AIC_Character_Indian;

#include "GameFramework/SpringArmComponent.h"
#include "IC_CarryObjectComponent.generated.h"

UCLASS()
class THEINDIANCAVE_API UIC_CarryObjectComponent : public UActorComponent {
  GENERATED_BODY()

public:

  ////////////////////////////////////////////////////////////////////////
  // Carry Object Component Components
  ///////////////////////////////////////////////////////////////////////

  // Reference of the IndianCharacter.
  AIC_Character_Indian* IndianReference;

  /** PhysicsHandle used for grabbing Environment objects. */
  UPROPERTY(EditAnywhere)
    UPhysicsHandleComponent* PhysicsHandle;

  /** Component used to set the position of the grabbed object. */
  UPROPERTY()
    USceneComponent* ObjectHolder;

  /** Component used to set the position of the bigger grabbed objects. */
  UPROPERTY()
    USceneComponent* BigObjectHolder;

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this component properties.
  UIC_CarryObjectComponent(const FObjectInitializer& ObjectInitializer);

  // Called when a component is registered, after Scene is set.
  virtual void OnRegister() override;

  // Initializes the component. Occurs at level startup. 
  virtual void InitializeComponent() override;

  // Called when the game starts or when spawned.
  virtual void BeginPlay() override;

  // Called every frame.
  virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

  ////////////////////////////////////////////////////////////////////////
  // Picking up / Throwing / Rotating Objects Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** 
  * Manages the grabbing of objects using the PhysicsHandle
  * @param RaycastData data of the character raycast. Used to detect
  *        the objects which are valid to pick up.
  */
  void GrabObject(FHitResult* RaycastData);

  /**
  * Throw the carried object to the specified direction.
  * @param direction direction to throw the object at.
  */
  void ThrowObject(FVector direction);

  /** Accumulates the force which will be applied when throwing the object. */
  void ChargeObjectThrowForce();

  /** Sets the grabbed object position and rotation according ObjectHolder */
  void SetPhysicsHandleTargetLocationAndRotation();

  /** 
  * Put the specified object in Yuma toolbelt.
  * @param obj object to put in Yuma's toolbelt.
  */
  void PutObjectInToolbelt(AIC_Object* obj);

  /** Removes the object in the toolbelt and puts it in the yuma's hands. */
  void PickObjectOfToolbelt();

  /** Gives any specified object by param to Yuma's hands. */
  void PickAnyObject(AIC_Object* obj);

  /* ----------------------------------------------------------------------- */
  
  /** Object reference of object hitted by the Raycast. */
  UPROPERTY(BlueprintReadOnly)
    UPrimitiveComponent* PhysicsObject;

  /** Object reference of object hitted by the Raycast. */
  UPROPERTY(BlueprintReadOnly)
    AIC_Object* GrabbedObject;

  UPROPERTY(BlueprintReadOnly)
    AIC_Object* ToolbeltItem;

  UPROPERTY(BlueprintReadOnly)
    AIC_Object* TemporalOnHoldObject;

  void ThrowTemporalObject();
  void GrabTemporalObject();

  /** Location of the grabbed object. To be used by the PhysicsHandle. */
  UPROPERTY(BlueprintReadOnly)
    FVector GrabbedObjectLocation;

  /** Wether the character is holding a object or not. */
  UPROPERTY(BlueprintReadOnly)
    bool bHoldObject;

  /** Wether the PhysicsHandle is active or not. */
  UPROPERTY(BlueprintReadOnly)
    bool bPhysicsHandleActive;

  /** Maximum object pickup distance. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float PickupDistance;

  /** Rate of charging force. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float ChargeForceRate;

  /** Maximum throw force. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float MaximumThrowForce;

  /** Rotation speed of the object when in inspecting mode. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float ObjectRotationSpeed;

  /** Force multiplier when throwing a Breakable Object. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float BreakableForceMultiplier;

  /** Force multiplier when throwing a Environment Object. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
    float EnvironmentForceMultiplier;

  /** Accumulated force to be applied to the picked up object when throwing. */
  float ThrowForce;
  /** Wether the force is being charged or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CarryObjectComponent")
  bool bChargeForce;

  /** Wether the grabbed component is a breakable object or not. */
  bool bGrabBreakable;
  /** Wether an object is already in the toolbet or not. */
  bool bGotToolbeltItem;

  ////////////////////////////////////////////////////////////////////////
  // Class Setters
  ///////////////////////////////////////////////////////////////////////

  /** Sets the USceneComponent reference used to put the grabbed object.
      @param grab_point default grab point
      @param big_grab_point grab point for bigger objects. */
  void SetGrabPoint(USceneComponent* grab_point, USceneComponent* big_grab_point);

  /** Sets a reference to the Indian Character. */
  void SetIndianReference(AIC_Character_Indian* indian_reference);

  private: 
    /** Wether the grabbed component is the Squirrel or not. */
    bool bGrabSquirrel;
    /** Wether the grabbed component is an object or not. */
    bool bGrabObject;
    /** Wether the grabbed component is a big object or not. */
    bool bBigObject;
    /** When true the object will teleport to its object holder (ignoring Physics handle) */
    bool bTeleport;
};
