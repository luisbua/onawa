// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IC_UserWidget.h"
#include "IC_UserWidget_Button.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_UserWidget_Button : public UIC_UserWidget
{
	GENERATED_BODY()
public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Button")
    UButton* Button;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Button")
    FLinearColor ButtonColorHover;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Button")
    FLinearColor ButtonColorUnhover;
};
