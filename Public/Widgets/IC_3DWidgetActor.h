// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WidgetComponent.h"
#include "IC_3DWidgetActor.generated.h"

UCLASS()
class THEINDIANCAVE_API AIC_3DWidgetActor : public AActor
{
	GENERATED_BODY()
	
public:	
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    UWidgetComponent* WidgetComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    bool bVisible;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    float WidgetDistance;

	// Sets default values for this actor's properties
	AIC_3DWidgetActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
