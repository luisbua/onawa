// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "Widgets/IC_3DSubtitleWidget.h"
#include "IC_HUD.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_HUD : public AHUD {
	GENERATED_BODY()
	
public:

    AIC_HUD();

  UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AIC_HUD")
    UIC_3DSubtitleWidget* SubtitleWidget;

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void ShowSubtitle();

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void HideSubtitle();

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void NextTutorial();

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void PreviousTutorial();

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void ShowTutorial();

  UFUNCTION(BlueprintImplementableEvent, Category = "AIC_HUD")
    void HideTutorial();

};
