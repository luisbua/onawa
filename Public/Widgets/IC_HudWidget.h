// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "IC_HudWidget.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_HudWidget : public UUserWidget
{
	GENERATED_BODY()

public:
		UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void SetSubtitleText(const FString &cad);
	
};
