// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Widgets/IC_3DSubtitleWidget.h"
#include "WidgetComponent.h"
#include "IC_3DSubtitleActor.generated.h"

USTRUCT()
struct FSubtitleStack {
  GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FText> Subtitles;
};

UCLASS()
class THEINDIANCAVE_API AIC_3DSubtitleActor : public AActor
{
	GENERATED_BODY()
	
public:	

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    UWidgetComponent* WidgetComponent;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    TArray<FSubtitleStack> SubtitleStack;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    bool bVisible;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | 3DWidget")
    float SubtitleDistance;

	// Sets default values for this actor's properties
	AIC_3DSubtitleActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

  int CurrentSubtitleIndex;
  int CurrentSubtitleStackIndex;

  UFUNCTION(BlueprintCallable, Category = "Subtitles")
    void Continue(bool& all_subtitles_completed);

  UFUNCTION(BlueprintCallable, Category = "Subtitles")
    void NextStack();

  UFUNCTION(BlueprintCallable, Category = "Subtitles")
    void SetSubtitleIndex(int index);

  UFUNCTION(BlueprintCallable, Category = "Subtitles")
    void SetSubtitleStackIndex(int index);
	
};
