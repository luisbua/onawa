// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "IC_3DSubtitleWidget.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_3DSubtitleWidget : public UUserWidget {
	GENERATED_BODY()
	
public:

  /** FText value displayed by this widget */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Text")
    FText DisplayText;

  /** Retrieves FText value currently held in DisplayText */
  UFUNCTION(BlueprintPure, Category = "Config | Text")
    FText GetDisplayText() const;

  /** Assigns passed FText to DisplayText */
  UFUNCTION(BlueprintCallable, Category = "Config | Text")
    void SetDisplayText(const FText NewDisplayText);

};
