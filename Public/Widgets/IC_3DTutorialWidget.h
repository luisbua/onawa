// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "SlateBrush.h"
#include "IC_3DTutorialWidget.generated.h"

USTRUCT()
struct FTutorialSlide {
  GENERATED_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UTexture2D *Texture;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FLinearColor Color;
};

UENUM(BlueprintType)
enum class ETutorialStatus : uint8 {
  TS_None             UMETA(DisplayName = "None"),
  TS_ShowTutorial     UMETA(DisplayName = "ShowTutorial"),
  TS_NextTutorial     UMETA(DisplayName = "NextTutorial"),
  TS_PreviousTutorial UMETA(DisplayName = "PreviousTutorial"),
  TS_TransitionBegin  UMETA(DisplayName = "TransitionBegin"),
  TS_TransitionEnd    UMETA(DisplayName = "TransitionEnd"),
  TS_HideTutorial     UMETA(DisplayName = "HideTutorial")
};


/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API UIC_3DTutorialWidget : public UUserWidget {
  GENERATED_BODY()

public:

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////
  
  UIC_3DTutorialWidget(const FObjectInitializer & ObjectInitializer);

  virtual void NativeConstruct() override;

  virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTime) override;

  ////////////////////////////////////////////////////////////////////////
  // Tutorial Properties
  ///////////////////////////////////////////////////////////////////////
  
  /* Time which will take Interpolations when fading-in-out */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    float TransitionSpeed;

  /* Background Widget Image */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    FTutorialSlide Background;

  /* RightArrow Widget Image */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    FTutorialSlide RightArrow;

  /* LeftArrow Widget Image */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    FTutorialSlide LeftArrow;

  /* Tutorial Widget Image */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    FTutorialSlide Tutorial;

  /* Array of Textures representing a complete Tutorial */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    TArray<UTexture2D*> TutorialSlides;

  /* Array of Textures of the Background */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | HUD")
    TArray<UTexture2D*> BackgroundTextures;

  // Current Tutorial Status (FSM)
  ETutorialStatus TutorialStatus;
  // Current Tutorial Slide
  int CurrentTutorialSlide;

  ////////////////////////////////////////////////////////////////////////
  // Tutorial Functions
  ///////////////////////////////////////////////////////////////////////
  
  /** Moves the tutorial slides to the next one. */
  UFUNCTION(BlueprintCallable, Category = "3DTutorialWidget")
    void MoveRight();

  /** Moves the tutorial slides to the previous one. */
  UFUNCTION(BlueprintCallable, Category = "3DTutorialWidget")
    void MoveLeft();

  /** Shows the tutorial menu. */
  UFUNCTION(BlueprintCallable, Category = "3DTutorialWidget")
    void Show();

  /** Hides the tutorial menu if the tutorial slide is the last one. */
  UFUNCTION(BlueprintCallable, Category = "3DTutorialWidget")
    void Hide();

  /** Hides the tutorial menu without checking if the tutorial slide is the last one. */
  UFUNCTION(BlueprintCallable, Category = "3DTutorialWidget")
    void ForceHide();
};
