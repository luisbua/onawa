// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IC_Interface.generated.h"

/**
 * 
 */
UINTERFACE(Blueprintable, meta = (CannotImplementInterfaceInBlueprint))
class THEINDIANCAVE_API UIC_Interface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class THEINDIANCAVE_API IIC_Interface
{
  GENERATED_IINTERFACE_BODY()

 public:
	
	 /** Return Character/Object Weight*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual float GetWeight();
	 /** Check bWeightLocked an return Character/Object Weight or 0*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual float GetWeightIn();
	 /** Check bWeightLocked an return Character/Object Weight or 0*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual float GetWeightOut();
	 
	 /** Return bWeightLocked value*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual bool GetLockWeight();
	 /** Set true bWeightLocked*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual void LockWeight();
	 /** Set false bWeightLocked*/
	 UFUNCTION(BlueprintCallable, Category = "Config | Interface")
		 virtual void UnlockWeight();
	
	
};
