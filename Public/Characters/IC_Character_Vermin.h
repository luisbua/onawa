// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "IC_Character_Vermin.generated.h"

UCLASS()
class THEINDIANCAVE_API AIC_Character_Vermin : public ACharacter
{
	GENERATED_BODY()

public:
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FVerminDelegate);

  //////////////////////////////////////////////////////////////////////////
  // HIERACHY COMPONENTS
  /////////////////////////////////////////////////////////////////////////

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    UStaticMeshComponent* StaticMesh;

  UPROPERTY(VisibleAnywhere)
    UBoxComponent* FearCollider;

  //////////////////////////////////////////////////////////////////////////
  // DEFAULT FUNCTIONS
  /////////////////////////////////////////////////////////////////////////

	// Sets default values for this character's properties
	AIC_Character_Vermin();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  ////////////////////////////////////////////////////////////////////////
  // Fear Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  void SetFear(bool state);

  /*********************************************************************/

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Fear")
    bool bFear;
	
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fear")
    bool bStayAwayAfterFirstGrowl;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fear")
    bool bBecomeInvisibleWhenScared;

  UPROPERTY(BlueprintAssignable, Category = "Event Delegate")
    FVerminDelegate OnStartFear;

  UPROPERTY(BlueprintAssignable, Category = "Event Delegate")
    FVerminDelegate OnEndFear;
};
