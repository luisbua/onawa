// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Characters/IC_Character.h"
#include "IC_Character_Wolf.generated.h"

class AIC_Object_Static_Invisible;
class AIC_Character_Vermin;

/**
 * 
 */
/** Param Growling true if the wolf is growling, false otherwise. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWolfGrowl, bool, Growling);

UCLASS()
class THEINDIANCAVE_API AIC_Character_Wolf : public AIC_Character {
	GENERATED_BODY()
	
public:
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWolfHowl);

  ////////////////////////////////////////////////////////////////////////
  // Wolf Character Components
  ///////////////////////////////////////////////////////////////////////
  UPROPERTY(VisibleAnywhere)
    USphereComponent* HighlightSphere;

  UPROPERTY(VisibleAnywhere)
    USphereComponent* RevealSphere;

  UPROPERTY(VisibleAnywhere)
    USphereComponent* ScareSphere;

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this character's properties
  AIC_Character_Wolf();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  virtual void MoveForward(float Val) override;
  virtual void MoveRight(float Val) override;
  virtual void TurnAtRate(float Rate) override;
  virtual void LookUpAtRate(float Rate) override;

  virtual void PossessedBy(AController* NewController) override;
  virtual void UnPossessed() override;

  ////////////////////////////////////////////////////////////////////////
  // Raycast Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Determines behaviour of the object hitted by the Raycast. */
  virtual void RaycastOnHoverHitObject() override;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Manages interaction between the Character and the Objects. */
  virtual void Interact() override;

  ////////////////////////////////////////////////////////////////////////
  // Highlight Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  UFUNCTION()
    void HighlightWhenPossessed();

  UFUNCTION()
    void HighlightWhenUnPossessed();

  UFUNCTION()
    void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndOverlap(class UPrimitiveComponent* HitComp,
      class AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex);

  /*********************************************************************/

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Highlight")
    float HighlightStrong;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Highlight")
    float HighlightSoft;

  ////////////////////////////////////////////////////////////////////////
  // Vision / Reveal Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  UFUNCTION(BlueprintCallable, Category = "Function | Update", meta=(DeprecatedFunction, DeprecationMessage="This function is deprecated and will be removed due to wolf's abilities changes, please delete it from your code."))
    void UpdateObjectReferences();

  void RevealWhenPossessed();

  void UnRevealWhenUnPossessed();

  UFUNCTION()
    void OnBeginReveal(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndReveal(class UPrimitiveComponent* HitComp,
      class AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex);
  
  /*********************************************************************/

  TArray<AIC_Object_Static_Invisible*> InvisibleObjects;

  bool bPossessed;

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FWolfHowl OnHowl;

  ////////////////////////////////////////////////////////////////////////
  // Scare Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  void ScareWhenPossessed();

  UFUNCTION()
    void OnBeginScare(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndScare(class UPrimitiveComponent* HitComp,
      class AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex);

  /*********************************************************************/

  //UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Config | Scare")
  TArray<AIC_Character_Vermin*> VerminReferences;

  UPROPERTY(BlueprintReadOnly, Category = "Config | Scare", meta = (DisplayName = "Is Growl Enabled"))
    bool bGrowl;

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FWolfGrowl OnGrowl;

  /******************************************/
  /* Wolf abilities activation/deactivation */
  /******************************************/

  void ActivateAbilitiesSpheres();

  void DeactivateAbilitiesSpheres();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Wolf Abilities")
    bool bCanScare;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Wolf Abilities")
    bool bCanView;
};
