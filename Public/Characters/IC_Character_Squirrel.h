// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Characters/IC_Character.h"
#include "IC_Character_Squirrel.generated.h"

/**
*
*/
UCLASS()
class THEINDIANCAVE_API AIC_Character_Squirrel : public AIC_Character
{
  GENERATED_BODY()

public:
  // Sets default values for this character's properties
  AIC_Character_Squirrel();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Raycast Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Determines behavior of the object hitted by the Raycast. */
  virtual void RaycastOnHoverHitObject() override;

  virtual void MoveForward(float Val) override;
  virtual void MoveRight(float Val) override;
  virtual void TurnAtRate(float Rate) override;
  virtual void LookUpAtRate(float Rate) override;

  void ControlGlide();
  void InitGlide();
  void StartGlide();
  void EndGlide();

  /** If the Squirrel is touching the Ground (Walkable surface) it
  will disable the physics simulation of the capsule component. */
  UFUNCTION(BlueprintCallable, Category = "Config | Squirrel")
    void CheckGrounded(FHitResult Hit);

  /** Maximum angle where the squirrel can walk for */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel")
    float WalkableFloorAngle;

  /** Default angle where the squirrel can walk for */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel")
    float DefaultWalkableFloorAngle;

  /* Whether the squirrel is gliding or not. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Squirrel")
    bool bGlide;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Squirrel")
    bool bClimbing;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Squirrel")
    bool bHanded;

  /* Whether the squirrel has glided or not. */
  UPROPERTY()
    bool bSquirrelGlidedWhenFlying;

  /** Whether the input glide has been pulsed or not. */
  UPROPERTY(BlueprintReadOnly, Category = "Config | Squirrel")
    bool bInputGlide;

  /** Whether the squirrel has been thrown or not. */
  UPROPERTY(BlueprintReadWrite)
    bool bThrowed;

  /** Force that squirrel applies to go up. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel")
    float ForceMovementWhenUp;

  /** Force that squirrel applies to go down. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel")
    float ForceMovementWhenDown;

  /** Speed that squirrel uses to move in the air. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel")
    float SpeedWhenGliding;

  /** Whether the squirrel can climb or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel Abilities")
    bool bCanClimb;

  /** Whether the squirrel can glide or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel Abilities")
    bool bCanGlide;

  UPROPERTY()
    float VelocityZ;

  UPROPERTY()
    float SaveZ;

  /** Value from 0.1f to 1.0f that adjust the speed when view up. Higher value = High head angle to start go up. Start with 0.4 or 0.5 to try. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Squirrel Glide")
    float GlideAngleRotationViewUp;
};
