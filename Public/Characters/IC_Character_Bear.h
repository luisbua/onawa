// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Characters/IC_Character.h"
#include "IC_Character_Bear.generated.h"

/**
 * 
 */
UCLASS()
class THEINDIANCAVE_API AIC_Character_Bear : public AIC_Character
{
	GENERATED_BODY()
	
public:
  // Sets default values for this character's properties
  AIC_Character_Bear();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  virtual void MoveForward(float Val) override;
  virtual void MoveRight(float Val) override;
  virtual void TurnAtRate(float Rate) override;
  virtual void LookUpAtRate(float Rate) override;

  ////////////////////////////////////////////////////////////////////////
  // Raycast Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Determines behaviour of the object hitted by the Raycast. */
  virtual void RaycastOnHoverHitObject() override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Bear Abilities")
    bool bCanBreak;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Bear Abilities")
    bool bCanPush;
};
