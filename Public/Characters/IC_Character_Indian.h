// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/Characters/IC_Character.h"
#include "IC_Character_Indian.generated.h"

/**
*
*/
UCLASS()
class THEINDIANCAVE_API AIC_Character_Indian : public AIC_Character
{
  GENERATED_BODY()

public:

  DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGrabObjectDelegate, AActor*, ObjectGrabbed);

  ////////////////////////////////////////////////////////////////////////
  // Indian Character Components
  ///////////////////////////////////////////////////////////////////////

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_Indian")
    UIC_CarryObjectComponent* CarriedObjectComp;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_Indian")
    USceneComponent* ObjectHolder;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_Indian")
    USceneComponent* ToolbeltHolder;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IC_Indian")
    USceneComponent* RightHandIK;

  UPROPERTY(VisibleAnywhere)
    USceneComponent* BigObjectHolder;

  UPROPERTY(VisibleAnywhere)
    UBoxComponent* FeetCollisionDetector;

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

  // Sets default values for this character's properties
  AIC_Character_Indian();

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  // Called every frame
  virtual void Tick(float DeltaSeconds) override;

  ////////////////////////////////////////////////////////////////////////
  // Movement Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Handles moving forward/backward. */
  virtual void MoveForward(float Val) override;

  /** Handles stafing movement, left and right. */
  virtual void MoveRight(float Val) override;

  /**
  * Called via input to turn at a given rate.
  * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate.
  */
  virtual void TurnAtRate(float Rate) override;

  /**
  * Called via input to turn look up/down at a given rate.
  * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate.
  */
  virtual void LookUpAtRate(float Rate) override;

  ////////////////////////////////////////////////////////////////////////
  // Raycast Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Determines behaviour of the object hitted by the Raycast. */
  virtual void RaycastOnHoverHitObject() override;

  ////////////////////////////////////////////////////////////////////////
  // Picking up / Throwing / Rotating Objects Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Charges the force which will be applied to the object when throwing. */
  void ChargeObjectThrowForce();

  /** Throws the picked up object using CarryObjectComponent */
  UFUNCTION(BlueprintCallable, Category = "Functions | Pickup and Throw")
  void ThrowObject();

  /** Sets bIsChargingForce to true. Used via PC */
  void EnableChargingForce();

  /** Locks the input and enables rotating the picked up object. */
  void InspectObject();

  /** 
  * Returns wether the Indian is holding an object or not.
  * @return true if holding, false otherwise.
  */
  UFUNCTION(BlueprintCallable, Category = "IC_Indian")
  bool IsHoldingObject();

  void CalcProjectilePath();

  /* ----------------------------------------------------------------------- */

  UPROPERTY()
    AActor* LastGrabbedObject;

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FGrabObjectDelegate OnGrabObject;

  ////////////////////////////////////////////////////////////////////////
  // Object Interaction Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Manages interaction between the Character and the Objects. */
  virtual void Interact() override;

  ////////////////////////////////////////////////////////////////////////
  // Collision detection Functions
  ///////////////////////////////////////////////////////////////////////

  /**
  * Delegated function to register the event OnComponentBeginOverlap
  * @param OverlappedComp Our component overlapped
  * @param OtherActor The other actor that is not this
  * @param OtherComp The component we overlap
  * @param OtherBodyIndex The index of the component in the other actor hierarchy
  * @param bFromSweep Is the event is called from a sweep movement
  * @param SweepResult Variable of the collision
  */
  UFUNCTION()
    void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  /** Detects overlapping with objects to prevent grabbing them 
      if they are under the indian's feet. Makes the objects to
      be non pickable. */
  UFUNCTION(BlueprintCallable, Category = "FeetCollision")
    void FeetCollisionBeginOverlap(AActor* OtherActor);

  /** Detects overlapping end with objects. Makes the objects to
      be again pickable. */
  UFUNCTION(BlueprintCallable, Category = "FeetCollision")
    void FeetCollisionEndOverlap(AActor* OtherActor);


  /************************************************************************/
  /* Indian Abilities activation/deactivation                             */
  /************************************************************************/
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Indian Abilities")
    bool bCanInteract;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Indian Abilities")
    bool bCanPickup;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Indian Abilities")
    bool bCanThrow;
};
