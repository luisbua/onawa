// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Public/Core/Characters/IC_Character.h"
#include "Public/Core/BlueprintLibrary/IC_BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "IC_LevelStreaming.generated.h"

UCLASS()
class THEINDIANCAVE_API AIC_LevelStreaming : public AActor
{
	GENERATED_BODY()
	
public:	
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStreamingStartDelegate);
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStreamingEndDelegate);

	//////////////////////////////////////////////////////////////////////////
	// HIERACHY COMPONENTS
	////////////////////////////////////////////////////////////////////////

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    UBoxComponent* CollisionDetector;

  //////////////////////////////////////////////////////////////////////////
  // DEFAULT FUNCTIONS
  ////////////////////////////////////////////////////////////////////////

	// Sets default values for this actor's properties
	AIC_LevelStreaming();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

  //////////////////////////////////////////////////////////////////////////
  // STREAMING FUNCTIONS AND VARIABLES
  ////////////////////////////////////////////////////////////////////////

  UFUNCTION(BlueprintCallable, Category = "Level Streaming")
    void StartLevelStreaming();

  UFUNCTION()
    void EndLevelStreaming();

  UFUNCTION()
    void LoadMap();


  UFUNCTION()
    void UnloadMap();

  /**
  * Delegated function to register the event OnComponentBeginOverlap
  * @param OverlappedComp Our component overlapped
  * @param OtherActor The other actor that is not this
  * @param OtherComp The component we overlap
  * @param OtherBodyIndex The index of the component in the other actor hierarchy
  * @param bFromSweep Is the event is called from a sweep movement
  * @param SweepResult Variable of the collision
  */
  UFUNCTION()
    void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
      AActor* OtherActor,
      class UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex,
      bool bFromSweep,
      const FHitResult& SweepResult);

  /************************************************************************/

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | General")
    bool bCanStream;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | General")
    int32 SectionID;

  /** Whether the camera will be faded or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fade")
    bool bDoCameraFade;

  /** Time that camera fade in will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fade", meta = (DisplayName = "Camera fade in time"))
    float FadeIn;

  /** Time that camera fade out will take. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fade", meta = (DisplayName = "Camera fade out time"))
    float FadeOut;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Fade")
    FLinearColor FadeColor;

  /** Time between fade in and streaming. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Delay")
    float DelayBegin;

  /** Time between streaming and fade out. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Delay")
    float DelayEnd;

  /** Array of maps that will be loaded when the box is overlapped */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Maps", meta=(DisplayName="Maps to load"))
    TArray<FName> LoadMaps;

  /** Array of maps that will be unloaded when the box is overlapped */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Maps", meta=(DisplayName="Maps to unload"))
    TArray<FName> UnloadMaps;

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FStreamingStartDelegate OnBeginStreaming;

  UPROPERTY(BlueprintAssignable, Category = "Events")
    FStreamingEndDelegate OnEndStreaming;

private:
  FLatentActionInfo StreamLatentInfo;
  int32 MapIndex;
  bool bBeginWorking;
  bool bUnloadFinished;
  bool bLoadFinished;
};
