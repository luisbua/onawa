// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "IC_Trigger.generated.h"

class AIC_GameMode;
class AIC_Character_Indian;



UCLASS()
class THEINDIANCAVE_API AIC_Trigger : public AActor
{
	GENERATED_BODY()
	
  // Don't move outside class scope or it will conflict with the
  // one declared in IC_Object_Static.
  DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTriggerInteractor);

public:	

  ////////////////////////////////////////////////////////////////////////
  // Default UE4 Functions / Constructor / BeginPlay / Tick
  ///////////////////////////////////////////////////////////////////////

	// Sets default values for this actor's properties
	AIC_Trigger();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

  ////////////////////////////////////////////////////////////////////////
  // Collision detection Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** 
  * Delegated function to register the event OnComponentBeginOverlap
  * @param OverlappedComp Our component overlapped
  * @param OtherActor The other actor that is not this
  * @param OtherComp The component we overlap
  * @param OtherBodyIndex The index of the component in the other actor hierarchy
  * @param bFromSweep Is the event is called from a sweep movement
  * @param SweepResult Variable of the collision
  */
  UFUNCTION()
  void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
                      AActor* OtherActor,
                      class UPrimitiveComponent* OtherComp,
                      int32 OtherBodyIndex,
                      bool bFromSweep,
                      const FHitResult& SweepResult);

  /* ----------------------------------------------------------------------- */

  /** This component will detect and generate the collisions */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
  UBoxComponent* TriggerBox;

  ////////////////////////////////////////////////////////////////////////
  // Trigger configuration Function and Variables
  ///////////////////////////////////////////////////////////////////////

  UFUNCTION()
  void TeleportWithoutFade(AIC_Character* character);

  /* ----------------------------------------------------------------------- */

  /** Damage that causes this Trigger. If bCausesDamage is enabled. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger")
    float Damage;
  /** Wether this Trigger causes damage or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger")
    bool bCausesDamage;
  /** Wether this Trigger instantly kills the character or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger")
    bool bDeadly;
  /** Wether this Trigger is a checkpoint or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger")
    bool bCheckpoint;
  /** Wether this Trigger teleports the Cahracter or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger")
    bool bTeleport;

  ////////////////////////////////////////////////////////////////////////
  // Trigger Vision Functions and Variables
  ///////////////////////////////////////////////////////////////////////

  /** Whether this Trigger is a Vision Trigger or not. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger| Vision")
    bool bVision;

  /** If true this Trigger will be usable after dispatching the Event when activated.
      Otherwise this Trigger could not be used again after activating it once. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger| Vision")
    bool bVisionReutilizable;

  /** If true this Trigger will autoreset without having to look away and then look again
      into it. Otherwise you have to look away and then look at the Trigger to reset. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger| Vision")
    bool bVisionAutoReset;

  /** Time needed to activate this trigger after staring at it. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Trigger| Vision")
    float TimeNeededToTrigger;

  /** If bVision is enabled this function will accumulate time till it reaches
      TimeNeedToTrigger parameter. */
  void AccumulateTime();
  /** Resets the TimeAccumulated to zero. */
  void ResetTime();
  
  UPROPERTY(BlueprintReadOnly)
  float RemainingTimeToActivate;

  /** Event dispatched when the Trigger reaches TimeNeededToTrigger if bVision is enabled. */
  UPROPERTY(BlueprintReadOnly, BlueprintAssignable, Category = "IC_Trigger")
    FTriggerInteractor VisionActive;
  /** Event dispatched when the Trigger resets its TimeAccumulated due to not being looked at anymore */
  UPROPERTY(BlueprintReadOnly, BlueprintAssignable, Category = "IC_Trigger")
    FTriggerInteractor VisionReset;


 private:
    AIC_GameMode* GameMode;

    ////////////////////////////////////////////////////////////////////////
    // Trigger Vision Variables
    ///////////////////////////////////////////////////////////////////////

    // Variable to store the initial time set in TimeNeededToTrigger.
    float InitialTimeSet;
    // Current time accumulated when bVision is enabled.
    float TimeAccumulated;
    // True if the VisionTrigger has been activated once.
    bool bVisionTriggerActivated;

};
