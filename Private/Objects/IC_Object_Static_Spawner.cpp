// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Object_Static_Spawner.h"


AIC_Object_Static_Spawner::AIC_Object_Static_Spawner()
{
	BoxSpawner = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Collision"));
	RootComponent = BoxSpawner;

	BoxSpawner->OnComponentEndOverlap.AddDynamic(this, &AIC_Object_Static_Spawner::Change);

	SpawnedObjectScale = FVector(1.0f, 1.0f, 1.0f);
	SpawnedObjectRotation = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
}



void AIC_Object_Static_Spawner::BeginPlay()
{
	Super::BeginPlay();
	
	FTransform trans;

	trans.SetLocation(BoxSpawner->GetComponentLocation());
	trans.SetRotation(SpawnedObjectRotation);
	trans.SetScale3D(SpawnedObjectScale);
	GetWorld()->SpawnActor<AIC_Object_Environment>(ObjectClass, trans);
}

void AIC_Object_Static_Spawner::Change(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	if (Cast<AIC_Object_Environment>(OtherActor))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandler,this, &AIC_Object_Static_Spawner::DelaySpawnObject,1.0f);
	
	}
}

void AIC_Object_Static_Spawner::DelaySpawnObject()
{
	FTransform trans;
	trans.SetLocation(BoxSpawner->GetComponentLocation());
	trans.SetRotation(SpawnedObjectRotation);
	trans.SetScale3D(SpawnedObjectScale);
	GetWorld()->SpawnActor<AIC_Object_Environment>(ObjectClass, trans);
}