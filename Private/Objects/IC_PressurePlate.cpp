// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_PressurePlate.h"

AIC_PressurePlate::AIC_PressurePlate() {
	AccumulatedWeight = 0.0;
	VelocityPlate = 1.0f;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	WeightDetector = CreateDefaultSubobject<UBoxComponent>(TEXT("Upper_Collision"));

	RootComponent = Root;
	StaticMesh->SetupAttachment(Root);
	WeightDetector->SetupAttachment(StaticMesh);

	WeightDetector->OnComponentBeginOverlap.AddDynamic(this, &AIC_PressurePlate::UpperCollisionBeginOverlap);
	WeightDetector->OnComponentEndOverlap.AddDynamic(this, &AIC_PressurePlate::UpperCollisionEndOverlap);

  StaticMesh->bGenerateOverlapEvents = false;
  StaticMesh->SetCollisionProfileName(TEXT("BlockAll"));
  PlateActivationDisplacement = FVector(0.0f, 0.0f, 0.0f);

  bPressed = false;
}

void AIC_PressurePlate::BeginPlay() {
	Super::BeginPlay();

  if (ActivationWeight == 0.0) {
    ActivationWeight = 10.0;
  }

  if (PlateActivationDisplacement == FVector(0.0f, 0.0f, 0.0f)) {
	  PlateActivationDisplacement = FVector(0.0, 0.0, 20.0);
  }

	PlateStartLocation = StaticMesh->GetComponentLocation();
	PlateEndLocation = StaticMesh->GetComponentLocation() - PlateActivationDisplacement;

	StaticMesh->bGenerateOverlapEvents = false;
  StaticMesh->SetCollisionProfileName(TEXT("BlockAll"));
}

void AIC_PressurePlate::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (AccumulatedWeight >= ActivationWeight) {
    StaticMesh->SetWorldLocation(FMath::VInterpTo(StaticMesh->GetComponentLocation(), PlateEndLocation, GetWorld()->GetDeltaSeconds(), VelocityPlate));
		int p = StaticMesh->GetComponentLocation().Z - PlateEndLocation.Z;
		
		if ((StaticMesh->GetComponentLocation().Z - PlateEndLocation.Z) < 2) {
      if (bPressed == false) {
				Pressed.Broadcast();
        bPressed = true;
      }
		}
	} else {
		StaticMesh->SetWorldLocation(FMath::VInterpTo(StaticMesh->GetComponentLocation(), PlateStartLocation, GetWorld()->GetDeltaSeconds(), VelocityPlate));
		if ((StaticMesh->GetComponentLocation().Z - PlateEndLocation.Z) > 1) {
      if (bPressed == true) {
				UnPressed.Broadcast();
        bPressed = false;
      }
		}
	}
}

void AIC_PressurePlate::UpperCollisionBeginOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {
  IIC_Interface *other_actor = Cast<IIC_Interface>(OtherActor);

	if (other_actor != nullptr) {
		AccumulatedWeight += other_actor->GetWeightIn();
		other_actor->LockWeight();
	}
}

void AIC_PressurePlate::UpperCollisionEndOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	IIC_Interface *other_actor = Cast<IIC_Interface>(OtherActor);

	if (other_actor != nullptr) {
		AccumulatedWeight -= other_actor->GetWeightOut();
		other_actor->UnlockWeight();
		
	}
}

