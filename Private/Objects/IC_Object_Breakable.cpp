// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Object_Breakable.h"

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Object_Breakable::AIC_Object_Breakable() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  bPickable = true;
  bDestroyed = false;
  bSpawnObject = false;
  Type = EObjectType::OT_Environment;
  DestructibleMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("DestructibleMesh"));


  RootComponent = DestructibleMesh;
  SpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnPoint"));
  SpawnPoint->SetupAttachment(RootComponent);
  SpawnPoint->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  SpawnedObjectScale = FVector(0.25f, 0.25f, 0.25f);
  SpawnedObjectRotation = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
}

void AIC_Object_Breakable::BeginPlay() {
  Super::BeginPlay();
  bValidMaterial = CheckIfMaterialIsValidForHover();
}

void AIC_Object_Breakable::Tick(float DeltaSeconds) {

}

////////////////////////////////////////////////////////////////////////
// Object Spawner Functions
///////////////////////////////////////////////////////////////////////

AIC_Object* AIC_Object_Breakable::GetSpawnedObject(bool &IsValid) {
  if (SpawnedObject == nullptr) {
    IsValid = false;
  }
  else {
    IsValid = true;
    return SpawnedObject;
  }
  return nullptr;
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions and Variables
///////////////////////////////////////////////////////////////////////

bool AIC_Object_Breakable::CanPickup() {
  if (bDestroyed) return false;
  return bPickable;
}

////////////////////////////////////////////////////////////////////////
// Destructible Behaviour
///////////////////////////////////////////////////////////////////////

void AIC_Object_Breakable::OnFracture() {
  if (bDestroyed == false) {
    OnBreak.Broadcast();

    if (bSpawnObject == true) {
      if (ObjectToSpawn != nullptr) {
        FTransform trans;
        FVector spawnLocation = SpawnPoint->GetComponentLocation();

        trans.SetLocation(spawnLocation);
        trans.SetRotation(SpawnedObjectRotation);
        trans.SetScale3D(SpawnedObjectScale);

        AIC_Object *object = GetWorld()->SpawnActor<AIC_Object>(ObjectToSpawn,
          trans);
      }
    }
    bDestroyed = true;
  }
}
