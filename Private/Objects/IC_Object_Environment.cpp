// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Characters/IC_Character.h"
#include "Objects/IC_Object_Environment.h"

// Sets default values
AIC_Object_Environment::AIC_Object_Environment() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  bPickable = true;
  Type = EObjectType::OT_Environment;
  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
  RootComponent = StaticMesh;
  SetupStaticMeshReference();
  StaticMesh->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
  StaticMesh->CanCharacterStepUpOn = ECB_No;
}

// Called when the game starts or when spawned
void AIC_Object_Environment::BeginPlay() {
  Super::BeginPlay();
  StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
  bValidMaterial = CheckIfMaterialIsValidForHover();
}

// Called every frame
void AIC_Object_Environment::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

}

bool AIC_Object_Environment::CanPickup() {
  // Comprobar si es humano/oso/ardilla/lobo y si el mismo
  // puede coger este objeto ...

  if (bPickable) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {

      return true;
    }
  }
  return false;
}

bool AIC_Object_Environment::CanInteract() {

  if (bInteractable) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian ||
        ControlledCharacter->CharacterType == ECharacterType::CT_Bear) {

      return true;
    }
  }
  return false;
}

void AIC_Object_Environment::Interact() {

}

void AIC_Object_Environment::BeginHover(float value, FLinearColor color) {
  Super::BeginHover(value, color);
}

void AIC_Object_Environment::EndHover() {
  Super::EndHover();
}




