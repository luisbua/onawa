// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Core/Characters/IC_Character.h"
#include "IC_Object_Static_Invisible.h"

AIC_Object_Static_Invisible::AIC_Object_Static_Invisible() {
  PrimaryActorTick.bCanEverTick = true;

  SetupStaticMeshReference();
  bPickable = false;
  bInteractable = true;
  Type = EObjectType::OT_Static;

  //MaxDistanceOfReveal = 1000.0f;
  bRevealOnReachScreenSpace = false;
  bKeepVisibleAfterReveal = true;
  bRevealed = false;
}

void AIC_Object_Static_Invisible::BeginPlay() {
  Super::BeginPlay();
  StaticMesh->SetCollisionProfileName("OverlapAllDynamic");
  StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
  StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
  StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Overlap);

  GameMode_ = Cast<AIC_GameMode>(GetWorld()->GetAuthGameMode());
  if (bSetupAsClue) {
    if (StaticMesh) {
      StaticMesh->SetSimulatePhysics(false);
    }
  }
  Hide(true);
}

void AIC_Object_Static_Invisible::Tick(float DeltaSeconds) {
  Super::Tick(DeltaSeconds);

  /*
  if (GameMode_->CharacterTypePossessed != ECharacterType::CT_Wolf) {
    Hide(false);
  }
  */

  if (bRevealOnReachScreenSpace) {
    if (!bRevealed) {
      Show();
    }
  }
}

void AIC_Object_Static_Invisible::Hide(bool force) {
  if (force) {
    StaticMesh->SetVisibility(false, true);
    bRevealed = false;
  }
  else {

    if (!GameMode_) {
      return;
    }

    if (GameMode_->CharacterTypePossessed == ECharacterType::CT_Wolf) {
      if (!bKeepVisibleAfterReveal) {
        StaticMesh->SetVisibility(false, true);
        StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Overlap);
        bRevealed = false;
      }
    }
  }
}

void AIC_Object_Static_Invisible::Show() {
  if (!GameMode_) {
    return;
  }

  if (GameMode_->CharacterTypePossessed == ECharacterType::CT_Wolf) {
    if (DetectOnScreen()) {
      StaticMesh->SetVisibility(true, true);
      StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Block);
      bRevealed = true;
      RevealEvent.Broadcast(this);
    }
  }
}

void AIC_Object_Static_Invisible::ForceShow() {
  StaticMesh->SetVisibility(true, true);
  StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Block);
  bRevealed = true;
  RevealEvent.Broadcast(this);
}

bool AIC_Object_Static_Invisible::DetectOnScreen() {
  FVector2D screen_location;
  FVector2D viewport_size = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());

  if (GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(
      GetActorLocation(), 
      screen_location))
  {
    if (screen_location.X >= 0.0f && screen_location.Y >= 0.0f &&
      screen_location.X <= viewport_size.X && screen_location.Y <= viewport_size.Y) {

      return true;
    }
  }  

  return false;
}
