// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Characters/IC_Character.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Objects/IC_Object_Static.h"

// Sets default values
AIC_Object_Static::AIC_Object_Static() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  bPickable = false;
  bInteractable = true;
  bColor = false;
  bMovementObject = false;
  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
  RootComponent = StaticMesh;
  SetupStaticMeshReference();
  StaticMesh->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
  StaticMesh->CanCharacterStepUpOn = ECB_No;
}

// Called when the game starts or when spawned
void AIC_Object_Static::BeginPlay() {
  Super::BeginPlay();

  NextPosition = GetActorLocation();
  if (StaticMesh != nullptr) {
    StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Block);
    StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
  }

  // Destructible default initialization
  if (StaticMesh != nullptr) {
    StaticMesh->SetSimulatePhysics(false);
  }

  if (bMovementObject) {
    StaticMesh->SetSimulatePhysics(true);
  }

  bValidMaterial = CheckIfMaterialIsValidForHover();

  /*
  if (AltarHolder == nullptr) {
    UActorComponent* tmp = GetComponentByClass(UIC_ObjectHolderComponent::StaticClass());
    if (!tmp)
      return;

    AltarHolder = Cast<UIC_ObjectHolderComponent>(tmp);
  }

  if (AltarHolder) {
    //AltarHolder->OnObjectReachHolder.AddDynamic(this, &AIC_Object_Static::ObjectReachHolder);
    //AltarHolder->OnObjectLeavesHolder.AddDynamic(this, &AIC_Object_Static::ObjectLeavesHolder);
  }
  */
}

// Called every frame
void AIC_Object_Static::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  /*AIC_GameMode* GameMode = Cast<AIC_GameMode>(GetWorld()->GetAuthGameMode());
  if (GameMode != nullptr) {
    if (GameMode->CharacterTypePossessed == ECharacterType::CT_Bear) {
      SetActorLocation(
        FMath::VInterpTo(
          GetActorLocation(),
          NextPosition,
          GetWorld()->GetDeltaSeconds(),
          1.0f), false);
    }
  }*/
}

void AIC_Object_Static::OnConstruction(const FTransform & Transform) {

}

bool AIC_Object_Static::CanPickup() {
  // Static objects can't be picked up.
  return false;
}

bool AIC_Object_Static::CanInteract() {

  if (bInteractable) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(
        GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian ||
        ControlledCharacter->CharacterType == ECharacterType::CT_Bear ||
        ControlledCharacter->CharacterType == ECharacterType::CT_Squirrel) {

      return true;
    }
  }
  return false;
}

void AIC_Object_Static::Interact() {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

  if (ControlledCharacter != nullptr) {
    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
      LOG_WARNING("Indian Triggered Event");
      ActivateEvent.Broadcast();
    }

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Bear) {
      LOG_WARNING("Bear Triggered Event");
      ActivateEventBear.Broadcast();
    }

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Squirrel) {
      LOG_WARNING("Squirrel Triggered Event");
      ActivateEventSquirrel.Broadcast();
    }
  }
}

void AIC_Object_Static::BeginHover(float value, FLinearColor color) {
  Super::BeginHover(value, color);
}

void AIC_Object_Static::EndHover() {
  Super::EndHover();
}

void AIC_Object_Static::MovementBear(float MovementDistance) {
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(GetWorld()->GetAuthGameMode());
  if (GameMode != nullptr) {
    AIC_Character* ControlledCharacter = GameMode->Bear;
    if (ControlledCharacter != nullptr) {
      StaticMesh->AddImpulse(MovementDistance * ControlledCharacter->FirstPersonCameraComponent->GetForwardVector().GetSafeNormal(), NAME_None, true);
    }
  }
	/*NextPosition.Z = GetActorLocation().Z;
  NextPosition = GetActorLocation()+ (ControlledCharacter->GetActorForwardVector() *distance);*/
}

// Cambia el estado del color del objeto.
void AIC_Object_Static::ChangeColorStatus(AIC_Object_Static *object, FLinearColor correctColor, FLinearColor incorrectColor) {
  object->bColor = !object->bColor;

  if (object->bColor == true) {
    object->DynamicMaterial->SetVectorParameterValue("Color", correctColor);
  }
  else {
    object->DynamicMaterial->SetVectorParameterValue("Color", incorrectColor);
  }
}

void AIC_Object_Static::ChangeColor(AIC_Object_Static *object, FLinearColor color, bool activate) {
  bColor = activate;
  object->DynamicMaterial->SetVectorParameterValue("Color", color);
}

// Devuelve si toda la array de objetos tiene el bColor positivo
bool AIC_Object_Static::CheckColorForPuzzle(TArray<AIC_Object_Static*> object) {
  bool checked = true;
  for (int32 i = 0; i < object.Num(); i++) {
    if (object[i]->bColor == false) {
      checked = false;
    }
  }
  return checked;
}

void AIC_Object_Static::ObjectReachHolder() {
  OnObjectReachHolder.Broadcast();
}

void AIC_Object_Static::ObjectLeavesHolder() {
  OnObjectLeavesHolder.Broadcast();
}


