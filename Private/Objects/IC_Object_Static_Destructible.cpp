// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "Characters/IC_Character_Bear.h"
#include "IC_Object_Static_Destructible.h"


////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Object_Static_Destructible::AIC_Object_Static_Destructible() {
  bPickable = false;
  bInteractable = true;
  bColor = false;
  DestructibleMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("Destructible Mesh"));
  DestructibleMesh->SetupAttachment(StaticMesh);
  SpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnPoint"));
  SpawnPoint->SetupAttachment(RootComponent);
  SpawnPoint->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  SpawnedObjectScale = FVector(0.25f, 0.25f, 0.25f);
  SpawnedObjectRotation = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
  bBreakOnInteract = true;
  bInteractableByBear = true;
  bInteractableByIndian = false;
  bFractured = false;
  bSpawnAtBeginPlay = false;
  SpawnedObject = nullptr;
}

void AIC_Object_Static_Destructible::BeginPlay() {
  Super::BeginPlay();
  if (BaseDamage == 0.00f) {
    BaseDamage = 2000.0f;
  }

  if (DamageRadius == 0.00f) {
    DamageRadius = 2000.0f;
  }

  if (ImpulseStrength == 0.00f) {
    ImpulseStrength = 2000.0f;
  }

  if (bSpawnAtBeginPlay == true) {
    SpawnObject();
  }
}

void AIC_Object_Static_Destructible::Tick(float DeltaSeconds) {

}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions and Variables
///////////////////////////////////////////////////////////////////////

bool AIC_Object_Static_Destructible::CanInteract() {
  if (bInteractable) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(
      GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Bear) {
      AIC_Character_Bear* bear = Cast<AIC_Character_Bear>(ControlledCharacter);
      if (bear != nullptr) {
        if (bear->bCanBreak) {
          return bInteractableByBear;
        }
      }
    }
    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
      return bInteractableByIndian;
    }
  }
  return false;
}

void AIC_Object_Static_Destructible::Interact() {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

  if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
    if (bBreakOnInteract == true) {
      if (DestructibleMesh != nullptr) {
        Break();
      }
    }
    else {
      ActivateEvent.Broadcast();
    }
  }
  if (ControlledCharacter->CharacterType == ECharacterType::CT_Bear) {
    if (bBreakOnInteract == true) {
      if (DestructibleMesh != nullptr) {
        Break();
      }
      else {
        LOG_ERROR("Destructible Mesh is NULLPTR");
      }
    }
    else {
      ActivateEventBear.Broadcast();
    }
  }
}

void AIC_Object_Static_Destructible::OnFracture() {
  if (DestructibleMesh != nullptr) {
    if (bFractured == false) {
      OnBreak.Broadcast();
      if (bSpawnAtBeginPlay == false) {
        SpawnObject();
      }
      else {
        // Apply physics to pre-spawned object
        UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(SpawnedObject->GetRootComponent());
        if (mesh != nullptr) {
          mesh->SetSimulatePhysics(true);
        }
      }
      bFractured = true;
    }
  }
}

void AIC_Object_Static_Destructible::Break() {
  if (DestructibleMesh != nullptr) {
    if (bFractured == false) {
      DestructibleMesh->ApplyRadiusDamage(BaseDamage,
        DestructibleMesh->Bounds.Origin, DamageRadius, ImpulseStrength, true);

      OnBreak.Broadcast();

      if (bSpawnAtBeginPlay == false) {
        SpawnObject();
      }
      else {
        // Apply physics to pre-spawned object
        UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(SpawnedObject->GetRootComponent());
        if (mesh != nullptr) {
          mesh->SetSimulatePhysics(true);
        }  
      }
    }
    bFractured = true;
  }
}

AIC_Object* AIC_Object_Static_Destructible::GetSpawnedObject(bool &IsValid) {
  if (SpawnedObject == nullptr) {
    IsValid = false;
  }
  else {
    IsValid = true;
    return SpawnedObject;
  }
  return nullptr;
}

void AIC_Object_Static_Destructible::SpawnObject() {
  if (bSpawnObject) {
    if (ObjectToSpawn != nullptr) {
      FTransform trans;
      FVector spawnLocation = SpawnPoint->GetComponentLocation();

      trans.SetLocation(spawnLocation);
      trans.SetRotation(SpawnedObjectRotation);
      trans.SetScale3D(SpawnedObjectScale);

      SpawnedObject = GetWorld()->SpawnActor<AIC_Object>(ObjectToSpawn,
        trans);

      if (bSpawnAtBeginPlay == true) {
        UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(SpawnedObject->GetRootComponent());
        if (mesh != nullptr) {
          mesh->SetSimulatePhysics(false);
        }
      }
    }
  }
}