// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Characters/IC_Character.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Objects/IC_Object_Mission.h"

////////////////////////////////////////////////////////////////////////
// Private Position Tracking Functions
///////////////////////////////////////////////////////////////////////

void AIC_Object_Mission::TrackPositions() {
  TArray<UStaticMeshComponent*> comps;
  this->GetComponents(comps);
  for (auto StaticMeshComponent : comps) {
    Velocity = StaticMeshComponent->GetComponentVelocity();
    FVector c(this->GetActorLocation().X,
      this->GetActorLocation().Y,
      this->GetActorLocation().Z + 250.0f);
    //positions.Add(this->GetActorLocation());
    TrackedPositions.Add(c);
  }
}

void AIC_Object_Mission::ObjectReachedZDistanceToReturnBack() {
  if (this->GetRootComponent()->GetComponentLocation().Z <= ZDistanceToReturnBack) {
    bStartInterpolatingObject = true;
    bThrowed = false;
    TArray<UStaticMeshComponent*> tmp_meshes;
    this->GetComponents<UStaticMeshComponent>(tmp_meshes);

    for (auto tmp_mesh : tmp_meshes) {
      if (tmp_mesh->IsSimulatingPhysics()) {
        tmp_mesh->SetSimulatePhysics(false);
        DisabledMeshes.Add(tmp_mesh);
      }
    }
  }
}

void AIC_Object_Mission::InterpolateToInitialPosition() {
  int i = TrackedPositions.Num() - 1 - PositionsPaddingIndex;
  SetActorLocation(FMath::VInterpTo(GetActorLocation(), TrackedPositions[i], GetWorld()->GetDeltaSeconds(), 1.0f), false);
  if (i > 1) {
    PositionsPaddingIndex += 1;
  }
  if (FVector::Dist(TrackedPositions[0], GetActorLocation()) < 80.0f) {
    for (i = 0; i < DisabledMeshes.Num(); i++) {
      DisabledMeshes[i]->SetSimulatePhysics(true);
    }
    DisabledMeshes.Empty();
    TrackedPositions.Empty();
    bStartInterpolatingObject = false;
    PositionsPaddingIndex = 0;
    bPlayerHeadHasBeenTracked = false;
  }
}

void AIC_Object_Mission::ReturnObjectAfterFalling() {
  if (bThrowed) {
    TrackPositions();
  }

  if (!bStartInterpolatingObject) {
    ObjectReachedZDistanceToReturnBack();
  }

  if (bStartInterpolatingObject) {
    InterpolateToInitialPosition();
  }
}

void AIC_Object_Mission::AutoTracking() {
  // If the object has any velocity...
  // ...means someone is moving it or falling
  if (!this->GetVelocity().IsNearlyZero()) {
    // Append Character 'head' position atleast 3-5 times
    // So it makes the Object return back above player.
    if (!bPlayerHeadHasBeenTracked) {
      bPlayerHeadHasBeenTracked = true;
      AIC_Character* ControlledCharacter = Cast<AIC_Character>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPawn());
      FVector p(ControlledCharacter->GetActorLocation().X,
        ControlledCharacter->GetActorLocation().Y,
        ControlledCharacter->GetActorLocation().Z + ZDistanceAbovePlayer);

      for (int i = 0; i < 8; i++) {
        TrackedPositions.Add(p);
      }
    }
    //ft.Append(this->GetVelocity().ToString());
    TrackPositions();
  }
  else {
    if (!bStartInterpolatingObject) {
      // If the object isn't moving
      bPlayerHeadHasBeenTracked = false;
      TrackedPositions.Empty();
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Object_Mission::AIC_Object_Mission() {
  PrimaryActorTick.bCanEverTick = true;
  bPickable = true;
  bThrowed = false;
  bPlayerHeadHasBeenTracked = false;
  bInteractable = true;
  bDetectionEnabled = false;
  bTargetDetected = false;
  bToolbeltItem = false;
  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
  RootComponent = StaticMesh;
  SetupStaticMeshReference();

  StaticMesh->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
  StaticMesh->CanCharacterStepUpOn = ECB_No;

  CollisionDetector = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Colision Detector"));
  CollisionDetector->SetupAttachment(StaticMesh);
  CollisionDetector->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
  CollisionDetector->CanCharacterStepUpOn = ECB_No;

  Type = EObjectType::OT_Mission;

  bLockedInLocation = false;
}

void AIC_Object_Mission::BeginPlay() {
  Super::BeginPlay();

  StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel3);
  bValidMaterial = CheckIfMaterialIsValidForHover();

  CollisionDetector->OnComponentBeginOverlap.AddDynamic(this, &AIC_Object_Mission::OnBeginOverlap);
  CollisionDetector->OnComponentEndOverlap.AddDynamic(this, &AIC_Object_Mission::OnEndOverlap);
  //CollisionDetector->SetRelativeLocation({ 0.0f, 0.0f, 0.0f });
  //CollisionDetector->IgnoreActorWhenMoving(GetParentActor(), true);
  
  if (bDetectionEnabled) {
    CollisionDetector->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
  }
  else {
    CollisionDetector->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  }

  MyPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent());
  SetupHolder();

  bStartInterpolatingObject = false;
  PositionsPaddingIndex = 0;

  if (ZDistanceToReturnBack == 0.00f) {
    ZDistanceToReturnBack = -1000.0f;
  }

  if (ZDistanceAbovePlayer == 0.0f) {
    ZDistanceAbovePlayer = 150.0f;
  }
}

void AIC_Object_Mission::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  // ReturnObjectAfterFalling();
  // AutoTracking();

  if (this->GetVelocity().IsNearlyZero() && bThrowed) {
    bThrowed = false;
  }

  if (bBroadcastEventOnce) {
    bBroadcastEventOnce = false;
    BroadcastTargetDetectionEvent();
    StartHolder();
  }
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions
///////////////////////////////////////////////////////////////////////

bool AIC_Object_Mission::CanPickup() {
  // Comprobar si es humano/oso/ardilla/lobo y si el mismo
  // puede coger este objeto ...


  if (bPickable && bLockInteractionAndPickup == false) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {

      // DEBUG
      return true;
    }
  }
  return false;
}

bool AIC_Object_Mission::CanInteract() {

  if (bInteractable && bLockInteractionAndPickup == false) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
      return true;
    }
  }
  return false;
}

void AIC_Object_Mission::Interact() {
  if (!bLockInteractionAndPickup) {
    if (bPickable) {
      UnlockInLocation();
    }

    if (bPickable && bInteractable) {
      CheckForOverlapedTargets();
    }

    if (!bPickable && bInteractable) {
      OnActivate.Broadcast();
    }
  }
}

void AIC_Object_Mission::LockInteractionAndPickup() {
  bLockInteractionAndPickup = true;
  bPickable = false;
  MyPrimitiveComponent->SetSimulatePhysics(false);
  MyPrimitiveComponent->SetCollisionProfileName("IC_Object_Mission");
}

void AIC_Object_Mission::UnlockInteractionAndPickup() {
  bLockInteractionAndPickup = false;
  bPickable = true;
}

////////////////////////////////////////////////////////////////////////
// Collision interaction Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Object_Mission::CheckForOverlapedTargets() {
  if (bTargetDetected == false) {
    CollisionDetector->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

    TArray<AActor*> overlapping_actors;
    CollisionDetector->GetOverlappingActors(overlapping_actors, AIC_Object_Static::StaticClass());

    for (int32 i = 0; i < InteractionTargets.Num() && bBroadcastEventOnce == false; ++i) {
      if (InteractionTargets[i]) {
        if (overlapping_actors.Contains(InteractionTargets[i])) {
          bTargetDetected = true;
          bBroadcastEventOnce = true;
          TargetIndex = i;
        }
      }
    }
  }
}

void AIC_Object_Mission::OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
  AActor* OtherActor,
  class UPrimitiveComponent* OtherComp,
  int32 OtherBodyIndex,
  bool bFromSweep,
  const FHitResult& SweepResult) {

  if (bTargetDetected == false) {
    AIC_Object_Static* tmp = Cast<AIC_Object_Static>(OtherActor);
    if (tmp) {
      for (int32 i = 0; i < InteractionTargets.Num() && bBroadcastEventOnce == false; ++i) {
        if (InteractionTargets[i] == tmp) {
          bTargetDetected = true;
          bBroadcastEventOnce = true;
          TargetIndex = i;
        }
      }
    }
  }
}

void AIC_Object_Mission::OnEndOverlap(
  UPrimitiveComponent * HitComp, 
  AActor * OtherActor, 
  UPrimitiveComponent * OtherComp, 
  int32 OtherBodyIndex)
{
  if (!bLockedInLocation) {
    AIC_Object_Static* tmp = Cast<AIC_Object_Static>(OtherActor);
    if (tmp && InteractionTargets.Num() > 0) {
      if (tmp && InteractionTargets[TargetIndex]) {
        if (tmp == InteractionTargets[TargetIndex]) {
          bTargetDetected = false;
          CollisionDetector->SetCollisionEnabled(ECollisionEnabled::NoCollision);
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////
// Object Highlighting Functions
///////////////////////////////////////////////////////////////////////

void AIC_Object_Mission::BeginHover(float value, FLinearColor color) {
  if (!bLockInteractionAndPickup) {
    Super::BeginHover(value, color);
  }
}

void AIC_Object_Mission::EndHover() {
  if (!bLockInteractionAndPickup) {
    Super::EndHover();
  }
}

////////////////////////////////////////////////////////////////////////
// Placing in holder Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Object_Mission::SetupHolder() {
  for (int32 i = 0; i < InteractionTargets.Num(); ++i) {
    if (InteractionTargets[i] != nullptr) {
      UIC_ObjectHolderComponent* holder = Cast<UIC_ObjectHolderComponent>(
        InteractionTargets[i]->GetComponentByClass(
          UIC_ObjectHolderComponent::StaticClass()));

      if (holder) {
        holder->SetObjectToPlace(this);
      }
    }
  }
}

void AIC_Object_Mission::StartHolder() {

  if (TargetIndex >= 0 && TargetIndex <= InteractionTargets.Num() - 1) {
    UIC_ObjectHolderComponent* holder = Cast<UIC_ObjectHolderComponent>(
      InteractionTargets[TargetIndex]->GetComponentByClass(
        UIC_ObjectHolderComponent::StaticClass()));

    if (holder) {
      holder->PlaceItem((int32)this->GetUniqueID());
    }    
  }
}

void AIC_Object_Mission::RemoveHolder(int32 holder_index) {
  if (holder_index >= 0 && holder_index <= InteractionTargets.Num() - 1) {
    UIC_ObjectHolderComponent* holder = Cast<UIC_ObjectHolderComponent>(
      InteractionTargets[holder_index]);
    if (holder) {
      holder->bPlacingItem = false;
      holder->ObjectsToPlace.Remove(this);
      InteractionTargets.RemoveAt(holder_index);
    }
  }
}

void AIC_Object_Mission::LockInLocation() {
  if (MyPrimitiveComponent) {
    MyPrimitiveComponent->GetBodyInstance()->bLockXRotation = true;
    MyPrimitiveComponent->GetBodyInstance()->bLockYRotation = true;
    MyPrimitiveComponent->GetBodyInstance()->bLockZRotation = true;
    MyPrimitiveComponent->GetBodyInstance()->bLockXTranslation = true;
    MyPrimitiveComponent->GetBodyInstance()->bLockYTranslation = true;
    MyPrimitiveComponent->GetBodyInstance()->bLockZTranslation = true;
    MyPrimitiveComponent->GetBodyInstance()->SetDOFLock(EDOFMode::SixDOF);

    bLockedInLocation = true;
  }
}

void AIC_Object_Mission::UnlockInLocation() {
  if (MyPrimitiveComponent->GetBodyInstance()) {
    MyPrimitiveComponent->SetSimulatePhysics(true);
    MyPrimitiveComponent->GetBodyInstance()->bLockXRotation = false;
    MyPrimitiveComponent->GetBodyInstance()->bLockYRotation = false;
    MyPrimitiveComponent->GetBodyInstance()->bLockZRotation = false;
    MyPrimitiveComponent->GetBodyInstance()->bLockXTranslation = false;
    MyPrimitiveComponent->GetBodyInstance()->bLockYTranslation = false;
    MyPrimitiveComponent->GetBodyInstance()->bLockZTranslation = false;
    MyPrimitiveComponent->GetBodyInstance()->SetDOFLock(EDOFMode::Default);

    bLockedInLocation = false;
  }
}

void AIC_Object_Mission::BroadcastTargetDetectionEvent() {
  if (TargetIndex >= 0 && TargetIndex <= InteractionTargets.Num() - 1) {
    if (InteractionTargets[TargetIndex] != nullptr) {
      TargetDetectionEvent.Broadcast(this, InteractionTargets[TargetIndex]);
    }
  }
}

void AIC_Object_Mission::BroadcastPickUpEvent() {
  PickUpEvent.Broadcast();
}