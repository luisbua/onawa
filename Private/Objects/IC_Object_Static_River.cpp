// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Object_Static_River.h"



AIC_Object_Static_River::AIC_Object_Static_River() {

	//River = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("River"));
	//River = CreateDefaultSubobject<USceneComponent>(TEXT("River"));
	DetectorBox = CreateDefaultSubobject<UBoxComponent>(TEXT("DetectorBox"));

	DetectorBox->SetupAttachment(StaticMesh);

	DetectorBox->OnComponentBeginOverlap.AddDynamic(this, &AIC_Object_Static_River::DetectorBoxBeginOverlap);
	DetectorBox->OnComponentEndOverlap.AddDynamic(this, &AIC_Object_Static_River::DetectorBoxEndOverlap);

	MaxDepht = 300.0f;
	DownVelocity = 1.0f;
}

void AIC_Object_Static_River::BeginPlay() {
	Super::BeginPlay();

	RiverOffset = 0.9;
	RiverHeigth = 0.0;
	RockIn = false;

	/** Add Material Instance to River->Material*/
	DynamicMaterialRiver = UMaterialInstanceDynamic::Create(StaticMesh->GetMaterial(0), this);
	StaticMesh->SetMaterial(0, DynamicMaterialRiver);

}

void AIC_Object_Static_River::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	/** Set in Material Instance RiverOffset*/
	DynamicMaterialRiver->SetScalarParameterValue("RiverOffset", RiverOffset);
	/** Set in Material Instance RiverHeigth*/
	DynamicMaterialRiver->SetScalarParameterValue("RiverHigth", RiverHeigth);

	if (RockIn) {

		//valor actual,valor objetivo, deltatime,velo interpolacion
		RiverHeigth = FMath::FInterpTo(RiverHeigth, MaxDepht, DeltaTime, DownVelocity/2);
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("rock IN"));
	}
	else {
		//valor actual,valor objetivo, deltatime,velo interpolacion
		RiverHeigth = FMath::FInterpTo(RiverHeigth, 0, DeltaTime, DownVelocity);
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("rock OUT"));
	}

}

void AIC_Object_Static_River::DetectorBoxBeginOverlap(
	class UPrimitiveComponent* HitComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult & SweepResult) {


	UClass* OC = OtherActor->GetClass();
	UClass* COC = BlockerObject;

	if (OC->IsChildOf(COC)) {

		//OtherActor->IsA(ObjectClass->GetClass())) {
		RockIn = true;

		FVector OtherActorLocation = OtherActor->GetActorLocation();
		//FVector InverseRiverLocation =  GetTransform().InverseTransformPosition(OtherActorLocation);


		FVector InverseRiverLocation = StaticMesh->GetRelativeTransform().InverseTransformPosition(OtherActorLocation);
		RiverOffset = (InverseRiverLocation.Y + (RiverLength / 2)) / RiverLength;// RiverLength;
		RockIn = true;
	}

}

void AIC_Object_Static_River::DetectorBoxEndOverlap(
	class UPrimitiveComponent* HitComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex) {

	UClass* OC = OtherActor->GetClass();
	UClass* COC = BlockerObject;

	if (OC->IsChildOf(COC)) {
		RockIn = false;
	}
}