// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Object_Static_Rocker.h"

AIC_Object_Static_Rocker::AIC_Object_Static_Rocker() {
	LeftPaddle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Left Paddle"));
	LeftPaddle->SetupAttachment(StaticMesh);

	RightPaddle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Right Paddle"));
	RightPaddle->SetupAttachment(StaticMesh);

	LeftCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Left Collider"));
	LeftCollider->SetupAttachment(LeftPaddle);

	RightCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Right Collider"));
	RightCollider->SetupAttachment(RightPaddle);

	fMaxHeight = 200.0f;
	fMinHeight = 0.0f;
	
	PrimaryActorTick.bCanEverTick = true;
}

void AIC_Object_Static_Rocker::BeginPlay() {
	Super::BeginPlay();
	LeftPaddleInitialLocation = FVector(LeftPaddle->GetComponentLocation().X, LeftPaddle->GetComponentLocation().Y, fMaxHeight);
	RightPaddleInitialLocation = FVector(RightPaddle->GetComponentLocation().X, RightPaddle->GetComponentLocation().Y, fMinHeight);

	LeftPaddle->SetWorldLocation(LeftPaddleInitialLocation);
	RightPaddle->SetWorldLocation(RightPaddleInitialLocation);
}

void AIC_Object_Static_Rocker::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
	fLeftWeight = CalculateWeightOverPaddle(LeftPaddle);
	fRightWeight = CalculateWeightOverPaddle(RightPaddle);
	DistributeHeightUsingWeight(DeltaSeconds);
}

float AIC_Object_Static_Rocker::CalculateWeightOverPaddle(UStaticMeshComponent* paddle) {
	TArray<AActor*> actors;
	paddle->GetOverlappingActors(actors);
	float fWeightTmp = 0.0f;
	for (auto actor : actors) {
		if (actor != this) {
			IIC_Interface *other_actor = Cast<IIC_Interface>(actor);
			fWeightTmp += other_actor->GetWeight();
		}
	}
	return fWeightTmp;
}

void AIC_Object_Static_Rocker::DistributeHeightUsingWeight(float DeltaSeconds) {
	if (fLeftWeight > fRightWeight) {
		if(RightPaddle->GetComponentLocation().Z != fMaxHeight)
			InterpAndMove(RightPaddle, LeftPaddle, DeltaSeconds, fMinHeight, fMaxHeight);
	}
	else if (fLeftWeight < fRightWeight) {
		if (LeftPaddle->GetComponentLocation().Z != fMaxHeight)
			InterpAndMove(LeftPaddle, RightPaddle, DeltaSeconds, fMinHeight, fMaxHeight);
	}
	else {
		if (LeftPaddle->GetComponentLocation().Z != fMidHeight)
			InterpAndMove(LeftPaddle, RightPaddle, DeltaSeconds, fMidHeight, fMidHeight);
	}
}

void AIC_Object_Static_Rocker::ReadjustInitialPosition() {
	LeftPaddle->SetWorldLocation(LeftPaddleInitialLocation);
	RightPaddle->SetWorldLocation(RightPaddleInitialLocation);
}

void AIC_Object_Static_Rocker::InterpAndMove(UStaticMeshComponent* PaddleUp, UStaticMeshComponent* PaddleDown, float DeltaSeconds, float MinHeight, float MaxHeight) {
	// Paddle up
	float fUpValue = FMath::FInterpTo(PaddleUp->RelativeLocation.Z, MaxHeight, DeltaSeconds, fInterpolationSpeed);
	PaddleUp->SetWorldLocation(FVector(PaddleUp->GetComponentLocation().X, PaddleUp->GetComponentLocation().Y, fUpValue));
	float fDownValue = FMath::FInterpTo(PaddleDown->RelativeLocation.Z, MinHeight, DeltaSeconds, fInterpolationSpeed);
	PaddleDown->SetWorldLocation(FVector(PaddleDown->GetComponentLocation().X, PaddleDown->GetComponentLocation().Y, -fDownValue));
}