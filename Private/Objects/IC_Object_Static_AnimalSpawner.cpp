// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Object_Static_AnimalSpawner.h"

AIC_Object_Static_AnimalSpawner::AIC_Object_Static_AnimalSpawner() {
	bPickable = false;
	bInteractable = false;
	bColor = false;
	BearSpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Bear Spawn Location"));
	BearSpawnLocation->SetupAttachment(StaticMesh);
	SquirrelSpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Squirrel Spawn Location"));
	SquirrelSpawnLocation->SetupAttachment(StaticMesh);
	WolfSpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Wolf Spawn Location"));
	WolfSpawnLocation->SetupAttachment(StaticMesh);

	BearSpawnLocation->SetRelativeLocation(FVector(400.0f, -30.0f, 140.0f));
	SquirrelSpawnLocation->SetRelativeLocation(FVector(-110.0f, -200.0f, 150.0f));
	WolfSpawnLocation->SetRelativeLocation(FVector(-110.0f, 200.0f, 150.0f));
}

void AIC_Object_Static_AnimalSpawner::BeginPlay() {
	Super::BeginPlay();
}

void AIC_Object_Static_AnimalSpawner::Tick(float DeltaSeconds) {

}

