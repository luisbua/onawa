// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Character.h"
#include "Components/BoxComponent.h"
#include "IC_Object_Menu.h"

AIC_Object_Menu::AIC_Object_Menu() {
  StaticMesh = nullptr;
  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh Component"));
  StaticMesh->SetupAttachment(RootComponent);

  SpotLight = nullptr;
  SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("SpotLight Component"));
  SpotLight->SetupAttachment(RootComponent);
  SpotLight->SetRelativeLocation(FVector(0.0f, 0.0f, 480.0f));
  SpotLight->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));

  BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
  BoxCollision->SetupAttachment(RootComponent);
  bInteractable = true;
  bHovered = false;
  bAfterBeginPlay = false;
}

void AIC_Object_Menu::Tick(float DeltaSeconds) {
  Super::Tick(DeltaSeconds);
  if (!bAfterBeginPlay) {
    AfterBeginPlay();
    bAfterBeginPlay = true;
  }
}

void AIC_Object_Menu::Execute_Implementation() {

}


bool AIC_Object_Menu::CanInteract() {
  if (bInteractable) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(
      GetWorld()->GetFirstPlayerController()->GetPawn());
    if (ControlledCharacter) {
      if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
        return true;
      }
    }
  }
  return false;
}

void AIC_Object_Menu::Interact() {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());

  if (ControlledCharacter) {
    if (ControlledCharacter->CharacterType == ECharacterType::CT_Indian) {
      LOG_WARNING("Indian Triggered Event");
      Execute();
    }
  }
}

void AIC_Object_Menu::AfterBeginPlay_Implementation() {

}
