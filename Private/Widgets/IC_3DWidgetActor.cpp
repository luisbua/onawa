// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_3DWidgetActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetStringLibrary.h"


// Sets default values
AIC_3DWidgetActor::AIC_3DWidgetActor() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("3DWidget"));
  WidgetComponent->SetupAttachment(GetRootComponent());
  PrimaryActorTick.bCanEverTick = true;
  WidgetDistance = 0.0f;
  bVisible = false;
}

// Called when the game starts or when spawned
void AIC_3DWidgetActor::BeginPlay() {
	Super::BeginPlay();
	
  if (WidgetDistance == 0) {
    WidgetDistance = 200.0f;
  }

}

// Called every frame
void AIC_3DWidgetActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

  if (bVisible) {
    APlayerCameraManager* cam = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
    if (cam != nullptr) {
      // Set Rotation of the Widget
      SetActorRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), cam->GetCameraLocation()));
      // Set Location of the Widget
      FVector widget_loc = cam->GetCameraLocation() + (cam->GetActorForwardVector() * WidgetDistance);
      SetActorLocation(widget_loc);
    }
  }

}

