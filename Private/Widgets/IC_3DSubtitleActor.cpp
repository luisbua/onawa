// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_3DSubtitleActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetStringLibrary.h"


// Sets default values
AIC_3DSubtitleActor::AIC_3DSubtitleActor() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
  WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("3DWidget"));
  WidgetComponent->SetupAttachment(GetRootComponent());
  bVisible = false;
  SubtitleDistance = 0.0f;
  CurrentSubtitleIndex = 0;
  CurrentSubtitleStackIndex = 0;
}

// Called when the game starts or when spawned
void AIC_3DSubtitleActor::BeginPlay() {
	Super::BeginPlay();
	
  if (SubtitleDistance == 0) {
    SubtitleDistance = 200.0f;
  }
}

// Called every frame
void AIC_3DSubtitleActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

  if (bVisible) {
    APlayerCameraManager* cam = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
    if (cam != nullptr) {
      // Set Rotation of the Widget
      SetActorRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), cam->GetCameraLocation()));
      // Set Location of the Widget
      FVector widget_loc = cam->GetCameraLocation() + (cam->GetActorForwardVector() * SubtitleDistance);
      SetActorLocation(widget_loc);
    }
  }

  if (WidgetComponent != nullptr) {
    UIC_3DSubtitleWidget* widget = Cast<UIC_3DSubtitleWidget>(WidgetComponent->GetUserWidgetObject());
    if (widget != nullptr) {

      TArray<FString> split_subtitle;
      split_subtitle = UKismetStringLibrary::ParseIntoArray(UKismetSystemLibrary::MakeLiteralString(SubtitleStack[CurrentSubtitleStackIndex].Subtitles[CurrentSubtitleIndex].ToString()),
                                                            FString("{br}"));

      // Build a string from the FString splitted text
      FString finaltext = split_subtitle[0];
      for (int i = 1; i < split_subtitle.Num(); i++) {
        finaltext.Append("\n");
        finaltext.Append(split_subtitle[i]);
      }
      widget->SetDisplayText(FText::FromString(finaltext));
    }
  }
}

void AIC_3DSubtitleActor::Continue(bool& all_subtitles_completed) {
  if (CurrentSubtitleIndex < SubtitleStack[CurrentSubtitleStackIndex].Subtitles.Num() - 1) {
    CurrentSubtitleIndex++;
    all_subtitles_completed = false;
  } else {
    all_subtitles_completed = true;
  }
}

void AIC_3DSubtitleActor::NextStack() {
  if (CurrentSubtitleStackIndex < SubtitleStack.Num() - 1) {
    CurrentSubtitleStackIndex++;
    CurrentSubtitleIndex = 0;
  }
}

void AIC_3DSubtitleActor::SetSubtitleIndex(int index) {
  CurrentSubtitleIndex = index;
}

void AIC_3DSubtitleActor::SetSubtitleStackIndex(int index) {
  CurrentSubtitleStackIndex = index;
}