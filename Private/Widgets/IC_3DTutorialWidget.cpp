// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_3DTutorialWidget.h"
#include "Core/GameModes/IC_GameMode.h"

UIC_3DTutorialWidget::UIC_3DTutorialWidget(const FObjectInitializer & ObjectInitializer) : UUserWidget(ObjectInitializer) {
  Background.Color = { 1.0f, 1.0f, 1.0f, 0.0f };
  RightArrow.Color = { 1.0f, 1.0f, 1.0f, 0.0f };
  LeftArrow.Color = { 1.0f, 1.0f, 1.0f, 0.0f };
  Tutorial.Color = { 1.0f, 1.0f, 1.0f, 0.0f };
  TutorialStatus = ETutorialStatus::TS_None;
  CurrentTutorialSlide = 0;
  TransitionSpeed = 0.0f;
}

void UIC_3DTutorialWidget::NativeConstruct() {
  Super::NativeConstruct();

  if (TransitionSpeed == 0.0f) {
    TransitionSpeed = 0.75f;
  }
}

void UIC_3DTutorialWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTime) {
  Super::NativeTick(MyGeometry, InDeltaTime);

  // GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("URoMWidget::NativeTick()"));

  switch (TutorialStatus) {
    case ETutorialStatus::TS_None: {

      break;
    }
    case ETutorialStatus::TS_ShowTutorial: {
      Background.Color.A = FMath::FInterpConstantTo(Background.Color.A, 1.0f, InDeltaTime, TransitionSpeed);
      RightArrow.Color.A = FMath::FInterpConstantTo(RightArrow.Color.A, 1.0f, InDeltaTime, TransitionSpeed);
      Tutorial.Color.A = FMath::FInterpConstantTo(Tutorial.Color.A, 1.0f, InDeltaTime, TransitionSpeed);
      if (Tutorial.Color.A > 0.98f) {
        TutorialStatus = ETutorialStatus::TS_None;
      }
      break;
    }
    case ETutorialStatus::TS_NextTutorial: {
      if (CurrentTutorialSlide > 0) {
        LeftArrow.Color.A = FMath::FInterpConstantTo(LeftArrow.Color.A, 1.0f, InDeltaTime, TransitionSpeed);
        Tutorial.Color.A = FMath::FInterpConstantTo(Tutorial.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
        if (Tutorial.Color.A < 0.02f) {
          TutorialStatus = ETutorialStatus::TS_TransitionBegin;
        }
      }
      break;
    }
    case ETutorialStatus::TS_PreviousTutorial: {
      if (CurrentTutorialSlide == 0) {
        LeftArrow.Color.A = FMath::FInterpConstantTo(LeftArrow.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      }
      Tutorial.Color.A = FMath::FInterpConstantTo(Tutorial.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      if (Tutorial.Color.A < 0.02f) {
        TutorialStatus = ETutorialStatus::TS_TransitionBegin;
      }
      break;
    }
    case ETutorialStatus::TS_TransitionBegin: {
      // Change texture now it is invisible
      Tutorial.Texture = TutorialSlides[CurrentTutorialSlide];
      TutorialStatus = ETutorialStatus::TS_TransitionEnd;
      break;
    }
    case ETutorialStatus::TS_TransitionEnd: {
      // Now unfade it again
      Tutorial.Color.A = FMath::FInterpConstantTo(Tutorial.Color.A, 1.0f, InDeltaTime, TransitionSpeed);
      if (Tutorial.Color.A > 0.98f) {
        TutorialStatus = ETutorialStatus::TS_None;
      }
      break;
    }
    case ETutorialStatus::TS_HideTutorial: {
      Background.Color.A = FMath::FInterpConstantTo(Background.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      RightArrow.Color.A = FMath::FInterpConstantTo(RightArrow.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      Tutorial.Color.A = FMath::FInterpConstantTo(Tutorial.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      LeftArrow.Color.A = FMath::FInterpConstantTo(LeftArrow.Color.A, 0.0f, InDeltaTime, TransitionSpeed);
      break;
    }
  }
}

void UIC_3DTutorialWidget::MoveRight() {
  if (TutorialStatus == ETutorialStatus::TS_None) { // Prevent moving too fast between slides
    if (CurrentTutorialSlide < TutorialSlides.Num() - 1) {
      CurrentTutorialSlide++;
      TutorialStatus = ETutorialStatus::TS_NextTutorial;
    } else {
      // Detect when is the last Slide and change the BackgroundTexture to end
      if (BackgroundTextures.Num() == 2) {
        Background.Texture = BackgroundTextures[1];
      }
    }
  }
}

void UIC_3DTutorialWidget::MoveLeft() {
  if (TutorialStatus == ETutorialStatus::TS_None) { // Prevent moving too fast between slides
    if (CurrentTutorialSlide > 0) {
      CurrentTutorialSlide--;
      // If we move one slide back we can safely return
      // the background texture to default.
      Background.Texture = BackgroundTextures[0];
      TutorialStatus = ETutorialStatus::TS_PreviousTutorial;
    }
  }
}

void UIC_3DTutorialWidget::Show() {
  // Make the Background, right arrow and first tutorial appear
  // by making a fade in.
  TutorialStatus = ETutorialStatus::TS_ShowTutorial;
  CurrentTutorialSlide = 0;

  // Initial Backgroudn Image
  Background.Texture = BackgroundTextures[0];
  Tutorial.Texture = TutorialSlides[0];

  // Reset the Alpha of all the showing elements
  Background.Color.A = 0.0f;
  RightArrow.Color.A = 0.0f;
  LeftArrow.Color.A = 0.0f;
  Tutorial.Color.A = 0.0f;

  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  if (GameMode != nullptr) {
    GameMode->bTutorialOpened = true;
  }
}

void UIC_3DTutorialWidget::Hide() {
  if (CurrentTutorialSlide == TutorialSlides.Num() - 1) {
    TutorialStatus = ETutorialStatus::TS_HideTutorial;
    AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (GameMode != nullptr) {
      GameMode->bTutorialOpened = false;
    }
  }
}

void UIC_3DTutorialWidget::ForceHide() {
  TutorialStatus = ETutorialStatus::TS_HideTutorial;
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  if (GameMode != nullptr) {
    GameMode->bTutorialOpened = false;
  }
}
