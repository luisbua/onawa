// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_3DSubtitleWidget.h"

FText UIC_3DSubtitleWidget::GetDisplayText() const {
  return DisplayText;
}

void UIC_3DSubtitleWidget::SetDisplayText(const FText NewDisplayText) {
  DisplayText = NewDisplayText;
}
