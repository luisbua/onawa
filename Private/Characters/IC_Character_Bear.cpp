// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Characters/IC_Character_Bear.h"

// Sets default values
AIC_Character_Bear::AIC_Character_Bear() {
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  CharacterType = ECharacterType::CT_Bear;
  
  Weight = 100.0f;

  // Activate/Deactivate abilities
  bCanBreak = true;
  bCanPush = true;
}

// Called when the game starts or when spawned
void AIC_Character_Bear::BeginPlay() {
  Super::BeginPlay();

}

// Called every frame
void AIC_Character_Bear::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  RaycastOnHoverHitObject();
}

////////////////////////////////////////////////////////////////////////
// Raycast Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character_Bear::RaycastOnHoverHitObject() {
  if (bRaycastHitObject) {
    if (HitData.GetActor() != nullptr) {
      if (ObjectHitByRaycast != nullptr) {
        ObjectHitByRaycast->EndHover();
        ObjectHitByRaycast = nullptr;
      }
      ObjectHitByRaycast = Cast<AIC_Object>(HitData.GetActor());
      if (ObjectHitByRaycast != nullptr) {
        if (ObjectHitByRaycast->CanInteract()) {
          ObjectHitByRaycast->BeginHover(HoverBaseBrightness, HoverColor);
        }
      }
    }
  }
}

void AIC_Character_Bear::MoveForward(float Val) {
  Super::MoveForward(Val);
}

void AIC_Character_Bear::MoveRight(float Val) {
  Super::MoveRight(Val);
}

void AIC_Character_Bear::TurnAtRate(float Rate) {
  Super::TurnAtRate(Rate);
}

void AIC_Character_Bear::LookUpAtRate(float Rate) {
  Super::LookUpAtRate(Rate);
}

