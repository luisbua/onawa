// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Characters/IC_Character_Indian.h"
#include "IC_Object_Menu.h"
#include "Objects/IC_Object_Mission.h"

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Character_Indian::AIC_Character_Indian() {
  PrimaryActorTick.bCanEverTick = true;
  CharacterType = ECharacterType::CT_Indian;
  CarriedObjectComp = CreateDefaultSubobject<UIC_CarryObjectComponent>(TEXT("CarriedObjectComp"));
  CarriedObjectComp->SetIndianReference(this);
  ObjectHolder = CreateDefaultSubobject<USceneComponent>(TEXT("ObjectHolder"));
  ObjectHolder->SetupAttachment(FirstPersonCameraComponent);
  ToolbeltHolder = CreateDefaultSubobject<USceneComponent>(TEXT("ToolbeltHolder"));
  ToolbeltHolder->SetupAttachment(GetCapsuleComponent());
  RightHandIK = CreateDefaultSubobject<USceneComponent>(TEXT("RightHandIK"));
  RightHandIK->SetupAttachment(FirstPersonCameraComponent);
  BigObjectHolder = CreateDefaultSubobject<USceneComponent>(TEXT("BigObjectHolder"));
  BigObjectHolder->SetupAttachment(FirstPersonCameraComponent);
  FeetCollisionDetector = CreateDefaultSubobject<UBoxComponent>(TEXT("FeetCollisionDetector"));
  FeetCollisionDetector->SetupAttachment(RootComponent);
  Weight = 50.0f;

  // Activate/deactivate indian abilities
  bCanInteract = true;
  bCanPickup = true;
  bCanThrow = true;

  LastGrabbedObject = nullptr;
}

void AIC_Character_Indian::BeginPlay() {
  Super::BeginPlay();

  Capsule->OnComponentBeginOverlap.AddDynamic(this, &AIC_Character_Indian::OnBeginOverlap);
  CarriedObjectComp->SetGrabPoint(ObjectHolder, BigObjectHolder);

  if (HoverBaseBrightness == 0.0f) {
    HoverBaseBrightness = 10.0f;
  }
}

void AIC_Character_Indian::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

  if (GameMode) {
    GameMode->CalculateAnimalsRecoverPosition(GetActorForwardVector(), GetActorLocation());
  }

  RaycastOnHoverHitObject();

  if (CarriedObjectComp->bChargeForce) {
    CalcProjectilePath();
  }
}

////////////////////////////////////////////////////////////////////////
// Movement Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character_Indian::MoveForward(float Val) {
  Super::MoveForward(Val);
}

void AIC_Character_Indian::MoveRight(float Val) {
  Super::MoveRight(Val);
}

void AIC_Character_Indian::TurnAtRate(float Rate) {
  Super::TurnAtRate(Rate);
}

void AIC_Character_Indian::LookUpAtRate(float Rate) {
  Super::LookUpAtRate(Rate);
}

////////////////////////////////////////////////////////////////////////
// Raycast Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character_Indian::RaycastOnHoverHitObject() {

  if (ObjectHitByRaycast != nullptr) {
    ObjectHitByRaycast->EndHover();
    AIC_Object_Menu* object_menu = Cast<AIC_Object_Menu>(ObjectHitByRaycast);
    if (object_menu) {
      object_menu->bHovered = true;
    }
  }

  if (bRaycastHitObject) {
    // Squirrel
    AIC_Character_Squirrel* ref = Cast<AIC_Character_Squirrel>(HitData.GetActor());
    if (ref != nullptr) {
      SquirrelHitByRaycast = ref;
    }
    else {
      if (HitData.GetActor() != nullptr && !CarriedObjectComp->bHoldObject) {
        ObjectHitByRaycast = Cast<AIC_Object>(HitData.GetActor());
        if (ObjectHitByRaycast != nullptr) {
          ObjectHitByRaycast->BeginHover(
            HoverBaseBrightness + CarriedObjectComp->ThrowForce, HoverColor);
        }
      }
    }
  }
  else {
    if (!IsHoldingObject()) {
      ObjectHitByRaycast = nullptr;
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Picking up / Throwing / Rotating Objects Functions and Variables
///////////////////////////////////////////////////////////////////////


void AIC_Character_Indian::ChargeObjectThrowForce() {
  if (bCanThrow) {
    CarriedObjectComp->ChargeObjectThrowForce();
  }
}

void AIC_Character_Indian::ThrowObject() {
  if (bCanThrow) {
    CarriedObjectComp->ThrowObject(FirstPersonCameraComponent->GetForwardVector());
    LastGrabbedObject = nullptr;
  }
}

void AIC_Character_Indian::EnableChargingForce() {
  // True if can throw, false if not.
  CarriedObjectComp->bChargeForce = bCanThrow;
}

void AIC_Character_Indian::InspectObject() {
  bDisableMovement = bDisableMouseRotation;
}

bool AIC_Character_Indian::IsHoldingObject() {
  return CarriedObjectComp->bHoldObject;
}

void AIC_Character_Indian::CalcProjectilePath() {
  FPredictProjectilePathParams PredictParams;
  FPredictProjectilePathResult PredictedResult;

  FVector lauch_velocity;
  float fraction = 1 / CarriedObjectComp->PhysicsObject->GetMass();

  if (CarriedObjectComp->bGrabBreakable) {
    lauch_velocity = FirstPersonCameraComponent->GetForwardVector() * CarriedObjectComp->ThrowForce * CarriedObjectComp->BreakableForceMultiplier * fraction;
  }
  else {
    lauch_velocity = FirstPersonCameraComponent->GetForwardVector() * CarriedObjectComp->ThrowForce * CarriedObjectComp->EnvironmentForceMultiplier * fraction;
  }

  PredictParams.LaunchVelocity = lauch_velocity;
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel1));
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel2));
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel3));
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel4));
  PredictParams.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel5));

  PredictParams.ActorsToIgnore.Add(CarriedObjectComp->PhysicsObject->GetAttachmentRootActor());
  PredictParams.ProjectileRadius = 2.0f;
  PredictParams.bTraceComplex = true;
  PredictParams.StartLocation = ObjectHolder->GetComponentLocation();
  PredictParams.DrawDebugType = EDrawDebugTrace::ForOneFrame;
  PredictParams.DrawDebugTime = 0.1f;

  UGameplayStatics::PredictProjectilePath(
    GetWorld(),
    PredictParams,
    PredictedResult);
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character_Indian::Interact() {
  if (ObjectHitByRaycast != nullptr) {
    // In case the object has to interact with other e.g. key
    if (ObjectHitByRaycast->bInteractable && ObjectHitByRaycast->bPickable && bCanPickup) {
      if (!IsHoldingObject()) {
        if (ObjectHitByRaycast->Type == EObjectType::OT_Mission) {
          AIC_Object_Mission* object_mission =
            Cast<AIC_Object_Mission>(ObjectHitByRaycast);

          if (object_mission->bLockedInLocation) {
            object_mission->UnlockInLocation();
          }
        }

        CarriedObjectComp->GrabObject(&HitData);
      }
      else {
        ObjectHitByRaycast->Interact();
        if (CarriedObjectComp->bHoldObject) {
          CarriedObjectComp->ThrowObject({ 0.0f, 0.0f, 0.0f });
        }
      }
    }

    if (ObjectHitByRaycast->CanInteract() == false && bCanPickup) {
      if (ObjectHitByRaycast->bPickable == true) {
        CarriedObjectComp->GrabObject(&HitData);
      }
    }
    else { // CanInteract() == true
      if (ObjectHitByRaycast->bPickable == false && bCanInteract) {
        ObjectHitByRaycast->Interact();
      }
    }
  }
  else {
    if (SquirrelHitByRaycast != nullptr && bCanPickup) {
      CarriedObjectComp->GrabObject(&HitData);
      SquirrelHitByRaycast->bHanded = true;
    }
  }

  if (CarriedObjectComp->bHoldObject &&
    LastGrabbedObject != CarriedObjectComp->PhysicsObject->GetAttachmentRootActor()) {

    OnGrabObject.Broadcast(CarriedObjectComp->PhysicsObject->GetAttachmentRootActor());
    LastGrabbedObject = CarriedObjectComp->PhysicsObject->GetAttachmentRootActor();
  }
}

////////////////////////////////////////////////////////////////////////
// Collision detection Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character_Indian::OnBeginOverlap(class UPrimitiveComponent * OverlappedComp,
  AActor * OtherActor,
  class UPrimitiveComponent * OtherComp,
  int32 OtherBodyIndex,
  bool bFromSweep,
  const FHitResult & SweepResult)
{
  
}

void AIC_Character_Indian::FeetCollisionBeginOverlap(AActor* OtherActor) {
  if (OtherActor != nullptr) {
    AIC_Object* obj = Cast<AIC_Object>(OtherActor);
    if (obj != nullptr) {
      obj->bPickable = false;
    }
  }
}

void AIC_Character_Indian::FeetCollisionEndOverlap(AActor* OtherActor) {
  if (OtherActor != nullptr) {
    AIC_Object* obj = Cast<AIC_Object>(OtherActor);
    if (obj != nullptr) {
      obj->bPickable = true;
    }
  }
}
