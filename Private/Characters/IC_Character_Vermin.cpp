// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_Character_Vermin.h"


// Sets default values
AIC_Character_Vermin::AIC_Character_Vermin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
  StaticMesh->SetupAttachment(GetCapsuleComponent());

  FearCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("FearCollison"));
  FearCollider->SetupAttachment(GetCapsuleComponent());
  FearCollider->SetCollisionProfileName("OverlapAll");
  FearCollider->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
  FearCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Overlap);

  bFear = false;
  bStayAwayAfterFirstGrowl = false;
  bBecomeInvisibleWhenScared = true;
}

// Called when the game starts or when spawned
void AIC_Character_Vermin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIC_Character_Vermin::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AIC_Character_Vermin::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AIC_Character_Vermin::SetFear(bool state) {
  bFear = state;

  if (bFear) {
    if (bBecomeInvisibleWhenScared) {
      GetCapsuleComponent()->SetVisibility(false, true);
      GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
      StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
      if (GetMesh()) {
        GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
      }
    }
    OnStartFear.Broadcast();
  }
  else {
    if (!bStayAwayAfterFirstGrowl) {
      if (bBecomeInvisibleWhenScared) {
        //CollisonComponent->SetCollisionProfileName(TEXT("NoCollision"));
        GetCapsuleComponent()->SetVisibility(true, true);
        GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        if (GetMesh()) {
          GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        }
      }
      OnEndFear.Broadcast();
    }
  }
}

