// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Objects/IC_Object.h"
#include "Objects/IC_Object_Static_Invisible.h"
#include "Characters/IC_Character_Vermin.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Characters/IC_Character_Wolf.h"

// Sets default values
AIC_Character_Wolf::AIC_Character_Wolf() {
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  CharacterType = ECharacterType::CT_Wolf;

  HighlightSphere = CreateDefaultSubobject<USphereComponent>(TEXT("HighlightSphereComponent"));
  HighlightSphere->SetupAttachment(GetRootComponent());
  HighlightSphere->SetSphereRadius(1000.0f);
  HighlightSphere->SetCollisionProfileName("WolfAbilities");

  RevealSphere = CreateDefaultSubobject<USphereComponent>(TEXT("RevealSphereComponent"));
  RevealSphere->SetupAttachment(GetRootComponent());
  RevealSphere->SetSphereRadius(1000.0f);
  RevealSphere->SetCollisionProfileName("WolfAbilities");

  ScareSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ScareSphereComponent"));
  ScareSphere->SetupAttachment(GetRootComponent());
  ScareSphere->SetSphereRadius(1000.0f);
  ScareSphere->SetCollisionProfileName("WolfAbilities");

  // Activate/deactivate wolf abilities
  bCanScare = true;
  bCanView = true;

  bPossessed = false;
  bGrowl = false;
  Weight = 20.0f;

  HighlightStrong = 10.0f;
  HighlightSoft = 2.0f;
}

// Called when the game starts or when spawned
void AIC_Character_Wolf::BeginPlay() {
  Super::BeginPlay();
  HighlightSphere->OnComponentBeginOverlap.AddDynamic(this, &AIC_Character_Wolf::OnBeginOverlap);
  HighlightSphere->OnComponentEndOverlap.AddDynamic(this, &AIC_Character_Wolf::OnEndOverlap);

  RevealSphere->OnComponentBeginOverlap.AddDynamic(this, &AIC_Character_Wolf::OnBeginReveal);
  RevealSphere->OnComponentEndOverlap.AddDynamic(this, &AIC_Character_Wolf::OnEndReveal);

  ScareSphere->OnComponentBeginOverlap.AddDynamic(this, &AIC_Character_Wolf::OnBeginScare);
  ScareSphere->OnComponentEndOverlap.AddDynamic(this, &AIC_Character_Wolf::OnEndScare);

  //DeactivateAbilitiesSpheres();
}

// Called every frame
void AIC_Character_Wolf::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  RaycastOnHoverHitObject();
}

void AIC_Character_Wolf::MoveForward(float Val) {
  Super::MoveForward(Val);
}

void AIC_Character_Wolf::MoveRight(float Val) {
  Super::MoveRight(Val);
}

void AIC_Character_Wolf::TurnAtRate(float Rate) {
  Super::TurnAtRate(Rate);
}

void AIC_Character_Wolf::LookUpAtRate(float Rate) {
  Super::LookUpAtRate(Rate);
}

void AIC_Character_Wolf::PossessedBy(AController * NewController) {
  Super::PossessedBy(NewController);
  ActivateAbilitiesSpheres();

  HighlightWhenPossessed();
  RevealWhenPossessed();
  ScareWhenPossessed();
}

void AIC_Character_Wolf::UnPossessed() {
  Super::UnPossessed();
  //DeactivateAbilitiesSpheres();


  HighlightWhenUnPossessed();
  UnRevealWhenUnPossessed();

  bPossessed = false;
}

void AIC_Character_Wolf::RaycastOnHoverHitObject() {

  if (bRaycastHitObject) {
    if (HitData.GetActor() != nullptr) {
      if (ObjectHitByRaycast != nullptr) {
        ObjectHitByRaycast->EndHover();
        ObjectHitByRaycast = nullptr;
      }
      ObjectHitByRaycast = Cast<AIC_Object>(HitData.GetActor());
      if (ObjectHitByRaycast != nullptr) {
        if (ObjectHitByRaycast->CanInteract()) {
          ObjectHitByRaycast->BeginHover(HoverBaseBrightness, HoverColor);
        }
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character_Wolf::Interact() {
  if (bPossessed && bCanView) {
    //OnHowl.Broadcast();
  }

  if (!bPossessed)
    bPossessed = true;
}

////////////////////////////////////////////////////////////////////////
// Vision / Reveal Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character_Wolf::UpdateObjectReferences() {

}

void AIC_Character_Wolf::RevealWhenPossessed() {

  TArray<AActor*> overlapping_actors;
  AIC_Object_Static_Invisible* invisible_object = nullptr;
  RevealSphere->GetOverlappingActors(overlapping_actors, AIC_Object_Static_Invisible::StaticClass());

  for (int32 i = 0; i < overlapping_actors.Num(); ++i)
  {
    if (overlapping_actors[i]) {
      invisible_object = Cast<AIC_Object_Static_Invisible>(overlapping_actors[i]);

      if (invisible_object) {
        invisible_object->Show();

        if (invisible_object->bSetupAsClue) {
          invisible_object->ForceShow();
        }

        if (!InvisibleObjects.Contains(invisible_object)) {
          InvisibleObjects.Add(invisible_object);
        }
      }
    }
  }
}

void AIC_Character_Wolf::UnRevealWhenUnPossessed() {
  for (int32 i = 0; i < InvisibleObjects.Num(); ++i) {
    if (InvisibleObjects[i]) {
      InvisibleObjects[i]->Hide(false);
    }
  }
  InvisibleObjects.Empty();
}

void AIC_Character_Wolf::OnBeginReveal(UPrimitiveComponent * OverlappedComp, 
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex, 
    bool bFromSweep, 
    const FHitResult & SweepResult) 
{
  if (bCanView) {
    //if (InvisibleObjects.Contains(Object))
    AIC_Object_Static_Invisible* invisible = Cast<AIC_Object_Static_Invisible>(OtherActor);
    if (invisible) {
      invisible->Show();
    }
  }
}

void AIC_Character_Wolf::OnEndReveal(UPrimitiveComponent * HitComp, 
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex) 
{
  if (bCanView) {
    AIC_Object_Static_Invisible* invisible = Cast<AIC_Object_Static_Invisible>(OtherActor);
    if (invisible) {
      invisible->Hide(false);
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Highlight Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character_Wolf::HighlightWhenPossessed() {
  TArray<AActor*> overlapping_actors;
  AIC_Object* highlight_object = nullptr;
  HighlightSphere->GetOverlappingActors(overlapping_actors, AIC_Object::StaticClass());

  for (int32 i = 0; i < overlapping_actors.Num(); ++i)
  {
    if (overlapping_actors[i]) {
      highlight_object = Cast<AIC_Object>(overlapping_actors[i]);

      if (highlight_object) {
        highlight_object->BeginWolfHighlight(HighlightStrong);
      }
    }
  }
}

void AIC_Character_Wolf::HighlightWhenUnPossessed() {
  TArray<AActor*> overlapping_actors;
  AIC_Object* highlight_object = nullptr;
  HighlightSphere->GetOverlappingActors(overlapping_actors, AIC_Object::StaticClass());

  for (int32 i = 0; i < overlapping_actors.Num(); ++i)
  {
    if (overlapping_actors[i]) {
      highlight_object = Cast<AIC_Object>(overlapping_actors[i]);

      if (highlight_object) {
        highlight_object->BeginWolfHighlight(HighlightSoft);
      }
    }
  }
}

void AIC_Character_Wolf::OnBeginOverlap(UPrimitiveComponent * OverlappedComp,
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex, 
    bool bFromSweep, 
    const FHitResult & SweepResult) 
{
  if (GameMode->CharacterTypePossessed == ECharacterType::CT_Wolf) {
    AIC_Object* highlight_object = Cast<AIC_Object>(OtherActor);
  
    if (highlight_object) {
      highlight_object->BeginWolfHighlight(HighlightStrong);
    }
  }
}

void AIC_Character_Wolf::OnEndOverlap(UPrimitiveComponent * HitComp, 
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex) 
{
  if (GameMode->CharacterTypePossessed == ECharacterType::CT_Wolf) {
    AIC_Object* highlight_object = Cast<AIC_Object>(OtherActor);

    if (highlight_object) {
      highlight_object->BeginWolfHighlight(HighlightSoft);
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Scare Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character_Wolf::ScareWhenPossessed() {
  TArray<AActor*> overlapping_actors;
  AIC_Character_Vermin* vermin = nullptr;
  ScareSphere->GetOverlappingActors(overlapping_actors, AIC_Character_Vermin::StaticClass());

  for (int32 i = 0; i < overlapping_actors.Num(); ++i)
  {
    if (overlapping_actors[i]) {
      vermin = Cast<AIC_Character_Vermin>(overlapping_actors[i]);

      if (vermin) {
        vermin->SetFear(true);
        if (!VerminReferences.Contains(vermin)) {
          VerminReferences.Add(vermin);
        }
      }
    }
  }

}

void AIC_Character_Wolf::OnBeginScare(UPrimitiveComponent * OverlappedComp,
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex, 
    bool bFromSweep, 
    const FHitResult & SweepResult)
{
  if (bCanScare) {
    if (!VerminReferences.Contains(OtherActor)) {
      AIC_Character_Vermin* tmp_vermin = Cast<AIC_Character_Vermin>(OtherActor);
      if (tmp_vermin) {
        tmp_vermin->SetFear(true);
        VerminReferences.Add(tmp_vermin);

        OnGrowl.Broadcast(true);
      }
    }
  }
}

void AIC_Character_Wolf::OnEndScare(UPrimitiveComponent * HitComp, 
    AActor * OtherActor, 
    UPrimitiveComponent * OtherComp, 
    int32 OtherBodyIndex)
{
  if (bCanScare) {
    if (VerminReferences.Contains(OtherActor)) {
      AIC_Character_Vermin* tmp_vermin = Cast<AIC_Character_Vermin>(OtherActor);
      if (tmp_vermin) {
        tmp_vermin->SetFear(false);
        VerminReferences.Remove(tmp_vermin);

        OnGrowl.Broadcast(false);
      }
    }
  }
}

void AIC_Character_Wolf::ActivateAbilitiesSpheres() {
  HighlightSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
  RevealSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
  ScareSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AIC_Character_Wolf::DeactivateAbilitiesSpheres() {
  HighlightSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  RevealSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  ScareSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
