// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Objects/IC_Object_Static.h"
#include "Characters/IC_Character_Squirrel.h"

// Sets default values
AIC_Character_Squirrel::AIC_Character_Squirrel() {
  PrimaryActorTick.bCanEverTick = true;
  CharacterType = ECharacterType::CT_Squirrel;
  DefaultWalkableFloorAngle = 0.0f;
  bThrowed = false;
  RaycastDistance = 500.0f;
  bInputGlide = false;
  Weight = 5.0f;
  VelocityZ = 0.0f;
  SpeedWhenGliding = 200.0f;
  ForceMovementWhenUp = 200.0f;
  ForceMovementWhenDown = 7.5f;
  GlideAngleRotationViewUp = 0.4f;

  bClimbing = false;
  bHanded = false;

  // Activate/Deactivate abilities
  bCanClimb = true;
  bCanGlide = true;
}

// Called when the game starts or when spawned
void AIC_Character_Squirrel::BeginPlay() {
  Super::BeginPlay();
  GetCapsuleComponent()->SetCollisionObjectType(ECollisionChannel::ECC_EngineTraceChannel4);
  GetCapsuleComponent()->SetCollisionProfileName(TEXT("IC_Character_Squirrel"));

  if (DefaultWalkableFloorAngle == 0.0f) {
    DefaultWalkableFloorAngle = 50.0f;
  }

  if (WalkableFloorAngle <= DefaultWalkableFloorAngle) {
    WalkableFloorAngle = 75.0f;
  }

  if (ForceMovementWhenUp == 0.0f) {
    ForceMovementWhenUp = 200.0f;
  }

  if (ForceMovementWhenDown == 0.0f) {
    ForceMovementWhenDown = 7.5f;
  }

  if (SpeedWhenGliding == 0.0f) {
    SpeedWhenGliding = 200.0f;
  }

  this->GetCharacterMovement()->bMaintainHorizontalGroundVelocity = false;
  this->GetCharacterMovement()->SetWalkableFloorAngle(DefaultWalkableFloorAngle);
}

// Called every frame
void AIC_Character_Squirrel::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

  if (bRaycastHitObject) {
    if (HitData.GetActor() != nullptr) {
      if (HitData.GetActor()->IsA(AIC_Object_Static::StaticClass())) {
        AIC_Object_Static* object_reference = Cast<AIC_Object_Static>(HitData.GetActor());
        if (object_reference != nullptr) {
          if (object_reference->bClimbable && bCanClimb) {
            this->GetCharacterMovement()->SetWalkableFloorAngle(WalkableFloorAngle);
            bClimbing = true;
          }
        }
      }
      else {
        if (this->GetCharacterMovement()->GetWalkableFloorAngle() > DefaultWalkableFloorAngle) {
          this->GetCharacterMovement()->SetWalkableFloorAngle(DefaultWalkableFloorAngle);
          bClimbing = false;
        }
      }
    }
  }
  RaycastOnHoverHitObject();

  if (bInputGlide) {
    InitGlide();
  }
  
  if (bGlide) {
    ControlGlide();
  }
}

void AIC_Character_Squirrel::ControlGlide() {
  float dot_product = FVector::DotProduct(FirstPersonCameraComponent->GetForwardVector().GetSafeNormal(), FVector(0.0f, 0.0f, 1.0f));
  // dot product < 0.0f => DOWN
  // dot product > 0.0f => UP
  // Calculate forward velocity firsts
  float LastZ = GetCharacterMovement()->Velocity.Z;
  GetCharacterMovement()->Velocity = FirstPersonCameraComponent->GetForwardVector() * SpeedWhenGliding;
  GetCharacterMovement()->Velocity.Z = LastZ;

  // Reduce Z to zero when view up
  if (dot_product > GlideAngleRotationViewUp && SaveZ > 0.0f) {
    SaveZ += dot_product * ForceMovementWhenUp;
    if (SaveZ >= -1.0f) {
      SaveZ = 0.0f;
    }
    GetCharacterMovement()->Velocity.Z = SaveZ;
  }

  // Up Z when view down
  if (dot_product < 0.0f) {
    GetCharacterMovement()->Velocity.Z += dot_product * ForceMovementWhenDown;
    SaveZ = GetCharacterMovement()->Velocity.Z;
  }

  if (dot_product > 0.0f && dot_product < 0.3f) {
    GetCharacterMovement()->Velocity.Z += (-1.0f * dot_product) * ForceMovementWhenDown;
    SaveZ = GetCharacterMovement()->Velocity.Z;
  }

  // Dirty fix
  if ((GetCharacterMovement()->Velocity.Z >= 0.0f && GetCharacterMovement()->Velocity.Z <= 1.1f)) {
    GetCharacterMovement()->Velocity.Z -= FMath::Abs(dot_product);
  }

  LOG_MESSAGE_PARAM("Velocity Z: %f", GetCharacterMovement()->Velocity.Z);
}

void AIC_Character_Squirrel::InitGlide() {
  if (((bInputGlide && GetCharacterMovement()->IsFalling()) || bThrowed) && bCanGlide) {
    bGlide = true;
    bSquirrelGlidedWhenFlying = true;
    bInputGlide = false;
    float LastZ = GetCharacterMovement()->Velocity.Z;
    GetCapsuleComponent()->SetSimulatePhysics(false);
    GetCharacterMovement()->MovementMode = MOVE_Flying;
    GetCharacterMovement()->Velocity.Z = LastZ;
  }
}

void AIC_Character_Squirrel::StartGlide() {
  bInputGlide = true;
}

void AIC_Character_Squirrel::EndGlide() {
  if (bGlide) {
    bGlide = false;
    bInputGlide = false;
    GetCharacterMovement()->MovementMode = MOVE_Walking;
  }
}

void AIC_Character_Squirrel::CheckGrounded(FHitResult Hit) {
  if (bThrowed || bSquirrelGlidedWhenFlying) {
    if (this->GetCharacterMovement()->IsWalkable(Hit)) {
			EndGlide();
      GetCapsuleComponent()->SetSimulatePhysics(false);
      SetActorRotation({ 0.0f,0.0f,0.0f });
      GetCharacterMovement()->Velocity.X = 0.0f;
      GetCharacterMovement()->Velocity.Y = 0.0f;
      GetCharacterMovement()->Velocity.Z = 0.0f;
      bThrowed = false;
      bSquirrelGlidedWhenFlying = false;
      bGlide = false;
      bInputGlide = false;
    }
  }
}

void AIC_Character_Squirrel::RaycastOnHoverHitObject() {

  if (bRaycastHitObject) {
    if (HitData.GetActor() != nullptr) {
      if (ObjectHitByRaycast != nullptr) {
        ObjectHitByRaycast->EndHover();
        ObjectHitByRaycast = nullptr;
      }
      ObjectHitByRaycast = Cast<AIC_Object>(HitData.GetActor());
      if (ObjectHitByRaycast != nullptr) {
        if (ObjectHitByRaycast->CanInteract()) {
          ObjectHitByRaycast->BeginHover(HoverBaseBrightness, HoverColor);
        }
      }
    }
  }
}

void AIC_Character_Squirrel::MoveForward(float Val) {
  if (!bGlide) {
    Super::MoveForward(Val);
  }
}

void AIC_Character_Squirrel::MoveRight(float Val) {
  if (!bGlide) {
    Super::MoveRight(Val);
  }
}

void AIC_Character_Squirrel::TurnAtRate(float Rate) {
  Super::TurnAtRate(Rate);
}

void AIC_Character_Squirrel::LookUpAtRate(float Rate) {
  Super::LookUpAtRate(Rate);
}