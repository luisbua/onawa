// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/GameModes/IC_GameMode.h"
#include "IC_BlueprintFunctionLibrary.h"
#include "Characters/IC_PlayerController.h"

AIC_GameMode::AIC_GameMode() {
  Bear = nullptr;
  Indian = nullptr;
  Squirrel = nullptr;
  Wolf = nullptr;
  PossessionMenu = nullptr;
	PlatformSpawner = nullptr;

  bBear = true;
  bSquirrel = true;
  bWolf = true;
  CharacterTypePossessed = ECharacterType::CT_Indian;

	BearRecoverLocation = FVector(0.0f);
	SquirrelRecoverLocation = FVector(0.0f);
	WolfRecoverLocation = FVector(0.0f);

  BearDistanceToIndian = { -200.0f, 200.0f, 0.0f };
  SquirrelDistanceToIndian = { 200.0f, 200.0f, 0.0f };
  WolfDistanceToIndian = { -200.0f, -200.0f, 0.0f };

  bCanPossess = true;
  bChangeMenu = false;
  bTutorialOpened = false;
}

void AIC_GameMode::BeginPlay() {
  Super::BeginPlay();
}

class AActor* AIC_GameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) {
	AActor* newActor = Super::FindPlayerStart_Implementation(Player, IncomingName);
	
	///////////////////////////
	TArray<AActor*> acts;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AIC_Object_Static::StaticClass(), acts);

	for (auto act : acts) {
		if (act->GetName().Contains("AnimalSpawner")) {
			PlatformSpawner = Cast<AIC_Object_Static_AnimalSpawner>(act);
		}
	}

	ObtainAnimalSpawnerReferences();
  if (Indian) {
    IndianLastCheckpoint = Indian->GetActorLocation();
  }
	///////////////////////////

	return newActor;
}

APawn * AIC_GameMode::SpawnDefaultPawnFor_Implementation(AController *NewPlayer, class AActor *StartSpot) {
  APawn *newPawn = Super::SpawnDefaultPawnFor_Implementation(NewPlayer, StartSpot);
  Indian = Cast<AIC_Character_Indian>(newPawn);
  AIC_PlayerController* myController = Cast<AIC_PlayerController>(NewPlayer);

  FRotator StartRotation(ForceInit);
  StartRotation.Yaw = StartSpot->GetActorRotation().Yaw;
  FVector StartLocation = StartSpot->GetActorLocation();

  FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;

	FVector newLocation;
  FRotator newRotation;

  //Bear
  if (bBear) {
		if (!PlatformSpawner) {
			newLocation.X = StartLocation.X + -100.0f;
			newLocation.Y = StartLocation.Y + 100.0f;
			newLocation.Z = StartLocation.Z + 0.0f;
		}
		else {
			newLocation = SpawnerRecoverBear;
		}
		
		newRotation = FRotator(-90.0f);

    Bear = GetWorld()->SpawnActor<AIC_Character_Bear>(ABearCharacterClass, newLocation, StartRotation, SpawnInfo);
  }

  //Squirrel
  if (bSquirrel) {
		if (!PlatformSpawner) {
			newLocation.X = StartLocation.X + +100.0f;
			newLocation.Y = StartLocation.Y + 100.0f;
			newLocation.Z = StartLocation.Z + 0.0f;
		}
		else {
			newLocation = SpawnerRecoverSquirrel;
		}
    Squirrel = GetWorld()->SpawnActor<AIC_Character_Squirrel>(ASquirrelCharacterClass, newLocation, StartRotation, SpawnInfo);
  }

  //Wolf
  if (bWolf) {
		if (!PlatformSpawner) {
			newLocation.X = StartLocation.X + -100.0f;
			newLocation.Y = StartLocation.Y + -100.0f;
			newLocation.Z = StartLocation.Z + 0.0f;
		}
		else {
			newLocation = SpawnerRecoverWolf;
		}
    Wolf = GetWorld()->SpawnActor<AIC_Character_Wolf>(AWolfCharacterClass, newLocation, StartRotation, SpawnInfo);
  }

  if (bChangeMenu) {
    //PossessionMenu
    newLocation.X = StartLocation.X + -400.0f;
    newLocation.Y = StartLocation.Y + 400.0f;
    newLocation.Z = StartLocation.Z + 150.0f;

    PossessionMenuNew = GetWorld()->SpawnActor<AIC_PossessionMenu>(APossessionMenu, newLocation, FRotator(0.0f, 0.0f, 0.0f), SpawnInfo);
    if (PossessionMenuNew != nullptr) {
      PossessionMenuNew->SetActorHiddenInGame(true);
      PossessionMenuNew->SetActorEnableCollision(false);
    }
  }
  else {
    //PossessionMenu
    newLocation.X = StartLocation.X + -400.0f;
    newLocation.Y = StartLocation.Y + 400.0f;
    newLocation.Z = StartLocation.Z + 150.0f;

    newRotation = StartRotation;
    newRotation += FRotator(0.0f, 90.0f, 90.0f);

    PossessionMenu = GetWorld()->SpawnActor<AActor>(APossessionMenu, newLocation, newRotation, SpawnInfo);
    if (PossessionMenu != nullptr) {
      PossessionMenu->SetActorHiddenInGame(true);
      PossessionMenu->SetActorEnableCollision(false);
    }
  }

  return newPawn;
}

void AIC_GameMode::ObtainAnimalSpawnerReferences() {
	if (PlatformSpawner) {
		SpawnerRecoverBear = PlatformSpawner->BearSpawnLocation->GetComponentLocation();
		SpawnerRecoverSquirrel = PlatformSpawner->SquirrelSpawnLocation->GetComponentLocation();
		SpawnerRecoverWolf = PlatformSpawner->WolfSpawnLocation->GetComponentLocation();
	}
}

void AIC_GameMode::TogglePossessionMenu(bool toggle, const FVector &newPosition, const FRotator &newRotation) {
  if (bChangeMenu) {
    if (PossessionMenuNew != nullptr && (bBear || bSquirrel || bWolf)) {
      PossessionMenuNew->SetActorHiddenInGame(!toggle);
      PossessionMenuNew->SetActorEnableCollision(toggle);
      PossessionMenuNew->SetActorLocation(newPosition);
      PossessionMenuNew->SetActorRotation(newRotation);
    }
  }
  else {
    if (PossessionMenu != nullptr && (bBear || bSquirrel || bWolf)) {
      PossessionMenu->SetActorHiddenInGame(!toggle);
      PossessionMenu->SetActorEnableCollision(toggle);
      PossessionMenu->SetActorLocation(newPosition);
      PossessionMenu->SetActorRotation(newRotation);
    }
  }
  OnPossessionMenu.Broadcast(toggle);
}

/*void ATheIndianCaveGameMode::RestartPlayer(AController* NewPlayer) {
Super::RestartPlayer(NewPlayer);
}*/

void AIC_GameMode::RespawnAnimalsIntoPlatform() {
	ObtainAnimalSpawnerReferences();
	if (bBear) {
		if (Bear) {
			Bear->SetActorLocation(SpawnerRecoverBear);
		}
	}

	if (bSquirrel) {
		if (Squirrel) {
			Squirrel->SetActorLocation(SpawnerRecoverSquirrel);
		}
	}

	if (bWolf) {
		if (Wolf) {
			Wolf->SetActorLocation(SpawnerRecoverWolf);
		}
	}
}

void AIC_GameMode::CalculateAnimalsRecoverPosition(const FVector& IndianForward, const FVector& IndianLocation) {
	BearRecoverLocation = IndianLocation + (IndianForward * BearDistanceToIndian);
	SquirrelRecoverLocation = IndianLocation + (IndianForward * SquirrelDistanceToIndian);
	WolfRecoverLocation = IndianLocation + (IndianForward * WolfDistanceToIndian);
}

void AIC_GameMode::SetIndianCheckpointLocation(FVector location) {
  IndianLastCheckpoint = location;
}

FVector AIC_GameMode::GetRespawnLocation(ECharacterType type) {
  FVector location(0.0f);

  switch (type) {
    case ECharacterType::CT_Undefined:
      break;

    case ECharacterType::CT_Indian:
      location = IndianLastCheckpoint;
      break;

    case ECharacterType::CT_Bear:
      location = BearRecoverLocation;
      break;

    case ECharacterType::CT_Squirrel:
      location = SquirrelRecoverLocation;
      break;

    case ECharacterType::CT_Wolf:
      location = SquirrelRecoverLocation;
      break;

    default:
      break;
  }

  return location;
}

void AIC_GameMode::TeleportAnimalToPosition(ECharacterType type, FVector TargetPosition) {
  UIC_BlueprintFunctionLibrary::TeleportAnimalToPosition(type, TargetPosition, GetWorld());
}

void AIC_GameMode::EnableAllCharactersMovement() {
  UIC_BlueprintFunctionLibrary::EnableAllCharactersMovement(GetWorld());
}

void AIC_GameMode::DisableAllCharactersMovement() {
  UIC_BlueprintFunctionLibrary::DisableAllCharactersMovement(GetWorld());
}
