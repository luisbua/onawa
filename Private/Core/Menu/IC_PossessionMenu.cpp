// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_GameMode.h"
#include "IC_PossessionMenu.h"


// Sets default values
AIC_PossessionMenu::AIC_PossessionMenu()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  idx_ = 0;
}

//   Called when the game starts or when sp awned
void AIC_PossessionMenu::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIC_PossessionMenu::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIC_PossessionMenu::MenuLeft() {
  idx_--;
  if (idx_ < 0) idx_ = 3;
  if (idx_ == possessed_idx_) idx_--;
  if (idx_ < 0) idx_ = 3;
  OnMenuMove.Broadcast(idx_);
}

void AIC_PossessionMenu::MenuRight() {
  idx_++;
  if (idx_ > 3) idx_ = 0;
  if (idx_ == possessed_idx_) idx_++;
  if (idx_ > 3) idx_ = 0;
  OnMenuMove.Broadcast(idx_);
}

void AIC_PossessionMenu::MenuPossess() {
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  if (GameMode) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (ControlledCharacter) {
      switch (idx_) {
      case 0:
        //Possess bear
        if (GameMode->Bear) {
          ControlledCharacter->PossessAnimal(ECharacterType::CT_Bear);
        }
        break;
      case 1:
        //Possess indian
        ControlledCharacter->PossessAnimal(ECharacterType::CT_Indian);
        break;
      case 2:
        //Possess wolf
        if (GameMode->Wolf) {
          ControlledCharacter->PossessAnimal(ECharacterType::CT_Wolf);
        }
        break;
      case 3:
        //Possess squirrel
        if (GameMode->Squirrel) {
          ControlledCharacter->PossessAnimal(ECharacterType::CT_Squirrel);
        }
        break;
      }
    }
  }
}

void AIC_PossessionMenu::MenuRecover() {
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  if (GameMode && idx_ != 1) {
    AIC_Character* ControlledCharacter = Cast<AIC_Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (ControlledCharacter) {
      switch (idx_) {
      case 0:
        //Recover bear
        if (GameMode->Bear) {
          ControlledCharacter->Recover(GameMode->Bear);
        }
        break;
      case 2:
        //Recover wolf
        if (GameMode->Wolf) {
          ControlledCharacter->Recover(GameMode->Wolf);
        }
        break;
      case 3:
        //Recover squirrel
        if (GameMode->Squirrel) {
          ControlledCharacter->Recover(GameMode->Squirrel);
        }
        break;
      }
    }
  }
}