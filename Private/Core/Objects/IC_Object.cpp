// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Objects/IC_Object.h"

////////////////////////////////////////////////////////////////////////
// Protected Functions
///////////////////////////////////////////////////////////////////////

void AIC_Object::SetupStaticMeshReference() {
  StaticMesh->SetSimulatePhysics(true);
  StaticMesh->WakeRigidBody();
  StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Block);
  StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
}

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Object::AIC_Object() {

  PrimaryActorTick.bCanEverTick = true;
  bInteractable = false;
  bPickable = false;
  bClimbable = false;
  bBig = false;
  bWasPickable = false;
  bCalculateScaleDifference = false;
  Weight = 0.0f;
	bWeightLocked = false;
	bInto = false;

  // Material basic settings
  DynamicMaterial = nullptr;
  bValidMaterial = false;
  bRestoreScale = false;
  bCalculateScaleDifference = false;
  Type = EObjectType::OT_Undefined;

  CurrentInterpolationValue = 0.0f;
  GoalInterpolationValue = 2000.0f;
  bHighlightInterpoleBegin = false;
  bHighlightDissolveFinished = false;
  InterpoleVelocity = 2.0f;
}

void AIC_Object::BeginPlay() {
  Super::BeginPlay();
  InitialScale = this->GetActorScale3D();
  bWasPickable = bPickable;

  ReadMeshesAndLoadMaterials();
  EndWolfHighlight();
  CurrentInterpolationValue = GoalInterpolationValue;
  if (GoalInterpolationValue != 0.0f) {
    SetWolfHighlight();
  }
}

void AIC_Object::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  if (bRestoreScale) {
    if (!bCalculateScaleDifference) {
      ScaleDifference = InitialScale - this->GetActorScale3D();
      bCalculateScaleDifference = true;
      if (bWasPickable == true) {
        bPickable = false;
      }
    }
    if (!ScaleDifference.IsNearlyZero(0.25f)) {
      FVector calc = this->GetActorScale3D() + (ScaleDifference / 40.0f);
      this->SetActorScale3D(calc);
      ScaleDifference -= (ScaleDifference / 40.0f);
    }
    else {
      this->SetActorScale3D(InitialScale);
      bPickable = bWasPickable;
      bCalculateScaleDifference = false;
      bRestoreScale = false;
    }
  }

  if (bHighlightInterpoleBegin && bHighlightDissolveFinished == false) {
    CurrentInterpolationValue = FMath::FInterpTo(
      CurrentInterpolationValue,
      0.0f,      
      DeltaTime,
      InterpoleVelocity);

    if (FMath::IsNearlyEqual(CurrentInterpolationValue, 10.0f, 5.0f)) {
      bHighlightInterpoleBegin = false;
      bHighlightDissolveFinished = true;
    }

    SetWolfHighlight();
  }
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions and Variables
///////////////////////////////////////////////////////////////////////

bool AIC_Object::CanPickup() {
  return false;
}

bool AIC_Object::CanInteract() {
  return false;
}

void AIC_Object::Interact() {

}

////////////////////////////////////////////////////////////////////////
// Object Highlighting Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Object::BeginHover(float value, FLinearColor color) {
  if (bValidMaterial) {
    DynamicMaterial->SetVectorParameterValue("HoverColor", color);
    DynamicMaterial->SetScalarParameterValue("EmissivePower", value);
  }
}

void AIC_Object::EndHover() {
  if (bValidMaterial) {
    DynamicMaterial->SetScalarParameterValue("EmissivePower", 0.0f);
  }
}

bool AIC_Object::CheckIfMaterialIsValidForHover() {
  bool result = false;

  if (StaticMesh != nullptr) {
    FLinearColor tmp_color;
    DynamicMaterial = UMaterialInstanceDynamic::Create(StaticMesh->GetMaterial(0), this);

    if (DynamicMaterial != nullptr) {

      if (!DynamicMaterial->GetVectorParameterValue("HoverColor", tmp_color) &&
          !DynamicMaterial->GetScalarParameterValue("EmissivePower", EmissiveValue)) {
        
        /** DEBUG Message disable on release*/
        //LOG_ERROR("WARNING doesn't support the highlight effect!");
      }
      else {
        StaticMesh->SetMaterial(0, DynamicMaterial);
        result = true;
      }
    }
  }

  return result;
}

void AIC_Object::ReadMeshesAndLoadMaterials() {
  TArray<UActorComponent*> components = this->GetComponents().Array(); 
  UMaterialInstanceDynamic* mid = nullptr;
  float value = 0.0f;

  for (int32 i = 0; i < components.Num(); ++i)
  {
    if (components[i] != nullptr) {
      if (components[i]->GetClass() == UStaticMeshComponent::StaticClass()) {
        UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(components[i]);

        if (mesh->GetMaterial(0) != nullptr) {
          mid = UMaterialInstanceDynamic::Create(mesh->GetMaterial(0), this);

          if (mid->GetScalarParameterValue("WolfObjects_EmissiveValue", value) ||
              mid->GetScalarParameterValue("DissolveMaskRadius", value)) {
            DynamicMaterials.Add(mid);
            mesh->SetMaterial(0, DynamicMaterials.Last());
          }
        }
      }

      if (components[i]->GetClass() == USkeletalMeshComponent::StaticClass()) {
        USkeletalMeshComponent* mesh = Cast<USkeletalMeshComponent>(components[i]);

        if (mesh->GetMaterial(0) != nullptr) {
          mid = UMaterialInstanceDynamic::Create(mesh->GetMaterial(0), this);

          if (mid->GetScalarParameterValue("WolfObjects_EmissiveValue", value) ||
              mid->GetScalarParameterValue("DissolveMaskRadius", value)) {
            DynamicMaterials.Add(mid);
            mesh->SetMaterial(0, DynamicMaterials.Last());
          }
        }
      }

      if (components[i]->GetClass() == UDestructibleComponent::StaticClass()) {
        UDestructibleComponent* mesh = Cast<UDestructibleComponent>(components[i]);

        for (int32 j = 0; j < mesh->GetNumMaterials(); ++j) {
          if (mesh->GetMaterial(j) != nullptr) {
            mid = UMaterialInstanceDynamic::Create(mesh->GetMaterial(j), this);

            if (mid->GetScalarParameterValue("WolfObjects_EmissiveValue", value) ||
                mid->GetScalarParameterValue("DissolveMaskRadius", value)) {
              DynamicMaterials.Add(mid);
              mesh->SetMaterial(j, DynamicMaterials.Last());
            }
          }
        }
      }
    }
  }
}

void AIC_Object::BeginWolfHighlight(float value)
{
  if (!bHighlightInterpoleBegin) {
    bHighlightInterpoleBegin = true;
    CurrentInterpolationValue = GoalInterpolationValue;
  }

  float scalar_parameter = 0.0f;
  for (int32 i = 0; i < DynamicMaterials.Num(); ++i)
  {
    if (DynamicMaterials[i]) {
      if (DynamicMaterials[i]->GetScalarParameterValue("WolfObjects_EmissiveValue", scalar_parameter)) {
        DynamicMaterials[i]->SetScalarParameterValue("WolfObjects_EmissiveValue", value);
      }
    }
  }
}

void AIC_Object::EndWolfHighlight()
{
  float scalar_parameter = 0.0f;
  for (int32 i = 0; i < DynamicMaterials.Num(); ++i)
  {
    if (DynamicMaterials[i]) {
      if (DynamicMaterials[i]->GetScalarParameterValue("WolfObjects_EmissiveValue", scalar_parameter)) {
        DynamicMaterials[i]->SetScalarParameterValue("WolfObjects_EmissiveValue", 0.0f);
      }
    }
  }
}

void AIC_Object::SetWolfHighlight()
{
  float scalar_parameter = 0.0f;
  for (int32 i = 0; i < DynamicMaterials.Num(); ++i)
  {
    if (DynamicMaterials[i]) {
      if (DynamicMaterials[i]->GetScalarParameterValue("DissolveMaskRadius", scalar_parameter)) {
        DynamicMaterials[i]->SetScalarParameterValue("DissolveMaskRadius", CurrentInterpolationValue);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Interface Functions and Variables
///////////////////////////////////////////////////////////////////////

float AIC_Object::GetWeight() {
 	return Weight;
}

float AIC_Object::GetWeightIn() {
	if (bWeightLocked) return 0.0f;
	else return Weight;
}

float AIC_Object::GetWeightOut() {
	if (!bWeightLocked) return 0.0f;
	else return Weight;
}

bool AIC_Object::GetLockWeight()
{
	return bWeightLocked;
}

void AIC_Object::LockWeight() {
	bWeightLocked = true;
}

void AIC_Object::UnlockWeight() {
	bWeightLocked = false;
}
