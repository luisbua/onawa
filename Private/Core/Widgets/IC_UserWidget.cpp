// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_PlayerController.h"
#include "IC_UserWidget_Button.h"
#include "IC_UserWidget.h"

void UIC_UserWidget::Tick(float DeltaSeconds) {

}

void UIC_UserWidget::SetFocusHere() {
  AIC_PlayerController* PC = Cast <AIC_PlayerController>(GetWorld()->GetFirstPlayerController());
  if (PC) {
    FocusRedirect->SetUserFocus(PC);
  }
}

FReply UIC_UserWidget::NativeOnFocusReceived(const FGeometry & InGeometry, const FFocusEvent & InFocusEvent) {
  Super::NativeOnFocusReceived(InGeometry, InFocusEvent);
  SetFocusHere();
  return FReply::Handled();
}

#include "AllowWindowsPlatformTypes.h"

void UIC_UserWidget::SendKeyInputFromChar(uint8 inChar) {
  INPUT i;
  i.type = INPUT_KEYBOARD;
  
  i.ki.wScan = inChar;
  i.ki.time = 0;
  i.ki.dwExtraInfo = 0;

  i.ki.wVk = 0;
  i.ki.dwFlags = KEYEVENTF_UNICODE;
  SendInput(1, &i, sizeof(INPUT));
}
#include "HideWindowsPlatformTypes.h"