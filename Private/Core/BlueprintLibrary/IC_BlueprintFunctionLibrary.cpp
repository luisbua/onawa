// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Core/Characters/IC_PlayerController.h"
#include "Core/GameInstance/IC_GameInstance.h"
#include "IC_SaveGame.h"
#include "IC_BlueprintFunctionLibrary.h"
#include "Widgets/IC_HUD.h"

bool UIC_BlueprintFunctionLibrary::CompareIndianHoldingObject(FString name, UObject* WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* gamemode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (gamemode != nullptr) {
    if (gamemode->Indian != nullptr) {
      AIC_Character_Indian* ind = gamemode->Indian;
      if (ind->IsHoldingObject()) {
        if (ind->CarriedObjectComp->GrabbedObject != nullptr) {
          if (ind->CarriedObjectComp->GrabbedObject->Name.Compare(name) == 0) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

void UIC_BlueprintFunctionLibrary::TeleportAnimalToPosition(ECharacterType type, FVector TargetPosition, UObject* WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (GameMode != nullptr) {
    if (type == ECharacterType::CT_Indian) {
      GameMode->Indian->SetActorLocation(TargetPosition);
    }
    else if (type == ECharacterType::CT_Squirrel) {
      GameMode->Squirrel->SetActorLocation(TargetPosition);
    }
    else if (type == ECharacterType::CT_Wolf) {
      GameMode->Wolf->SetActorLocation(TargetPosition);
    }
    else if (type == ECharacterType::CT_Bear) {
      GameMode->Bear->SetActorLocation(TargetPosition);
    }
  }
}

AIC_Character_Indian* UIC_BlueprintFunctionLibrary::GetIndianReference(UObject* WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (GameMode != nullptr) {
    return GameMode->Indian;
  }
  return nullptr;
}

AIC_Character* UIC_BlueprintFunctionLibrary::GetCurrentCharacterReference(UObject* WorldContextObject) {
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
	if (GameMode != nullptr) {
		switch (GameMode->CharacterTypePossessed) {
		case ECharacterType::CT_Bear:
			return Cast<AIC_Character>(GameMode->Bear);
			break;
		case ECharacterType::CT_Indian:
			return Cast<AIC_Character>(GameMode->Indian);
			break;
		case ECharacterType::CT_Squirrel:
			return Cast<AIC_Character>(GameMode->Squirrel);
			break;
		case ECharacterType::CT_Wolf:
			return Cast<AIC_Character>(GameMode->Wolf);
			break;
		}
	}
	return nullptr;
}

void UIC_BlueprintFunctionLibrary::ControllerShake(float intensity, float duration, bool affectLeftLarge, bool affectLeftSmall, bool affectRightLarge, bool affectRightSmall, UObject* WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_PlayerController* PC = Cast<AIC_PlayerController>(World->GetFirstPlayerController());
  
  FLatentActionInfo actioninfo; // JA es un fix temporal para que trabajen los artistas, ajustalo como debas
  if (PC) {
    PC->PlayDynamicForceFeedback(intensity, duration, affectLeftLarge, affectLeftSmall, affectRightLarge, affectRightSmall, EDynamicForceFeedbackAction::Start, actioninfo);
  }
  else {
    LOG_MESSAGE("Shake doesn't work well!");
  }
}

void UIC_BlueprintFunctionLibrary::ShowSubtitle(UObject* WorldContextObject, FString Cadena, FColor color, float TimeToShowSubtitle_, float Height) {

	//UIC_BlueprintFunctionLibrary::GetCurrentCharacterReference(GetWorld());
	AIC_Character *Character = GetCurrentCharacterReference(WorldContextObject);
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UIC_HudWidget* widgethud = nullptr;

	if (Character) {
		widgethud = Cast<UIC_HudWidget>(Character->HudWidget->GetUserWidgetObject());
		if (widgethud) {
			widgethud->SetSubtitleText(Cadena);
			Character->HudWidget->RequestRedraw();
		}
		
		Character->TimeToShowSubtitle = TimeToShowSubtitle_;
		Character->HudWidget->SetVisibility(true, true);
		Character->StartTime = World->GetTimeSeconds();
		Character->ActiveSubtitle = true;
		
		//Character->Subtitle->SetVisibility(true,true);
		//Character->Subtitle->SetText(Cadena);
		//Character->Subtitle->SetRelativeLocation(FVector(300.0f,0.0f, Height));
		//Character->Subtitle->SetTextRenderColor(color);
		//Character->SubtitleBack->SetVisibility(true);
	}
}

void UIC_BlueprintFunctionLibrary::EnableAllCharactersMovement(UObject * WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  GameMode->Indian->EnableMovement(true);
  GameMode->Bear->EnableMovement(true);
  GameMode->Squirrel->EnableMovement(true);
  GameMode->Wolf->EnableMovement(true);
}

void UIC_BlueprintFunctionLibrary::DisableAllCharactersMovement(UObject * WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  GameMode->Indian->DisableMovement(false);
  GameMode->Bear->DisableMovement(false);
  GameMode->Squirrel->DisableMovement(false);
  GameMode->Wolf->DisableMovement(false);
}

void UIC_BlueprintFunctionLibrary::PutObjectInToolbelt(AIC_Object* object) {
  UWorld * World = GEngine->GetWorldFromContextObject(object);
  AIC_GameMode* gamemode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (gamemode != nullptr) {
    if (gamemode->Indian != nullptr) {
      AIC_Character_Indian* ind = gamemode->Indian;
      ind->CarriedObjectComp->PutObjectInToolbelt(object);
    }
  }
}

void UIC_BlueprintFunctionLibrary::PickObjectOfToolbelt(UObject * WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  AIC_GameMode* gamemode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (gamemode != nullptr) {
    if (gamemode->Indian != nullptr) {
      AIC_Character_Indian* ind = gamemode->Indian;
      ind->CarriedObjectComp->PickObjectOfToolbelt();
    }
  }
}

void UIC_BlueprintFunctionLibrary::PickAnyObject(AIC_Object* object) {
  UWorld * World = GEngine->GetWorldFromContextObject(object);
  AIC_GameMode* gamemode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(World));
  if (gamemode != nullptr) {
    if (gamemode->Indian != nullptr) {
      AIC_Character_Indian* ind = gamemode->Indian;
      ind->CarriedObjectComp->PickAnyObject(object);
    }
  }
}

bool UIC_BlueprintFunctionLibrary::GameInstanceDevelopment(UObject * WorldContextObject) {
  UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
  UIC_GameInstance* game_instance = Cast<UIC_GameInstance>(UGameplayStatics::GetGameInstance(World));
  if (game_instance != nullptr) {
    return game_instance->bDevelopment;
  }
  return false;
}

void UIC_BlueprintFunctionLibrary::SaveGame(UObject * WorldContextObject)
{
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	AIC_Character *Character = GetCurrentCharacterReference(WorldContextObject);
	UIC_GameInstance* game_instance = Cast<UIC_GameInstance>(UGameplayStatics::GetGameInstance(World));
	UIC_SaveGame* SaveGameInstance = Cast<UIC_SaveGame>(UGameplayStatics::CreateSaveGameObject(UIC_SaveGame::StaticClass()));
	game_instance->MapIndex++;
	SaveGameInstance->MapIndex = game_instance->MapIndex;
	
	/*
	*/
	for (int i = 0; i < game_instance->CurrentMaps.Num(); i++)
	{
		SaveGameInstance->CurrentMaps.Add(game_instance->CurrentMaps[i]);
	}
	SaveGameInstance->Position = Character->GetActorLocation();
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
}

int UIC_BlueprintFunctionLibrary::LoadGame(UObject * WorldContextObject)
{
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UIC_GameInstance* game_instance = Cast<UIC_GameInstance>(UGameplayStatics::GetGameInstance(World));
	UIC_SaveGame* LoadGameInstance = Cast<UIC_SaveGame>(UGameplayStatics::CreateSaveGameObject(UIC_SaveGame::StaticClass()));
	LoadGameInstance = Cast<UIC_SaveGame>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->SaveSlotName, LoadGameInstance->UserIndex));
	game_instance->MapIndex = LoadGameInstance->MapIndex;
	/*
	*/
	for (int i = 0; i < LoadGameInstance->CurrentMaps.Num(); i++)
	{
		game_instance->CurrentMaps.Add(LoadGameInstance->CurrentMaps[i]);
	}

	return game_instance->MapIndex;
}

void UIC_BlueprintFunctionLibrary::SetActualMapSection(UObject * WorldContextObject, int IndexSection)
{
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UIC_GameInstance* game_instance = Cast<UIC_GameInstance>(UGameplayStatics::GetGameInstance(World));

	if(IndexSection >= 0)	game_instance->MapIndex = IndexSection;

}

void UIC_BlueprintFunctionLibrary::SetActualMapsName(UObject * WorldContextObject, TArray<FName> LoadMaps, TArray<FName> UnloadMaps)
{
	UWorld * World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UIC_GameInstance* game_instance = Cast<UIC_GameInstance>(UGameplayStatics::GetGameInstance(World));

	//remove Unload Maps from CurrentMaps
	for (int i = 0; i < UnloadMaps.Num(); i++)
	{
		if (!UnloadMaps[i].IsNone() || UnloadMaps[i] != " ") {
			for (int j = 0; j < game_instance->CurrentMaps.Num(); j++)
			{
				// (game_instance->CurrentMaps[j] == UnloadMaps[i]) {
					if(game_instance->CurrentMaps[j].Compare(UnloadMaps[i]) == 0){
					game_instance->CurrentMaps.RemoveAt(j);
				}
			}
		}
		
	}
	//Add new Maps to CurrentMpas
	for (int i = 0; i < LoadMaps.Num(); i++)
	{
		if (!LoadMaps[i].IsNone() || LoadMaps[i] != " ") {
			game_instance->CurrentMaps.Add(LoadMaps[i]);
		}
	}

}

void UIC_BlueprintFunctionLibrary::ShowSubtitles(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->ShowSubtitle();
  }
}

void UIC_BlueprintFunctionLibrary::HideSubtitles(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->HideSubtitle();
  }
}

void UIC_BlueprintFunctionLibrary::NextTutorial(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->NextTutorial();
  }
}

void UIC_BlueprintFunctionLibrary::PreviousTutorial(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->PreviousTutorial();
  }
}

void UIC_BlueprintFunctionLibrary::ShowTutorial(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->ShowTutorial();
  }
}

void UIC_BlueprintFunctionLibrary::HideTutorial(UObject* WorldContextObject) {
  AIC_HUD *hud = Cast<AIC_HUD>(UGameplayStatics::GetPlayerController(WorldContextObject, 0)->GetHUD());
  if (hud != nullptr) {
    hud->HideTutorial();
  }
}
