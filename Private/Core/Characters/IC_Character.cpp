// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "GameFramework/InputSettings.h"
#include "Core/Characters/IC_PlayerController.h"
#include "Core/BlueprintLibrary/IC_BlueprintFunctionLibrary.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Kismet/KismetMathLibrary.h"
#include "Core/Characters/IC_Character.h"



////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_Character::AIC_Character() {
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  BaseTurnRate = 45.f;
  BaseLookUpRate = 45.f;
  Weight = 0.0f;

  CharacterType = ECharacterType::CT_Undefined;

  // Create a CameraComponent	
  FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
  FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
  FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
  FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->bLockToHmd = false;
	billboardComponent = CreateDefaultSubobject<UMaterialBillboardComponent>(TEXT("Material Billboard pointer"));
	billboardComponent->SetupAttachment(FirstPersonCameraComponent);

  PossessionMenuLocation = CreateDefaultSubobject<USceneComponent>(TEXT("PossessionMenuLocation"));
  PossessionMenuLocation->SetupAttachment(GetCapsuleComponent());

  bRaycastHitObject = false;
  HoverColor = FColor(0, 0, 255, 255);
  bDisableMovement = false;
  bDisableMouseRotation = false;
  bAcceptMenu = false;
  bToggleMenu = false;

	//Create Subtitles
	//Subtitle = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Subtitle"));
	//Subtitle->SetupAttachment(FirstPersonCameraComponent);
	//SubtitleBack = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SubtitleBack"));
	//SubtitleBack->SetupAttachment(FirstPersonCameraComponent);
	ActiveSubtitle = false;
	TimeToShowSubtitle = 3.0f;
	ScrollSpeed = 10;

	HudWidget = CreateAbstractDefaultSubobject<UWidgetComponent>(TEXT("HudWidget"));
	HudWidget->SetupAttachment(FirstPersonCameraComponent);
	

	//const ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObj(TEXT("StaticMesh'/Game/TheIndianCave/Assets/ContentExamples/Plane.Plane'"));
	//SubtitleBack->SetStaticMesh(MeshObj.Object);


}

void AIC_Character::BeginPlay() {
  Super::BeginPlay();

	DynamicCrosshair = UMaterialInstanceDynamic::Create(billboardComponent->GetMaterial(0), this);
	billboardComponent->SetMaterial(0, DynamicCrosshair);

  if (RaycastDistance == 0.0f) {
    RaycastDistance = 500.0f;
  }

  if (WalkSpeed == 0.0f) {
    WalkSpeed = 200.0f;
  }

  if (RunningSpeed == 0.0f) {
    RunningSpeed = 500.0f;
  }

  if (FadeTime == 0.0f) {
    FadeTime = 2.0f;
  }

  if (HoverBaseBrightness == 0.0f) {
    HoverBaseBrightness = 10.0f;
  }

  this->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;

  if (GameMode == nullptr) {
    GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  }
  
  if (PlayerController == nullptr) {
    PlayerController = Cast<AIC_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
  }

  Capsule = GetCapsuleComponent();

  GetCharacterMovement()->bRunPhysicsWithNoController = true;

  if (CharacterType != ECharacterType::CT_Indian) {
    billboardComponent->SetVisibility(false);
  }

	//Configure Subtitle
	/*
	Subtitle->HorizontalAlignment = EHorizTextAligment::EHTA_Center;
	Subtitle->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
	Subtitle->SetRelativeLocation(FVector(300.0f, 0.0f, 0.0f));
	StartPos = Subtitle->RelativeLocation;
	EndPos = StartPos + FVector(0.0, 0.0, 20.0);
	Subtitle->SetRelativeRotation(FRotator(0.0f, 180.0f, 0.0f).Quaternion());
	Subtitle->SetFont(Font);
	Subtitle->SetWorldSize(20.0f);
	Subtitle->SetTextMaterial(TextMaterial);
	//Subtitle->SetTextRenderColor(FColor::Blue);
	Subtitle->SetText("");
	*/

	if (HudReference) {
		HudWidget->SetWidgetClass(HudReference);
		HudWidget->SetVisibility(false);
	}

	
	/*
	SubtitleBack->SetRelativeLocation(FVector(300.0f, 0.0f, 0.0f));
	StartPos = SubtitleBack->RelativeLocation;
	EndPos = StartPos + FVector(0.0, 0.0, 20.0);
	SubtitleBack->SetRelativeRotation(FRotator(0.0f, 180.0f, 0.0f).Quaternion());
	*/
	
	//Subtitle->SetRelativeLocation(FVector(302.0f, 0.0f, -17.0f));
	//Subtitle->SetRelativeRotation(FRotator(90.0f, 0.0f, 90.0f).Quaternion());
}

void AIC_Character::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
  // To debug raycast and keep active when possesing diferent characters
  // change the if to 0
#if 1
  if (GameMode != nullptr) {
    if (GameMode->CharacterTypePossessed == CharacterType) {
      Raycast();
      RaycastOnHoverHitObject();
			BillboardRaycast();
      if (GameMode->bChangeMenu) {
        FRotator newRotation = UKismetMathLibrary::FindLookAtRotation(GameMode->PossessionMenuNew->GetActorLocation(), this->GetActorLocation());
        newRotation.Pitch = 0.0f;
        GameMode->PossessionMenuNew->SetActorRotation(newRotation);
      }
    }
  }
#else 
  Raycast();
  RaycastOnHoverHitObject();
#endif
  InteractMenu();
	
	//Time to Show
	if (GetWorld()->GetTimeSeconds() > StartTime + TimeToShowSubtitle) {
		ActiveSubtitle = false;
		HudWidget->SetVisibility(false);
	}
	/*
	//Scroll subtitles 
	if (ActiveSubtitle) {
		Subtitle->SetRelativeLocation(FVector(300.0, 0.0, Subtitle->RelativeLocation.Z+ GetWorld()->GetDeltaSeconds() * ScrollSpeed));
	}
	*/
	
		/*
		Subtitle->SetRelativeLocation(StartPos);
		Subtitle->SetVisibility(false);
		Subtitle->SetText("");
		*/
		
}

////////////////////////////////////////////////////////////////////////
// Raycast Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character::BillboardRaycast() {

	FHitResult bbHitData;
	bool bbRaycastHitObject;

	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector End = Start + FirstPersonCameraComponent->GetForwardVector() * 5000.0f;

	UWorld* World = this->GetWorld();

	FCollisionQueryParams TraceParams(FName(TEXT("TraceParams")), true, this);
	TraceParams.bTraceComplex = true;
	//Ignore Actors
	TraceParams.AddIgnoredActor(this);

	//bbRaycastHitObject = World->LineTraceSingleByObjectType(bbHitData, Start, End, ECC_Visibility, TraceParams);
	bbRaycastHitObject = World->LineTraceSingleByChannel(bbHitData, Start, End, ECC_Visibility, TraceParams);
	float distanceBillboard;
	if (bbRaycastHitObject) {
		distanceBillboard = (bbHitData.ImpactPoint - Start).Size();
	}
	else {
		distanceBillboard = (End - Start).Size();
	}
	billboardComponent->SetRelativeLocation(FVector(distanceBillboard, 0.0f, 0.0f));

}

void AIC_Character::Raycast() {
  const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
  const FVector End = Start + FirstPersonCameraComponent->GetForwardVector() * RaycastDistance;

  UWorld* World = this->GetWorld();

  FCollisionQueryParams TraceParams(FName(TEXT("TraceParams")), true, this);
  TraceParams.bTraceComplex = true;
  //Ignore Actors
  TraceParams.AddIgnoredActor(this);

  FCollisionObjectQueryParams param;
  param.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldStatic);
  param.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel1);
  param.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel2);
  param.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel3);
  if (GameMode->CharacterTypePossessed != ECharacterType::CT_Squirrel) {
    param.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel4);
  }
  param.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel5);

  bRaycastHitObject = World->LineTraceSingleByObjectType(HitData, Start, End, param);
  // DrawDebugLine(GetWorld(), Start, End, FColor(255, 0, 0), false, -1, 0, 12.333);

  TriggerVisionRaycast();

	AIC_Object* obj = Cast<AIC_Object>(HitData.GetActor());
	float value = 0.0f;

	if (obj != nullptr) {
		if (obj->bInteractable) {
			if (DynamicCrosshair != nullptr) {
				if (DynamicCrosshair->GetScalarParameterValue("Selection", value)) {
					DynamicCrosshair->SetScalarParameterValue("Selection", 1.0f);
				}
			}
		} else {
			if (DynamicCrosshair->GetScalarParameterValue("Selection", value)) {
				DynamicCrosshair->SetScalarParameterValue("Selection", 0.0f);
			}
		}
	}
	else {
		if (DynamicCrosshair->GetScalarParameterValue("Selection", value)) {
			DynamicCrosshair->SetScalarParameterValue("Selection", 0.0f);
		}
	}
}

void AIC_Character::TriggerVisionRaycast() {
  AIC_Trigger* trig = Cast<AIC_Trigger>(HitData.GetActor());
  if (trig != nullptr) {
    if (TriggerHitByRaycast == nullptr) {
      TriggerHitByRaycast = trig;
    }
    else {
      if (trig != TriggerHitByRaycast) {
        // Eliminate accumulated time of previous one
        TriggerHitByRaycast->ResetTime();
        // assign the new one
        TriggerHitByRaycast = trig;
      }
    }
    // Accumulate time
    TriggerHitByRaycast->AccumulateTime();
  }
  else {
    // Reset time of TriggerHitByRaycast
    if (TriggerHitByRaycast != nullptr) {
      TriggerHitByRaycast->ResetTime();
      TriggerHitByRaycast = nullptr;
    }
  }
}

void AIC_Character::RaycastOnHoverHitObject() {

}

////////////////////////////////////////////////////////////////////////
// Movement Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character::MoveForward(float Value) {
  if (!bDisableMovement) {
    if (Value != 0.0f) {
      AddMovementInput(GetActorForwardVector(), Value);
    }
  }
}

void AIC_Character::MoveRight(float Value) {
  if (!bDisableMovement) {
    if (Value != 0.0f) {
      AddMovementInput(GetActorRightVector(), Value);
    }
  }
}

void AIC_Character::TurnAtRate(float Rate) {
  if (!bDisableMouseRotation) {
    // calculate delta for this frame from the rate information
    AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
  }
}

void AIC_Character::LookUpAtRate(float Rate) {
  if (!bDisableMouseRotation) {
    // calculate delta for this frame from the rate information
    AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
  }
}

void AIC_Character::StartSprint() {
  this->GetCharacterMovement()->MaxWalkSpeed = RunningSpeed;
}

void AIC_Character::EndSprint() {
  this->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void AIC_Character::EnableMovement(bool enable_mouse_rotation) {
  bDisableMouseRotation = !enable_mouse_rotation;
  bDisableMovement = false;
}

void AIC_Character::DisableMovement(bool disable_mouse_rotation) {
  bDisableMovement = true;
  bDisableMouseRotation = disable_mouse_rotation;
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character::Interact() {
  if (ObjectHitByRaycast != nullptr) {
    if (ObjectHitByRaycast->CanInteract()) {
      ObjectHitByRaycast->Interact();	
    }
    ObjectHitByRaycast = nullptr;
  }
}

////////////////////////////////////////////////////////////////////////
// Possess Functions
///////////////////////////////////////////////////////////////////////

void AIC_Character::InteractMenu() {
  if (GameMode != nullptr) {
    if (HitData.GetComponent() != nullptr && bToggleMenu && !GameMode->bChangeMenu) {
      UMeshComponent* object_reference = Cast<UMeshComponent>(HitData.GetComponent());
      if (object_reference != nullptr) {
        if (object_reference->GetName().Contains("DreamCatcherRing")) {
          if (bAcceptMenu) {
            bAcceptMenu = false;
            if (object_reference->GetName().Equals("DreamCatcherRing_Indian")) {
              this->PossessAnimal(ECharacterType::CT_Indian);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Bear") && GameMode->bBear) {
              this->PossessAnimal(ECharacterType::CT_Bear);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Squirrel") && GameMode->bSquirrel) {
              this->PossessAnimal(ECharacterType::CT_Squirrel);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Wolf") && GameMode->bWolf) {
              this->PossessAnimal(ECharacterType::CT_Wolf);
            }
          }
          if (bRecoverMenu && GameMode->CharacterTypePossessed == ECharacterType::CT_Indian) {
            bRecoverMenu = false;
            if (object_reference->GetName().Equals("DreamCatcherRing_Indian")) {
              this->Recover(GameMode->Indian);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Bear") && GameMode->bBear) {
              this->Recover(GameMode->Bear);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Squirrel") && GameMode->bSquirrel) {
              this->Recover(GameMode->Squirrel);
            }
            else if (object_reference->GetName().Equals("DreamCatcherRing_Wolf") && GameMode->bWolf) {
              this->Recover(GameMode->Wolf);
            }
          }
        }
      }
    }
  }  
}

void AIC_Character::PossessAnimal(ECharacterType Creature, bool bForce) {
  if (GameMode->bCanPossess || bForce) {
    AIC_Character *animal = nullptr;
    bool bIsSelf = false;
    if (GameMode != nullptr && PlayerController != nullptr) {
      switch (Creature) {
      case ECharacterType::CT_Bear:
        if (GameMode->bBear) {
          animal = GameMode->Bear;
          animal->DisableWolfVision();
          bIsSelf = PlayerController->GetPawn()->IsA(AIC_Character_Bear::StaticClass());
          GameMode->Indian->CarriedObjectComp->ThrowTemporalObject();
				}
        break;
      case ECharacterType::CT_Indian:
        animal = GameMode->Indian;
        animal->DisableWolfVision();
        bIsSelf = PlayerController->GetPawn()->IsA(AIC_Character_Indian::StaticClass());
        GameMode->Indian->CarriedObjectComp->GrabTemporalObject();
        break;
      case ECharacterType::CT_Squirrel:
        if (GameMode->bSquirrel) {
          animal = GameMode->Squirrel;
          animal->DisableWolfVision();
          bIsSelf = PlayerController->GetPawn()->IsA(AIC_Character_Squirrel::StaticClass());
          GameMode->Indian->CarriedObjectComp->ThrowTemporalObject();
        }
        break;
      case ECharacterType::CT_Wolf:
        if (GameMode->bWolf) {
          animal = GameMode->Wolf;
          animal->EnableWolfVision();
          bIsSelf = PlayerController->GetPawn()->IsA(AIC_Character_Wolf::StaticClass());
          GameMode->Indian->CarriedObjectComp->ThrowTemporalObject();
        }
        break;
      }
      if (animal != nullptr && !bIsSelf) {
       	PlayerController->Possess(animal);
        GameMode->CharacterTypePossessed = animal->CharacterType;
        GameMode->OnPossess.Broadcast(animal->CharacterType);
				//UpdateSubtitleParams(animal);
        if (bToggleMenu) ToggleMenu();
      }
    }
  }
}

void AIC_Character::AcceptMenu() {
  bAcceptMenu = true;
  if (GameMode) {
    if (GameMode->bChangeMenu) {
      GameMode->PossessionMenuNew->MenuPossess();
    }
  }
}

void AIC_Character::RecoverMenu() {
  bRecoverMenu = true;
  if (GameMode) {
    if (GameMode->bChangeMenu) {
      GameMode->PossessionMenuNew->MenuRecover();
    }
  }
}

void AIC_Character::Recover(AIC_Character *animal) {
  if (GameMode != nullptr && animal->CharacterType != ECharacterType::CT_Indian) {
    FVector new_location = this->GetActorLocation();

    switch (animal->CharacterType) {
    case ECharacterType::CT_Bear:
      new_location = GameMode->GetRespawnLocation(ECharacterType::CT_Bear);
      break;
    case ECharacterType::CT_Squirrel:
      new_location = GameMode->GetRespawnLocation(ECharacterType::CT_Squirrel);
      break;
    case ECharacterType::CT_Wolf:
      new_location = GameMode->GetRespawnLocation(ECharacterType::CT_Wolf);
      break;
    default:
      new_location = FVector(0.0f);
      break;
    }
    GameMode->OnSummon.Broadcast(animal->CharacterType);
    animal->SetActorLocation(new_location);
  }
}

void AIC_Character::ToggleMenu() {
  if (GameMode->bCanPossess) {
    bToggleMenu = !bToggleMenu;
    if (bToggleMenu) {
      this->GetCharacterMovement()->MaxWalkSpeed = 0.0f;
    }
    else {
      this->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
    }
    if (GameMode != nullptr) {
      bAcceptMenu = false;
      bRecoverMenu = false;
      FVector newPosition;
      FRotator newRotation;
      if (GameMode->bChangeMenu) {
        newPosition = PossessionMenuLocation->GetComponentLocation();
        newRotation = UKismetMathLibrary::FindLookAtRotation(GameMode->PossessionMenuNew->GetActorLocation(), this->GetActorLocation());
        newRotation.Pitch = 0.0f;
      }
      else {
        newPosition = this->GetActorForwardVector() * 400.0f + this->GetActorLocation();
        newPosition.Z += 150.0f;
        newRotation = FRotator(90.0f, this->GetActorRotation().Yaw, this->GetActorRotation().Roll + 1.0f);
      }
      GameMode->TogglePossessionMenu(bToggleMenu, newPosition, newRotation);
    }
  }
}

void AIC_Character::MenuLeft() {
  if (GameMode != nullptr) {
    if (GameMode->bChangeMenu) {
      if (bToggleMenu) {
        GameMode->PossessionMenuNew->MenuLeft();
        LOG_MESSAGE("Menu left");
      }
    }
  }
}

void AIC_Character::MenuRight() {
  if (GameMode != nullptr) {
    if (GameMode->bChangeMenu) {
      if (bToggleMenu) {
        GameMode->PossessionMenuNew->MenuRight();
        LOG_MESSAGE("Menu right");
      }
    }
  }
}

void AIC_Character::PossessedBy(AController* NewController) {
  Super::PossessedBy(NewController);
  billboardComponent->SetVisibility(true);
  UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(1.0f, 0.0f, FadeTime, FadeColor, false, true);
	//HudWidget->SetVisibility(true);
	
}

void AIC_Character::UnPossessed() {
  Super::UnPossessed();
  UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, FadeTime, FadeColor, false, true);
  billboardComponent->SetVisibility(false);
  bRaycastHitObject = false;
	//Subtitle->SetVisibility(false);
	HudWidget->SetVisibility(false);
}

////////////////////////////////////////////////////////////////////////
// Wolf Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Character::EnableWolfVision() {
  UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), Collection, FName(TEXT("Wolf_Emissive")), 9.0f);
};

void AIC_Character::DisableWolfVision() {
  UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), Collection, FName(TEXT("Wolf_Emissive")), 0.0f);
};

////////////////////////////////////////////////////////////////////////
// Interface Functions and Variables
///////////////////////////////////////////////////////////////////////

float AIC_Character::GetWeight() {
  return Weight;
}

float AIC_Character::GetWeightIn() {
	if (bWeightLocked) return 0.0f;
	else return Weight;
}
float AIC_Character::GetWeightOut() {
	if (!bWeightLocked) return 0.0f;
	else return Weight;
}
bool AIC_Character::GetLockWeight()
{
	return bWeightLocked;
}
void AIC_Character::LockWeight() {
	bWeightLocked = true;
}

void AIC_Character::UnlockWeight() {
	bWeightLocked = false;
}

void AIC_Character::Respawn() {
  UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, FadeTime, FadeColor, false, true);
  this->SetActorLocation(GameMode->GetRespawnLocation(CharacterType));
  UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(1.0f, 0.0f, FadeTime, FadeColor, false, true);
}

////////////////////////////////////////////////////////////////////////
// Subtitles fucntions and Variables
///////////////////////////////////////////////////////////////////////
 

void  AIC_Character::UpdateSubtitleParams(AIC_Character* Character) {
	/*
	Subtitle->SetActive(false);
	LOG_MESSAGE_PARAM("Animal: %s", *Character->GetName());
	Character->Subtitle->SetText(Subtitle->Text);
	Character->ActiveSubtitle = true;
	Character->Subtitle->SetRelativeLocation(Subtitle->RelativeLocation);
	Character->Subtitle->ToggleVisibility();
	Character->StartTime = StartTime;
	*/

	//Character->ShowSubtitle(Subtitle->Text.ToString(), TimeToShowSubtitle);
	//HudWidget->SetVisibility(false);

}
 