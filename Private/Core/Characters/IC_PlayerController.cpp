// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Core/Characters/IC_Character.h"
#include "Characters/IC_Character_Indian.h"
#include "Characters/IC_Character_Squirrel.h"
#include "Characters/IC_Character_Wolf.h"
#include "Core/Characters/IC_PlayerController.h"
#include "Core/Objects/IC_Object.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Widgets/IC_HUD.h"

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

AIC_PlayerController::AIC_PlayerController(const FObjectInitializer& ObjectInitializer)
  : Super(ObjectInitializer) {

}

void AIC_PlayerController::SetupInputComponent() {
  //Super::SetupPlayerInputComponent(InputComponent);
  Super::SetupInputComponent();
  check(InputComponent);
  bSprint = true;
  if (InputComponent != NULL) {
    InputComponent->BindAxis("Vertical", this, &AIC_PlayerController::MoveForward);
    InputComponent->BindAxis("Strafe", this, &AIC_PlayerController::MoveRight);
    InputComponent->BindAxis("YawMove", this, &AIC_PlayerController::TurnAtRate);
    InputComponent->BindAxis("RollMove", this, &AIC_PlayerController::LookUpAtRate);
    InputComponent->BindAction("ButtonDown", IE_Released, this, &AIC_PlayerController::InteractObject);
    InputComponent->BindAction("RightTrigger", IE_Pressed, this, &AIC_PlayerController::EnableChargingForce);
    InputComponent->BindAction("RightTrigger", IE_Released, this, &AIC_PlayerController::Throw);
    InputComponent->BindAction("Sprint", IE_Pressed, this, &AIC_PlayerController::StartSprint);
    //InputComponent->BindAction("Sprint", IE_Released, this, &AIC_PlayerController::EndSprint);
    InputComponent->BindAction("Bear", IE_Released, this, &AIC_PlayerController::PossessBear);
    InputComponent->BindAction("Indian", IE_Released, this, &AIC_PlayerController::PossessIndian);
    InputComponent->BindAction("Squirrel", IE_Released, this, &AIC_PlayerController::PossessSquirrel);
    InputComponent->BindAction("Wolf", IE_Released, this, &AIC_PlayerController::PossessWolf);
    InputComponent->BindAction("ButtonDown", IE_Pressed, this, &AIC_PlayerController::AcceptMenu);
    InputComponent->BindAction("LeftTrigger", IE_Pressed, this, &AIC_PlayerController::StartGlide);
    InputComponent->BindAction("LeftTrigger", IE_Released, this, &AIC_PlayerController::EndGlide);
    InputComponent->BindAction("ButtonLeft", IE_Pressed, this, &AIC_PlayerController::RecoverMenu);
    InputComponent->BindAction("ButtonUp", IE_Pressed, this, &AIC_PlayerController::ToggleMenu);
    InputComponent->BindAction("MenuLeft", IE_Pressed, this, &AIC_PlayerController::MenuLeft);
    InputComponent->BindAction("MenuRight", IE_Pressed, this, &AIC_PlayerController::MenuRight);

    // Tutorial Inputs
    InputComponent->BindAxis("NextTutorial", this, &AIC_PlayerController::NextTutorial);
    InputComponent->BindAxis("PreviousTutorial", this, &AIC_PlayerController::PreviousTutorial);
    InputComponent->BindAction("CloseTutorial", IE_Pressed, this, &AIC_PlayerController::CloseTutorial);
  }
}

////////////////////////////////////////////////////////////////////////
// Movement Input Functions
///////////////////////////////////////////////////////////////////////

void AIC_PlayerController::MoveForward(float Val) {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());

  if (ControlledCharacter != nullptr) {
    ControlledCharacter->MoveForward(Val);
  }
}

void AIC_PlayerController::MoveRight(float Val) {

    AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
    if (ControlledCharacter != nullptr) {
      ControlledCharacter->MoveRight(Val);
    }

}

void AIC_PlayerController::TurnAtRate(float Rate) {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());

  if (ControlledCharacter != nullptr) {
    ControlledCharacter->TurnAtRate(Rate);
  }
}

void AIC_PlayerController::LookUpAtRate(float Rate) {

  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->LookUpAtRate(Rate);
  }
}

void AIC_PlayerController::StartSprint() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    bSprint = !bSprint;
    if (bSprint) {
      ControlledCharacter->StartSprint();
    }
    else {
      ControlledCharacter->EndSprint();
    }
  }
}

void AIC_PlayerController::EndSprint() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->EndSprint();
  }
}

////////////////////////////////////////////////////////////////////////
// Object Interaction Input Functions
///////////////////////////////////////////////////////////////////////

void AIC_PlayerController::InteractObject() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter) {
    ControlledCharacter->Interact();
  }
}

void AIC_PlayerController::EnableChargingForce() {
  AIC_Character_Indian* ControlledCharacter = Cast<AIC_Character_Indian>(GetPawn());
  if (ControlledCharacter != nullptr) {
    if (ControlledCharacter->IsHoldingObject()) {
      ControlledCharacter->EnableChargingForce();
    }
  }
}

void AIC_PlayerController::Throw() {

  AIC_Character_Indian* ControlledCharacter = Cast<AIC_Character_Indian>(GetPawn());
  if (ControlledCharacter != nullptr) {
    if (ControlledCharacter->IsHoldingObject()) {
      if (ControlledCharacter->CarriedObjectComp->bChargeForce) {
        ControlledCharacter->ThrowObject();
      }
    }
  }

}

////////////////////////////////////////////////////////////////////////
// Squirrel Input Functions
///////////////////////////////////////////////////////////////////////

void AIC_PlayerController::StartGlide() {
  AIC_Character_Squirrel* ControlledCharacter = Cast<AIC_Character_Squirrel>(GetPawn());

  if (ControlledCharacter != nullptr) {
    if (!ControlledCharacter->bToggleMenu) {
      ControlledCharacter->StartGlide();
    }
  }
}

void AIC_PlayerController::EndGlide() {
  AIC_Character_Squirrel* ControlledCharacter = Cast<AIC_Character_Squirrel>(GetPawn());

  if (ControlledCharacter != nullptr) {
    if (!ControlledCharacter->bToggleMenu) {
      ControlledCharacter->EndGlide();
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Posession Input Functions
///////////////////////////////////////////////////////////////////////


void AIC_PlayerController::AcceptMenu() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->AcceptMenu();
  }
}

void AIC_PlayerController::RecoverMenu() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->RecoverMenu();
  }
}

void AIC_PlayerController::ToggleMenu() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  AIC_Character_Indian * IndianCharacter = Cast<AIC_Character_Indian>(GetPawn());
  if(ControlledCharacter) {
    if (ControlledCharacter->IsA(AIC_Character_Indian::StaticClass()) && IndianCharacter != nullptr) {
      if (!IndianCharacter->IsHoldingObject()) {
        ControlledCharacter->ToggleMenu();
      }
    }
  }
  else if (!ControlledCharacter->IsA(AIC_Character_Indian::StaticClass()) && ControlledCharacter != nullptr) {
    ControlledCharacter->ToggleMenu();
  }
}

void AIC_PlayerController::MenuLeft() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->MenuLeft();
  }
}

void AIC_PlayerController::MenuRight() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->MenuRight();
  }
}

void AIC_PlayerController::PossessBear() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->PossessAnimal(ECharacterType::CT_Bear);
  }
}

void AIC_PlayerController::PossessIndian() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->PossessAnimal(ECharacterType::CT_Indian);
  }
}

void AIC_PlayerController::PossessSquirrel() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->PossessAnimal(ECharacterType::CT_Squirrel);
  }
}

void AIC_PlayerController::PossessWolf() {
  AIC_Character* ControlledCharacter = Cast<AIC_Character>(GetPawn());
  if (ControlledCharacter != nullptr) {
    ControlledCharacter->PossessAnimal(ECharacterType::CT_Wolf);
  }
}

////////////////////////////////////////////////////////////////////////
// Tutorial Input Functions
///////////////////////////////////////////////////////////////////////

void AIC_PlayerController::NextTutorial(float Val) {
  if (Val > 0.0f) {
    AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (GameMode != nullptr) {
      if (GameMode->bTutorialOpened) {
        AIC_HUD *hud = Cast<AIC_HUD>(GetHUD());
        if (hud != nullptr) {
          hud->NextTutorial();
        }
      }
    }
  }
}

void AIC_PlayerController::PreviousTutorial(float Val) {
  if (Val < 0.0f) {
    AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (GameMode != nullptr) {
      if (GameMode->bTutorialOpened) {
        AIC_HUD *hud = Cast<AIC_HUD>(GetHUD());
        if (hud != nullptr) {
          hud->PreviousTutorial();
        }
      }
    }
  }
}

void AIC_PlayerController::CloseTutorial() {
  AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  if (GameMode != nullptr) {
    if (GameMode->bTutorialOpened) {
      AIC_HUD *hud = Cast<AIC_HUD>(GetHUD());
      if (hud != nullptr) {
        hud->HideTutorial();
      }
    }
  }
}
