// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "IC_LevelStreaming.h"


// Sets default values
AIC_LevelStreaming::AIC_LevelStreaming()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  CollisionDetector = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionDetector"));
  CollisionDetector->SetupAttachment(RootComponent);

  bCanStream = true;

  bBeginWorking = false;
  bUnloadFinished = false;
  bLoadFinished = false;
  MapIndex = 0;

  bDoCameraFade = true;
  FadeColor = { 0.0f, 0.0f, 0.0f, 0.0f };
  FadeIn = 0.5f;
  FadeOut = 1.5f;
  DelayBegin = 0.5f;
  DelayEnd = 0.5f;

  SectionID = 0;
}

// Called when the game starts or when spawned
void AIC_LevelStreaming::BeginPlay()
{
	Super::BeginPlay();
  bCanStream = !UIC_BlueprintFunctionLibrary::GameInstanceDevelopment(this);
  CollisionDetector->OnComponentBeginOverlap.AddDynamic(this, &AIC_LevelStreaming::OnBeginOverlap);
}

// Called every frame
void AIC_LevelStreaming::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIC_LevelStreaming::StartLevelStreaming() {
  if (!bBeginWorking && bCanStream) {
    // Finishing level streaming
    AIC_Character_Indian* indian = UIC_BlueprintFunctionLibrary::GetIndianReference(this);
    if (indian) {
			if (indian->IsHoldingObject()) {
				indian->ThrowObject();
			}
    }

    UIC_BlueprintFunctionLibrary::DisableAllCharactersMovement(GetWorld());
    if (UnloadMaps.Num() > 0) {
      if (!UnloadMaps[MapIndex].IsNone() || UnloadMaps[MapIndex] != " ") {
        StreamLatentInfo.CallbackTarget = this;
        StreamLatentInfo.ExecutionFunction = "UnloadMap";
        StreamLatentInfo.UUID = 1;
        StreamLatentInfo.Linkage = 0;
        if (bDoCameraFade) {
          UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, FadeIn, FadeColor, false, true);
        }
        UKismetSystemLibrary::Delay(GetWorld(), DelayBegin, StreamLatentInfo);
      }
    }
    else if (LoadMaps.Num() > 0) {
      if (!LoadMaps[MapIndex].IsNone() || LoadMaps[MapIndex] != " ") {
        StreamLatentInfo.CallbackTarget = this;
        StreamLatentInfo.ExecutionFunction = "LoadMap";
        StreamLatentInfo.UUID = 1;
        StreamLatentInfo.Linkage = 0;
        if (bDoCameraFade) {
          UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, FadeIn, FadeColor, false, true);
        }
        UKismetSystemLibrary::Delay(GetWorld(), DelayBegin, StreamLatentInfo);
      }
    }
  }
  //*************************

}

void AIC_LevelStreaming::EndLevelStreaming() {
  UIC_BlueprintFunctionLibrary::EnableAllCharactersMovement(GetWorld());
  UIC_BlueprintFunctionLibrary::SetActualMapSection(this, SectionID);
  if (bDoCameraFade) {
    UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(1.0f, 0.0f, FadeOut, FadeColor, false, false);
    OnEndStreaming.Broadcast();
  }
}

void AIC_LevelStreaming::LoadMap() {
  if (MapIndex >= LoadMaps.Num()) {
    bLoadFinished = true;
    MapIndex = 0;

    // Finishing level streaming
    StreamLatentInfo.CallbackTarget = this;
    StreamLatentInfo.ExecutionFunction = "EndLevelStreaming";
    StreamLatentInfo.UUID = 1;
    StreamLatentInfo.Linkage = 0;
    UKismetSystemLibrary::Delay(GetWorld(), DelayEnd, StreamLatentInfo);
    //*************************
  }
  else {
    if (MapIndex < LoadMaps.Num() && LoadMaps.Num() > 0) {
      if (!LoadMaps[MapIndex].IsNone() || LoadMaps[MapIndex] != " ") {
        bBeginWorking = true;
        StreamLatentInfo.CallbackTarget = this;
        StreamLatentInfo.ExecutionFunction = "LoadMap";
        StreamLatentInfo.UUID = 1;
        StreamLatentInfo.Linkage = 0;
        UGameplayStatics::LoadStreamLevel(GetWorld(), LoadMaps[MapIndex], true, false, StreamLatentInfo);
        MapIndex++;
      }
    }
  }
}

void AIC_LevelStreaming::UnloadMap() {
  if (MapIndex >= UnloadMaps.Num()) {
    bUnloadFinished = true;
    MapIndex = 0;
    if (LoadMaps.Num() > 0) {
      if (!LoadMaps[MapIndex].IsNone() || LoadMaps[MapIndex] != " ") {
        bBeginWorking = true;
        StreamLatentInfo.CallbackTarget = this;
        StreamLatentInfo.ExecutionFunction = "LoadMap";
        StreamLatentInfo.UUID = 1;
        StreamLatentInfo.Linkage = 0;
        UGameplayStatics::LoadStreamLevel(GetWorld(), LoadMaps[MapIndex], true, false, StreamLatentInfo);
      }
    }
    else {
      bLoadFinished = true;

      // Finishing level streaming
      StreamLatentInfo.CallbackTarget = this;
      StreamLatentInfo.ExecutionFunction = "EndLevelStreaming";
      StreamLatentInfo.UUID = 1;
      StreamLatentInfo.Linkage = 0;
      UKismetSystemLibrary::Delay(GetWorld(), DelayEnd, StreamLatentInfo);
      //*************************
    }
  }
  else {
    if (MapIndex < UnloadMaps.Num() && UnloadMaps.Num() > 0) {
      if (!UnloadMaps[MapIndex].IsNone() || UnloadMaps[MapIndex] != " ") {
        bBeginWorking = true;
        StreamLatentInfo.CallbackTarget = this;
        StreamLatentInfo.ExecutionFunction = "UnloadMap";
        StreamLatentInfo.UUID = 1;
        StreamLatentInfo.Linkage = 0;
        UGameplayStatics::UnloadStreamLevel(this, UnloadMaps[MapIndex], StreamLatentInfo);
        MapIndex++;
      }
    }
  }
}

void AIC_LevelStreaming::OnBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
  if (!bBeginWorking) {
    AIC_Character* character = Cast<AIC_Character>(OtherActor);
    if (character) {
      if (character->CharacterType == ECharacterType::CT_Indian) {
        if (UIC_BlueprintFunctionLibrary::GetIndianReference(GetWorld())->CarriedObjectComp->bHoldObject) {
          UIC_BlueprintFunctionLibrary::GetIndianReference(GetWorld())->ThrowObject();
        }
        StartLevelStreaming();
      }
    }
  }
}

