// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Core/Characters/IC_Character.h"
#include "Trigger/IC_Trigger.h"


////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

// Sets default values
AIC_Trigger::AIC_Trigger() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
  TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
  TriggerBox->bGenerateOverlapEvents = true;
  GameMode = nullptr;
  TimeAccumulated = 0.0f;
  TimeNeededToTrigger = 0.0f;
  bVision = false;
  bVisionReutilizable = false;
  bVisionAutoReset = false;
  bVisionTriggerActivated = false;
}

// Called when the game starts or when spawned
void AIC_Trigger::BeginPlay() {
	Super::BeginPlay();

  TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AIC_Trigger::OnBeginOverlap);

  if (TimeNeededToTrigger == 0.0f) {
    TimeNeededToTrigger = 5.0f;
  }

  TriggerBox->SetCollisionProfileName(FName("IC_Trigger"));

  if (GameMode == nullptr) {
    GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
  }

  RemainingTimeToActivate = TimeNeededToTrigger;
  InitialTimeSet = TimeNeededToTrigger;
}

// Called every frame
void AIC_Trigger::Tick( float DeltaTime ) {
	Super::Tick( DeltaTime );

}

////////////////////////////////////////////////////////////////////////
// Collision detection Functions and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Trigger::OnBeginOverlap(class UPrimitiveComponent* OverlappedComp,
                                 AActor* OtherActor,
                                 class UPrimitiveComponent* OtherComp,
                                 int32 OtherBodyIndex,
                                 bool bFromSweep,
                                 const FHitResult& SweepResult) {
  AIC_Character* character = Cast<AIC_Character>(OtherActor);

  if (character != nullptr) {
    if (bCheckpoint) {
      if (character->CharacterType == ECharacterType::CT_Indian) {
        GameMode->SetIndianCheckpointLocation(TriggerBox->GetComponentLocation());
      }
    }

    if (bDeadly) {
      character->Respawn();
    }

    if (bTeleport) {
      TeleportWithoutFade(character);
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Trigger configuration Function and Variables
///////////////////////////////////////////////////////////////////////

void AIC_Trigger::TeleportWithoutFade(AIC_Character* character) {

  if (character) {
    if (character->CharacterType != ECharacterType::CT_Undefined) {
      character->SetActorLocation(GameMode->GetRespawnLocation(character->CharacterType));
    }
    else {
      // DEBUG
      LOG_ERROR("Debug: Error character type not found!");
    }
  }  
}

void AIC_Trigger::AccumulateTime() {
  if (bVision == true) {
    if (bVisionTriggerActivated == false) {
      if (TimeAccumulated < TimeNeededToTrigger) {
        TimeAccumulated += GetWorld()->GetDeltaSeconds();
        RemainingTimeToActivate -= GetWorld()->GetDeltaSeconds();
        if (RemainingTimeToActivate < 0.0f) {
          RemainingTimeToActivate = 0.0f;
        }
        // LOG_ERROR_PARAM("Accumulated Time: %f", TimeAccumulated);
      }
      else {
        // LOG_ERROR("TIME ACCUMULATED.");
        VisionActive.Broadcast();
        if (bVisionAutoReset == true) {
          RemainingTimeToActivate = InitialTimeSet;
          TimeAccumulated = 0.0f;
        }
        if (bVisionReutilizable == false) {
          bVisionTriggerActivated = true;
        }
      }
    }
  }
}

void AIC_Trigger::ResetTime() {
  if (bVision == true) {
    RemainingTimeToActivate = InitialTimeSet;
    TimeAccumulated = 0.0f;
    // LOG_ERROR("TIME RESET");
    VisionReset.Broadcast();
  }
}
