// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave/TheIndianCave.h"
#include "Interfaces/IC_Interface.h"

UIC_Interface::UIC_Interface(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

float IIC_Interface::GetWeight()
{
	return 0.0f;
}
float IIC_Interface::GetWeightIn()
{
	return 0.0f;
}

float IIC_Interface::GetWeightOut()
{
	return 0.0f;
}

bool IIC_Interface::GetLockWeight()
{
	return false;
}
void IIC_Interface::LockWeight(){}
void IIC_Interface::UnlockWeight(){}
