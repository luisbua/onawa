// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "../../Core/GameModes/IC_GameMode.h"
#include "../../Objects/IC_Object_Mission.h"
#include "Objects/IC_Object_Static.h"
#include "IC_ObjectHolderComponent.h"


// Sets default values for this component's properties
UIC_ObjectHolderComponent::UIC_ObjectHolderComponent()
{
  // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
  // off to improve performance if you don't need them.
  PrimaryComponentTick.bCanEverTick = true;
  bPlacingItem = false;
  bItemInLocation = false;
  bBlockMovement = false;
  bBlockItemInteraction = false;
  bStopInertia = true;
  CurrentObjectReference = nullptr;
  InterpolationSpeed = 5.0f;
  ReachTolerance = 2.0f;
}


// Called when the game starts
void UIC_ObjectHolderComponent::BeginPlay()
{
  Super::BeginPlay();

  AIC_Object_Static* tmp = Cast<AIC_Object_Static>(GetAttachmentRootActor());
  if (tmp) {
    ParentObject = tmp;
  }

  if (ReachTolerance == 0.0f) {
    ReachTolerance = 2.0f;
  }
}


// Called every frame
void UIC_ObjectHolderComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  // ...
  if (bPlacingItem) {
    InterpoleItemToLocationAndRotation();
    ItemReachedLocation();
  }

  if (CurrentObjectReference) {
    if (bItemInLocation && !CurrentObjectReference->bTargetDetected) {
      bItemInLocation = false;
      if (ParentObject)
        ParentObject->OnObjectLeavesHolder.Broadcast();
    }
  }
}

void UIC_ObjectHolderComponent::SetObjectToPlace(AIC_Object_Mission * ObjectReference) {
  ObjectsToPlace.Add(ObjectReference);
}

void UIC_ObjectHolderComponent::PlaceItem(int32 ObjectId) {
  CheckItemHaveToBePlaced(ObjectId);

  if (CurrentObjectReference != nullptr) {
    bPlacingItem = true;
    AIC_GameMode* gm = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (gm) {
      if (gm->Indian->IsHoldingObject()) {
        gm->Indian->ThrowObject();
      }
    }
    CurrentObjectReference->LockInteractionAndPickup();
  }  
}

void UIC_ObjectHolderComponent::CheckItemHaveToBePlaced(unsigned int ObjectID) {
  for (int32 i = 0; i < ObjectsToPlace.Num(); ++i) {
    if (ObjectsToPlace[i]) {
      if (ObjectsToPlace[i]->GetUniqueID() == ObjectID) {
        CurrentObjectReference = ObjectsToPlace[i];
      }
    }
  }
}

void UIC_ObjectHolderComponent::InterpoleItemToLocationAndRotation() {
  if (CurrentObjectReference) {
    CurrentObjectReference->SetActorLocationAndRotation(
      FMath::VInterpTo(
        CurrentObjectReference->GetActorLocation(),
        GetComponentLocation(),
        GetWorld()->GetDeltaSeconds(),
        InterpolationSpeed),
      FMath::RInterpTo(
        CurrentObjectReference->GetActorRotation(),
        GetComponentRotation(),
        GetWorld()->GetDeltaSeconds(),
        InterpolationSpeed)
    );
  }
}

void UIC_ObjectHolderComponent::ItemReachedLocation() {
  //if (FVector::PointsAreNear(CurrentObjectReference->GetActorLocation(), GetComponentLocation(), 2.0f)) {
  if (CurrentObjectReference) {
    if (CurrentObjectReference->GetActorLocation().Equals(GetComponentLocation(), ReachTolerance)) {
      // FORCE LOCATION IN CASE OF PREVIOUS COMPROVATION TOLERANCE IS TO BIG (EQUALS) 
      CurrentObjectReference->SetActorLocation(GetComponentLocation());
      CurrentObjectReference->SetActorRotation(GetComponentRotation());

      bPlacingItem = false;
      bItemInLocation = true;
      CurrentObjectReference->bDetectionEnabled = false;

      if (ParentObject)
        ParentObject->OnObjectReachHolder.Broadcast();

      if (bBlockMovement) {
        CurrentObjectReference->LockInLocation();
      }
      else {
        CurrentObjectReference->UnlockInLocation();
      }

      if (!bBlockItemInteraction) {
        CurrentObjectReference->UnlockInteractionAndPickup();
      } else {
        CurrentObjectReference->LockInteractionAndPickup();
      }

      if (bStopInertia) {
        UPrimitiveComponent* p = CurrentObjectReference->MyPrimitiveComponent;
        if (p) {
          p->SetPhysicsAngularVelocity(FVector::ZeroVector);
          p->SetPhysicsLinearVelocity(FVector::ZeroVector);
        }
      }
    }
  }
}
