// Fill out your copyright notice in the Description page of Project Settings.

#include "TheIndianCave.h"
#include "Characters/IC_Character_Indian.h"
#include "Objects/IC_Object_Mission.h"
#include "Characters/IC_Character_Squirrel.h"
#include "IC_CarryObjectComponent.h"
#include "Core/GameModes/IC_GameMode.h"
#include "Objects/IC_Object_Breakable.h"

////////////////////////////////////////////////////////////////////////
// Default UE4 Functions / Constructor / BeginPlay / Tick
///////////////////////////////////////////////////////////////////////

UIC_CarryObjectComponent::UIC_CarryObjectComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {
  PrimaryComponentTick.bCanEverTick = true;

  bHoldObject = false;
  bPhysicsHandleActive = false;
  bChargeForce = false;
  ThrowForce = 0.0f;
  PickupDistance = 0.0f;
  ChargeForceRate = 0.0f;
  ObjectRotationSpeed = 0.0f;
  BreakableForceMultiplier = 0.0f;
  EnvironmentForceMultiplier = 0.0f;
  PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("Physics Handle"));
  bGrabBreakable = false;
  bTeleport = false;
  bGotToolbeltItem = false;
}

void UIC_CarryObjectComponent::OnRegister() {
  Super::OnRegister();
}

void UIC_CarryObjectComponent::InitializeComponent() {}

void UIC_CarryObjectComponent::BeginPlay() {

  if (MaximumThrowForce == 0.0f) {
    MaximumThrowForce = 2000.0f;
  }

  if (ChargeForceRate == 0.0f) {
    ChargeForceRate = 10.0f;
  }

  if (ObjectRotationSpeed == 0.0f) {
    ObjectRotationSpeed = 1.0f;
  }

  if (PickupDistance == 0.0f) {
    PickupDistance = 300.0f;
  }

  if (BreakableForceMultiplier == 0.0f) {
    BreakableForceMultiplier = 100.0f;
  }

  if (EnvironmentForceMultiplier == 0.0f) {
    EnvironmentForceMultiplier = 1000.0f;
  }
}

void UIC_CarryObjectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (bPhysicsHandleActive) {
    SetPhysicsHandleTargetLocationAndRotation();
  }

  if (bHoldObject && bChargeForce) {
    ChargeObjectThrowForce();
  }
}

////////////////////////////////////////////////////////////////////////
// Picking up / Throwing / Rotating Objects Functions and Variables
///////////////////////////////////////////////////////////////////////

void UIC_CarryObjectComponent::GrabObject(FHitResult* RaycastData) {
  if (PhysicsHandle != NULL) {
    if (!bHoldObject) {
      if (RaycastData->GetActor() != NULL) {
        // Environment
        const FVector Start = IndianReference->FirstPersonCameraComponent->GetComponentLocation();
        FVector CameraForward = Start + IndianReference->FirstPersonCameraComponent->GetForwardVector() * PickupDistance;

        AIC_Object* target_object = Cast<AIC_Object>(RaycastData->GetActor());
        bool pickable = false;

        if (target_object != nullptr) {
          if (target_object->CanPickup() && target_object->bPickable) {
            bGrabObject = true;
            pickable = true;
            AIC_Object_Breakable* ob = Cast<AIC_Object_Breakable>(RaycastData->GetActor());
            if (ob != nullptr) {
              bGrabBreakable = true;
            }
            if (target_object->bBig) {
              bBigObject = true;
              target_object->SetActorScale3D(target_object->TargetScale);
            }
          }
        }

        if (!pickable) {
          AIC_Character_Squirrel* target_squirrel = Cast<AIC_Character_Squirrel>(RaycastData->GetActor());
          if (target_squirrel != nullptr) {
            bGrabSquirrel = true;
            target_squirrel->GetCapsuleComponent()->SetSimulatePhysics(true);
            pickable = true;
          }
        }

        if (pickable) {
          // Set holder position to its bounds. This allows object rotate
          // around himself.
          FVector holderPosition;
          if (bBigObject) {
            holderPosition = BigObjectHolder->Bounds.Origin;
          } else {
            holderPosition = ObjectHolder->Bounds.Origin;
          }

          PhysicsHandle->CurrentTransform = IndianReference->GetTransform();
          RaycastData->GetComponent()->SetWorldLocation(holderPosition);
          PhysicsHandle->GrabComponentAtLocationWithRotation(RaycastData->GetComponent(),
             RaycastData->BoneName, holderPosition, { 0.0f, 0.0f, 0.0f });

          GrabbedObject = Cast<AIC_Object>(RaycastData->GetActor());
          PhysicsObject = RaycastData->GetComponent();
          if (GrabbedObject) {
            if (GrabbedObject->Type == EObjectType::OT_Mission) {
              AIC_Object_Mission* MissionGrabbedObject = Cast<AIC_Object_Mission>(GrabbedObject);
              if (MissionGrabbedObject) {
                MissionGrabbedObject->BroadcastPickUpEvent();
              }
            }
          }
          bHoldObject = true;
          bPhysicsHandleActive = true;
        }
      }
    } // !bHoldObject
  }
}

void UIC_CarryObjectComponent::PutObjectInToolbelt(AIC_Object* obj) {
  if (!bGotToolbeltItem) {
    if (obj != nullptr) {
      ToolbeltItem = obj;
      PhysicsHandle->ReleaseComponent();
      if (PhysicsObject != nullptr) {
        PhysicsObject->WakeRigidBody();
      }
      ToolbeltItem->StaticMesh->SetSimulatePhysics(false);
      ToolbeltItem->StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
      ToolbeltItem->AttachToComponent(IndianReference->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "Yuma_Spine_01Socket");
      ToolbeltItem->SetActorRelativeRotation({ -85.04903f, 73.197128f, -86.468285f });
      bGotToolbeltItem = true;
      bGrabObject = false;
      bGrabSquirrel = false;
      bHoldObject = false;
      bGrabBreakable = false;
      bPhysicsHandleActive = false;
      bBigObject = false;
      bChargeForce = false;
      bTeleport = false;
      ThrowForce = 0.0f;
      FQuat reset = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
      ObjectHolder->SetWorldRotation(reset);

      if (PhysicsObject != nullptr) {
        // Throw the object to stop Yuma's Animation
        ThrowObject({ 0.0f, 0.0f, 0.0f });
      }
    }
  }
}

void UIC_CarryObjectComponent::PickObjectOfToolbelt() {
  if (bGotToolbeltItem) {
    //ToolbeltItem->DetachRootComponentFromParent();
    ToolbeltItem->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
    ToolbeltItem->StaticMesh->SetSimulatePhysics(true);
    ToolbeltItem->StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    FVector holderPosition;
    holderPosition = ObjectHolder->Bounds.Origin;
    PhysicsHandle->CurrentTransform = IndianReference->GetTransform();
    ToolbeltItem->SetActorLocation(holderPosition);
    UPrimitiveComponent* ToolbeltRoot = Cast<UPrimitiveComponent>(ToolbeltItem->GetRootComponent());
    PhysicsHandle->GrabComponentAtLocationWithRotation(ToolbeltRoot,
                                                       "None", holderPosition, { 0.0f, 0.0f, 0.0f});
    GrabbedObject = ToolbeltItem;
    bHoldObject = true;
    PhysicsObject = ToolbeltRoot;
    bPhysicsHandleActive = true;
    bGotToolbeltItem = false;
  }
}

void UIC_CarryObjectComponent::PickAnyObject(AIC_Object* obj) {
  if (obj != nullptr) {
    // If we're currently holding an object
    // We must drop it in order to grab another one.
    if (bHoldObject) {
      ThrowObject({ 0.0f, 0.0f, 0.0f });
    }
    FVector holderPosition;
    holderPosition = ObjectHolder->Bounds.Origin;
    PhysicsHandle->CurrentTransform = IndianReference->GetTransform();
    obj->SetActorLocation(holderPosition);
    UPrimitiveComponent* objRoot = Cast<UPrimitiveComponent>(obj->GetRootComponent());
    PhysicsHandle->GrabComponentAtLocationWithRotation(objRoot,
                                                       "None", holderPosition, { 0.0f, 0.0f, 0.0f });
    GrabbedObject = obj;
    bHoldObject = true;
    PhysicsObject = objRoot;
    bPhysicsHandleActive = true;
  }
}

void UIC_CarryObjectComponent::ThrowTemporalObject() {
  if (GrabbedObject != nullptr) {
    TemporalOnHoldObject = GrabbedObject;
    ThrowObject({ 0.0f, 0.0f, 0.0f });
  }
}

void UIC_CarryObjectComponent::GrabTemporalObject() {
  if (TemporalOnHoldObject != nullptr) {
    PickAnyObject(TemporalOnHoldObject);
  }
}

void UIC_CarryObjectComponent::ThrowObject(FVector direction) {

  bool throw_object = false;

  if (ThrowForce > 1.0f) {
    throw_object = true;
  }

  PhysicsHandle->ReleaseComponent();
  PhysicsObject->WakeRigidBody();

  if (throw_object) {
    FVector forward = IndianReference->FirstPersonCameraComponent->GetForwardVector();
    PhysicsObject->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));

    if (bGrabBreakable) {
      PhysicsObject->AddImpulse(forward * ThrowForce * BreakableForceMultiplier);
    } else {
      PhysicsObject->AddImpulse(forward * ThrowForce * EnvironmentForceMultiplier);
    }

    if (bGrabSquirrel) {
      AIC_Character_Squirrel* squirrel = Cast<AIC_Character_Squirrel>(PhysicsObject->GetOwner());
      squirrel->bThrowed = true;
      squirrel->bHanded = false;
      //Change to squirrel
      AIC_GameMode* GameMode = Cast<AIC_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
      if (GameMode != nullptr) {
        GameMode->Indian->PossessAnimal(ECharacterType::CT_Squirrel);
      }
    } else {
      if (bGrabObject) {
        if (bBigObject) {
          AIC_Object* obj = Cast<AIC_Object>(PhysicsObject->GetOwner());
          if (obj != nullptr) {
            obj->bRestoreScale = true;
          }
        }
      }
    }
  }

  bGrabObject = false;
  bGrabSquirrel = false;
  bHoldObject = false;
  bGrabBreakable = false;
  bPhysicsHandleActive = false;
  bBigObject = false;
  bChargeForce = false;
  bTeleport = false;
  ThrowForce = 0.0f;

  FQuat reset = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
  ObjectHolder->SetWorldRotation(reset);
}

void UIC_CarryObjectComponent::ChargeObjectThrowForce() {
  if (ThrowForce < MaximumThrowForce) {
    ThrowForce += 10.0f * ChargeForceRate * GetWorld()->GetDeltaSeconds();
    LOG_WARNING_PARAM("Force: %f", ThrowForce);
  }
}

void UIC_CarryObjectComponent::SetPhysicsHandleTargetLocationAndRotation() {

  //FVector holderPosition = ObjectHolder->Bounds.Origin;
  FVector holderPosition;
  if (bBigObject) {
    holderPosition = BigObjectHolder->Bounds.Origin;
  } else {
    holderPosition = ObjectHolder->Bounds.Origin;
  }

  FRotator holderRotation = ObjectHolder->GetComponentRotation();
  if (bTeleport == false) {
    PhysicsObject->SetWorldLocation(holderPosition);
    bTeleport = true;
  }
  PhysicsHandle->SetTargetLocation(holderPosition);
  // Rotation needs to be set according holderRotation because if not set
  // this way the object would return to his original rotation after rotating
  // it.
  PhysicsHandle->SetTargetRotation(holderRotation);
  
}

////////////////////////////////////////////////////////////////////////
// Class Setters
///////////////////////////////////////////////////////////////////////

void UIC_CarryObjectComponent::SetGrabPoint(USceneComponent* grab_point,
                                            USceneComponent* big_grab_point) {

  ObjectHolder = grab_point;
  BigObjectHolder = big_grab_point;
}

void UIC_CarryObjectComponent::SetIndianReference(AIC_Character_Indian* indian_reference) {
  IndianReference = indian_reference;
}
